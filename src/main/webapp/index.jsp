<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<fmt:message key="title.catalog" var="title" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<link href='<c:url value="/resources/css/child_rows.css" />'
	rel="stylesheet">
<body>
	<%@ include file="/WEB-INF/jspf/banner.jspf"%>

	<%@ include file="/WEB-INF/jspf/menu/menu.jspf"%>

	<%@ include file="/WEB-INF/jspf/table/list_books.jspf"%>

	<%@ include file="/WEB-INF/jspf/footer.jspf"%>


	<script type="text/javascript"
		src='<c:url value="/resources/js/detailsTable.js" />'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/tableSearch.js" />'></script>

	<script type="text/javascript">
		var sort = false;
		var column = [ {
			"className" : 'details-control',
			"orderable" : false,
			"data" : null,
			"defaultContent" : ''
		}, {
			"data" : 1
		}, {
			"data" : 4
		}, {
			"data" : 5
		}, {
			"data" : 6
		} ];
		<c:if test ="${role eq 'reader'}">
		sort = [-1];
		column
				.push({
					'data' : null,
					render : function(data, type, row) {
						return "<a href='openOrder?bookId="
								+ row[0]
								+ "'><span class='glyphicon glyphicon-ok-circle'></span></a>";
					}
				});
		</c:if>

		/* Formatting function for row details - modify as you need */
		function format(d) {
			var img = d[7];
			if (img !== "") {
				img = "<font style='font-size:1.3em'><image src='" +d[7] + "'/></font> ";
			}
			return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'
					+ '<tr>' + '<td><fmt:message key="title.book.subtitle" />:</td>' + '<td>'
					+ d[2]
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td><fmt:message key="title.book.descriptions" />:</td>'
					+ '<td>'
					+ d[3]
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td><fmt:message key="title.book.language" />:</td>'
					+ '<td>'
					+ d[8]
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td><fmt:message key="title.book.pages" />:</td>'
					+ '<td>'
					+ d[9]
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td><fmt:message key="title.book.image" />:</td>'
					+ '<td>'
					+ img
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td>isbn 10:</td>'
					+ '<td>'
					+ d[10]
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td>isbn 13:</td>'
					+ '<td>'
					+ d[11]
					+ '</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td><fmt:message key="title.book.other_identifiers" />:</td>'
					+ '<td>'
					+ d[12] + '</td>' + '</tr>' + '</table>';
		}

		var table = table("./ajaxListBooksServlet", sort, column, [ {
			extend : 'excelHtml5',
			text : '<i class="fa fa-file-excel-o"></i>',
			titleAttr : 'Excel'
		}, {
			extend : 'csvHtml5',
			text : '<i class="fa fa-file-text-o"></i>',
			titleAttr : 'CSV'
		}, {
			extend : 'pdfHtml5',
			text : '<i class="fa fa-file-pdf-o"></i>',
			titleAttr : 'PDF'
		} ]);

		tableSearch('.search-input-text', table);
		detailsTable("#table", table, "details-control", format);
	</script>

</body>
</html>