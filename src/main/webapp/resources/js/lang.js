const LANG_CONST = "lang";
const REGEX = /lang=[^&]+&*/g;

function changeLang(val) {
    var valueLang = val.value;
    var fullPath = window.location.href;
    var result = "";
    if (window.location.search === "") {
        result += fullPath + "?" + LANG_CONST + "=" + valueLang;
    } else if (fullPath.indexOf(LANG_CONST) > -1) {
        result += fullPath.replace(REGEX, "");
        result += LANG_CONST + "=" + valueLang;
    } else {
        result += fullPath + "&" + LANG_CONST + "=" + valueLang;
    }
    window.location.replace(result);
}
