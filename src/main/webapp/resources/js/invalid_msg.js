function InvalidMsg(textbox, message_empty, message_invalid) {
    if (textbox.value === '') {
        textbox.setCustomValidity(message_empty);
    }
    else if (textbox.validity.typeMismatch){
        textbox.setCustomValidity(message_invalid);
    }
    else {
       textbox.setCustomValidity('');
    }
    return true;
}