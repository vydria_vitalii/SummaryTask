function calendar(element, minDayFromCurrent) {
	var todayDate = new Date();
	var date = todayDate.getFullYear() + '-'
			+ ('0' + (todayDate.getMonth() + 1)).slice(-2) + '-'
			+ ('0' + (todayDate.getDate() + minDayFromCurrent)).slice(-2);

	var mydate = document.getElementById(element);
	mydate.setAttribute("min", date);
	mydate.setAttribute("required", "");

}

