$(document).ready(function() {
	const
	MAX_VISIBLE = 10;
	$("#search").click(function() {
		var values = {};
		$.each($('#books_form :input').serializeArray(), function(i, field) {
			var name = field.name;
			var value = field.value;
			if (name !== "lang-select" && !value.isEmpty()) {
				values[name] = value;
			}
		});
		booksSearch(values, KEY, MAX_VISIBLE, paramHref(HREF));
	});
});

function paramHref(href) {
	var resultHref;
	if (href.indexOf('?') > -1) {
		resultHref = href + "&";
	} else {
		resultHref = href + "?";
	}
	return resultHref;
}

String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.replaceAt = function(index, character) {
	return this.substr(0, index) + character
			+ this.substr(index + character.length);
}

String.prototype.isEmpty = function() {
	return (this.length === 0 || !this.trim());
}

function booksSearch(names, key, maxVisible, hrefParam) {
	var str = "";
	$.each(names, function(key, value) {
		str += key + ":" + value + "+";
	});
	if (!str.isEmpty()) {
		str = str.replaceAt(str.length - 1, " ");
		$.ajax({
			url : "https://www.googleapis.com/books/v1/volumes?q=" + str
					+ "&key=" + key,
			dateType : "json",
			success : function(rawData) {
				var obj = JSON.stringify(rawData);
				var data = JSON.parse(obj);
				var totalPages = Math.ceil(data.totalItems / maxVisible);
				$('#show_paginator').bootpag({
					total : totalPages,
					page : 1,
					maxVisible : maxVisible
				}).on('page', function(event, num) {
					refreshData((num - 1) * maxVisible, str, key, hrefParam);
				});
				renderDom(data, hrefParam);
			},
			type : "GET"
		});

	}
}

function renderDom(data, hrefParam) {
	var container = document.getElementById('result-rows');
	while (container.firstChild) {
		container.removeChild(container.firstChild);
	}
	var books = data.items;
	if (books) {
		var content = document.createDocumentFragment();
		for (var i = 0; i < books.length; i++) {
			var row = document.createElement('tr');
			appendTd(row, books[i].volumeInfo.title);
			appendTd(row, books[i].volumeInfo.subtitle);
			appendTd(row, books[i].volumeInfo.description);
			appendTd(row, books[i].volumeInfo.authors);
			appendTd(row, books[i].volumeInfo.publisher);
			appendTd(row, books[i].volumeInfo.publishedDate);
			appendTd(row, books[i].volumeInfo.pageCount);
			appendTd(row, books[i].volumeInfo['language']);
			appendImage(
					row,
					books[i].volumeInfo.imageLinks ? books[i].volumeInfo.imageLinks.smallThumbnail
							: undefined);
			appendTd(row, "<a href='" + hrefParam + "bookId=" + books[i].id
					+ "'><span class='glyphicon glyphicon-plus'></span></a>");
			content.appendChild(row);
			container.appendChild(content);
			window.scrollTo(0, 0);
		}
	}
}

function appendTd(container, data) {
	var td = document.createElement('td');
	td.innerHTML = data || '--//--';
	container.appendChild(td);
}

function appendImage(container, data) {
	var td = document.createElement('td');
	if (data) {
		var img = document.createElement('img');
		img.setAttribute('src', data);
		td.appendChild(img);
	} else {
		td.innerHTML = '--//--';
	}
	container.appendChild(td);
}

function refreshData(head, str, key, hrefParam) {
	$.ajax({
		url : "https://www.googleapis.com/books/v1/volumes?q=" + str
				+ '&startIndex=' + head + "&key=" + key,
		dateType : "json",
		success : function(rawData) {
			var obj = JSON.stringify(rawData);
			var data = JSON.parse(obj);
			renderDom(data, hrefParam);
		},
		type : "GET"
	});
}
