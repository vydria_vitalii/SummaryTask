function tableSearch(selector, table) {
	$(selector).on('keyup change', function() { // for text boxes
		var i = $(this).attr('data-column'); // getting column index
		var v = $(this).val(); // getting search input value
		table.columns(i).search(v).draw();
	});
}