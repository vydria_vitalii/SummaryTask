function checkPasswords(message) {
	var pass1 = document.getElementById("password1");
	var pass2 = document.getElementById("password2");
	if (pass1.value !== pass2.value) {
		pass1.setCustomValidity(message);
	} else {
		pass1.setCustomValidity('');
	}
}

