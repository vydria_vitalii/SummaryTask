

function table(nameAjaxSource,targets, columns, buttons) {

	// DataTable
	var lang = $('#lang-select option:selected').val();
	var table = $('#table').DataTable({
		"order": [[ 1, "asc" ]],
		"dom": 'lBfrtip',
		"buttons": buttons,
	     scrollCollapse: true,
		"language" : {
			"url" : "resources/json/i18n/" + lang + ".json"
		},
		"pagingType" : "full_numbers",
		"lengthMenu" : [ [ 10, 25, 50 ], [ 10, 25, 50 ] ],
		"processing" : true,
		"bServerSide" : true,
		"sAjaxSource" : nameAjaxSource,
		"sServerMethod" : "GET",
		"columns": columns,
		"aoColumnDefs" : [ {
			"bSortable" : false,
			"aTargets" : targets
		}]
	});
	
	return table;	
}