<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<html>
<head>
<title>500 Error Server</title>
<link href="<c:url value='/resources/css/error_500.css'/>" rel="stylesheet">
</head>
<body>
	<div class="parent">
		<div class="block">
			<img src="<c:url value='/resources/img/error/500.png'/>"> <br>
			<p align="center">
				<a href="index.jsp">Home page</a>
			</p>
		</div>
	</div>
</body>
</html>