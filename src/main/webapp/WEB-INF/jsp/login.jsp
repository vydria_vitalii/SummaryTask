<%@page
	import="ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="title.login" /></title>
<meta name="keywords" content="login library" />
<meta name="description" content="login for library" />
<link href="<c:url value='/resources/css/login.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/box.css'/>" rel="stylesheet">

</head>
<body>
	<%@ include file="/WEB-INF/jspf/directive/span.jspf"%>

	<ctf:message type="error" title="${error_span}"
		key="${error.error_not_user}" />

	<ctf:message type="error" title="${error_span}"
		key="${error.error_capcha}" />


	<div class="login-block">
		<p align="right">
			<ctf:locale />
		</p>
		<div class="logo">
			<a href="index.jsp"> <img
				src="<c:url value='/resources/img/logo.png' />" alt="logo">
			</a>
		</div>
		<form method="POST" action="loginUser">
			<input type="email" name="email"
				placeholder="<fmt:message key='login.placeholder.email'/>"
				value="<c:out value='${form_bean.login}'/>"
				pattern="<%= RegexConstants.EMAIL_PATTERN %>"
				oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>','<fmt:message key='error.js.email'/>');"
				oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>','<fmt:message key='error.js.email'/>');"
				required> 
				<input type="password" name="password"
				placeholder="<fmt:message key='login.placeholder.password'/>"
				oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
				oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
				required>

			<!-- reCAPTCHA -->
			<div align="center" class="g-recaptcha"
				data-sitekey="${initParam['SiteKey']}"></div> <br>
			<input class="btn" type="submit" value="" name="In">
		</form>

	</div>
	<script type="text/javascript"
		src='<c:url value="/resources/js/invalid_msg.js" />'></script>
	<!-- reCAPTCHA Libary -->
	<script
		src='https://www.google.com/recaptcha/api.js?hl=${pageContext.request.locale}'></script>
</body>
</html>
