<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<fmt:message key="title.user.info" var="title" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<link href='<c:url value="/resources/css/child_rows.css" />'
	rel="stylesheet">
<body>
	<%@ include file="/WEB-INF/jspf/banner.jspf"%>

	<%@ include file="/WEB-INF/jspf/menu/menu.jspf"%>

	<div class="container">
		<div class="row">

			<div
				class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">

				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">${title}</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3 col-lg-3 " align="center">
								<img alt="User Pic"
									src="<c:url value='/resources/img/user.png'/>"
									class="img-circle img-responsive">
							</div>

							<div class=" col-md-9 col-lg-9 ">
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td><fmt:message key="title.user.name" />:</td>
											<td>${user.name}</td>
										</tr>
										<tr>
											<td><fmt:message key="title.user.surname" />:</td>
											<td>${user.surname}</td>
										</tr>
										<tr>
											<td><fmt:message key="title.user.patronymic" />:</td>
											<td>${user.patronomic}</td>
										</tr>
										<tr>
											<td><fmt:message key="title.user.passport" /></td>
											<td>${user.passportID}</td>
										</tr>

										<tr>
											<td><fmt:message key="title.user.email" />:</td>
											<td>${user.email}</td>
										</tr>
										<tr>
											<td><fmt:message key="title.user.address" /></td>
											<td>${user.adress}</td>
										</tr>
										<c:if test="${role ne 'admin' }">
											<tr>
												<td><fmt:message key="title.user.library_card" /></td>
												<td>${user.libraryCard}</td>
											</tr>
										</c:if>

									</tbody>
								</table>

							</div>
						</div>
					</div>
					<div class="panel-footer">
						<span class="pull-right"> <a href="edit.html"
							data-original-title="Edit this user" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-warning"><i
								class="glyphicon glyphicon-edit"></i></a>
						</span>
						<p></p>
						<br>
					</div>

				</div>
			</div>
		</div>
	</div>

	<%@ include file="/WEB-INF/jspf/footer.jspf"%>


</body>
</html>