<%@page
	import="ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>
<html>
<head>
<fmt:message key="title.registrations" var="title" />
<%@ include file="/WEB-INF/jspf/form/head.jspf"%>
</head>
<body>

	<%@ include file="/WEB-INF/jspf/directive/span.jspf"%>

	<form class="form-horizontal" method="POST" action="registrationsUser">
		<fieldset>

			<!-- Form Name -->
			<legend>${title}</legend>

			<ctf:message type="error" title="${error_span}"
				key="${error.error_registaration}" />


			<!-- Select Basic -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="selectbasic"></label>
				<div class="col-md-2">
					<ctf:locale />
				</div>
			</div>

			<div class="form-group">

				<label class="col-md-4 control-label" for="email"><fmt:message
						key="table.registrations.email" /></label>
				<div class="col-md-2">
					<input id="email" name="email" type="email"
						value="${form_bean.email}" placeholder="example@mail.com"
						class="form-control input-md"
						pattern="<%=RegexConstants.EMAIL_PATTERN%>"
						oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>','<fmt:message key='error.js.email'/>');"
						oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>','<fmt:message key='error.js.email'/>');"
						required>

					<ctf:message type="warning" title="${warning_span}"
						key="${error.errror_reg_email}" />
				</div>
			</div>

			<fmt:message key="table.registrations.password.msg"
				var="pass_errot_msg" />

			<!-- Password input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="password1"><fmt:message
						key="table.registrations.password" /></label>
				<div class="col-md-2">
					<input id="password1" name="password1" type="password"
						maxlength="20" placeholder="" class="form-control input-md"
						onchange="checkPasswords('${pass_errot_msg}');" required>
					<span class="help-block"><fmt:message
							key="table.registrations.password.help" /></span>
				</div>
			</div>

			<!-- Password input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="password2"><fmt:message
						key="table.registrations.password.conf" /></label>
				<div class="col-md-2">
					<input id="password2" name="password2" type="password"
						maxlength="20" placeholder="" class="form-control input-md"
						onchange="checkPasswords('${pass_errot_msg}');">
				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="name"><fmt:message
						key="table.registrations.name" /></label>
				<div class="col-md-2">
					<input id="name" name="nameUser" type="text" placeholder=""
						value="${form_bean.name}" class="form-control input-md"
						oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						required>

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="surname"><fmt:message
						key="table.registrations.surname" /></label>
				<div class="col-md-2">
					<input id="surname" name="surname" type="text" placeholder=""
						value="${form_bean.surname}" class="form-control input-md"
						oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						required>
				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="patronomic"><fmt:message
						key="table.registrations.patronomic" /></label>
				<div class="col-md-2">
					<input id="patronomic" name="patronomic" type="text" placeholder=""
						value="${form_bean.patronomic}" class="form-control input-md"
						oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						required>

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="passportID"><fmt:message
						key="table.registrations.passport" /></label>
				<div class="col-md-2">
					<input id="passportID" name="passportID" type="text"
						pattern="<%=RegexConstants.PASSPORT_PATTERN%>"
						placeholder="EH 000000" class="form-control input-md"
						value="${form_bean.passportID}"
						oninvalid="setCustomValidity('<fmt:message key='error.js.not_pattern'/>');"
						onchange="setCustomValidity('');" required>

					<ctf:message type="warning" title="${warning_span}"
						key="${error.error_reg_passpotr}" />
						
					<ctf:message type="error" title="${error_span}"
						key="${error.error_passport_incorrect}" />
				</div>
			</div>

			<!-- Textarea -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="adress"><fmt:message
						key="table.registrations.adress" /></label>
				<div class="col-md-3">
					<textarea class="form-control" id="adress" name="adress"
						oninvalid="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						oninput="InvalidMsg(this,'<fmt:message key='error.js.empty'/>');"
						required>${form_bean.adress}</textarea>
				</div>
			</div>

			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="singlebutton"></label>
				<div class="col-md-4">

					<!-- reCAPTCHA -->
					<div align="left" class="g-recaptcha"
						data-sitekey="${initParam['SiteKey']}"></div>

					<ctf:message type="warning" title="${warning_span}"
						key="${error.error_capcha}" />
					<br>


					<button type="submit" id="singlebutton" name="singlebutton"
						class="btn btn-success">
						<fmt:message key="table.submit" />
					</button>
				</div>
			</div>

		</fieldset>
	</form>


	<!-- reCAPTCHA Libary -->
	<script
		src='https://www.google.com/recaptcha/api.js?hl=${pageContext.request.locale}'></script>
	<%@ include file="/WEB-INF/jspf/form/footer.jspf"%>
	<script type="text/javascript"
		src='<c:url value="/resources/js/checkPasswords.js" />'></script>

</body>
</html>