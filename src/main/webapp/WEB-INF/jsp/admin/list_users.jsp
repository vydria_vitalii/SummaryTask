<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<fmt:message key="title.list.users" var="title" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body onload="">
	<%@ include file="/WEB-INF/jspf/banner.jspf"%>
	<%@ include file="/WEB-INF/jspf/menu/menu.jspf"%>

	<%@ include file="/WEB-INF/jspf/table/list_users.jspf"%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>

	<script type="text/javascript">
		table("./ajaxListUsersServlet", [-1],[
		                  		            { "data": 1 },
		                		            { "data": 2 },
		                		            { "data": 3 },
		                		            { "data": 4 },
		                		            { "data": null, render: function ( data, type, row ) {
												return row[5] === 0 ? "" : row[5];		                		            		
	                		            		}
	                		            	},
		                		            { "data": 6 },
		                		            { "data": 7 },
		                		            { "data": 8 },
		                		            { "date": null, render: function ( data, type, row ) {
		                		            	var clazz = (row[9] === false) ? "glyphicon glyphicon-remove-circle" : "glyphicon glyphicon-ok-circle";
		                		            	return "<a href='blockUser?id=" + row[0] + "&deleted=" + (!row[9]) + "'><span class='" + clazz +
		                		            			"'></span></a>"; }
		                		            }
		                		          ]
		,[ {
            text: "<span class='glyphicon glyphicon-plus'></span>",
            action: function ( e, dt, node, config ) {
            	$(location).attr('href', './addEmployee');
            }
        }, {
			extend : 'excelHtml5',
			text : '<i class="fa fa-file-excel-o"></i>',
			titleAttr : 'Excel'
		}, {
			extend : 'csvHtml5',
			text : '<i class="fa fa-file-text-o"></i>',
			titleAttr : 'CSV'
		}, {
			extend : 'pdfHtml5',
			text : '<i class="fa fa-file-pdf-o"></i>',
			titleAttr : 'PDF'
		}]);
	</script>

</body>
</html>
