<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<head>
<fmt:message key="title.add.employee" var="title" />
<%@ include file="/WEB-INF/jspf/form/head.jspf"%>
</head>
<body>
	<%@ include file="/WEB-INF/jspf/form/add_employee.jspf"%>
	
	
	
	<!-- reCAPTCHA Libary -->
	<script
		src='https://www.google.com/recaptcha/api.js?hl=${pageContext.request.locale}'></script>
	<%@ include file="/WEB-INF/jspf/form/footer.jspf"%>
</body>
</html>