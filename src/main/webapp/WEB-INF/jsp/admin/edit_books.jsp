<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<head>
<fmt:message key="title.add.books" var="title" />

<%@ include file="/WEB-INF/jspf/form/head.jspf"%>

<link href='<c:url value="/resources/css/table.css"/>' rel="stylesheet"
	type="text/css" />

</head>
<body>

	<%@ include file="/WEB-INF/jspf/form/books.jspf"%>

	<%@ include file="/WEB-INF/jspf/form/footer.jspf"%>

	<script type="text/javascript"
		src="http://botmonster.com/jquery-bootpag/jquery.bootpag.js"></script>
	
	<script type="text/javascript">
		const KEY = '<c:out value='${initParam.Google_Books_API_Key}'></c:out>';
		const HREF = "editBooksCommand?id=" + '<c:out value='${param.id}'></c:out>';
	</script>

	<script type="text/javascript"
		src='<c:url value="/resources/js/booksSearch.js" />'></script>

</body>
</html>