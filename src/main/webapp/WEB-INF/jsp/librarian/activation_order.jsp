<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<head>
<fmt:message key="title.order.activation" var="title" />

<%@ include file="/WEB-INF/jspf/form/head.jspf"%>
</head>
<body>
	<%@ include file="/WEB-INF/jspf/form/activation_order.jspf"%>

	<%@ include file="/WEB-INF/jspf/form/footer.jspf"%>

	<script type="text/javascript"
		src='<c:url value="/resources/js/invalid_msg.js" />'></script>

	<script src="<c:url value='/resources/js/calendar.js'/>"></script>

	<script>
		calendar("calendar", 3);
	</script>

</body>
</html>