<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/locale.jspf"%>

<html>
<fmt:message key="title.list.orders" var="title" />
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<link href='<c:url value="/resources/css/child_rows.css" />' rel="stylesheet">
<body>
	<%@ include file="/WEB-INF/jspf/banner.jspf"%>
	<%@ include file="/WEB-INF/jspf/menu/menu.jspf"%>
	
	
	<%@ include file="/WEB-INF/jspf/table/list_all_orders.jspf"%>

	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<script type="text/javascript" src='<c:url value="/resources/js/detailsTable.js" />'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/tableSearch.js" />'></script>	

	
	<script type="text/javascript">
	/* Formatting function for row details - modify as you need */
	function format (d) {
		var img = d[2][1][7];
		
		if(img !== "") {
     		img = "<font style='font-size:1.3em'><image src='" +d[2][1][7] + "'/></font> ";
     	}
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
	        '<tr>'+
	            '<td><fmt:message key="title.book.subtitle" />:</td>'+
	            '<td>'+d[2][1][2]+'</td>'+
	        '</tr>'+
	        '<tr>'+
	            '<td><fmt:message key="title.book.descriptions" />:</td>'+
	            '<td>'+d[2][1][3]+'</td>'+
	        '</tr>'+
	        '<tr>'+
	            '<td><fmt:message key="title.book.language" />:</td>'+
	            '<td>' + d[2][1][8] + '</td>'+
	        '</tr>'+
	        '<tr>'+
           		 '<td><fmt:message key="title.book.pages" />:</td>'+
            	'<td>'+d[2][1][9]+'</td>'+
        	'</tr>'+
        	'<tr>'+
      		 	'<td><fmt:message key="title.book.image" />:</td>'+
       			'<td>' + img + '</td>'+
   			'</tr>'+       
   			'<tr>'+
  		 		'<td>isbn 10:</td>'+
   				'<td>' + d[2][1][10] + '</td>'+
			'</tr>'+
			'<tr>'+
		 		'<td>isbn 13:</td>'+
				'<td>' + d[2][1][11] + '</td>'+
			'</tr>'+
			'<tr>'+
	 			'<td><fmt:message key="title.book.other_identifiers" />:</td>'+
				'<td>' + d[2][1][12] + '</td>'+
			'</tr>'+
			'<tr>'+
 				'<td><fmt:message key="title.order.status" />:</td>'+
				'<td>' + d[6] + '</td>'+
			'</tr>'+
			'<tr>'+
 				'<td><fmt:message key="title.user" />:</td>'+
				'<td>' + d[1][2] + " " + d[1][3] + " " + d[1][4] + '</td>'+
			'</tr>'+
			'<tr>'+
				'<td><fmt:message key="title.user.passport" />:</td>'+
				'<td>' + d[1][6] + '</td>'+
			'</tr>'+
			'<tr>'+
				'<td><fmt:message key="title.user.address" />:</td>'+
				'<td>' + d[1][7] + '</td>'+
			'</tr>'+
			'<tr>'+
				'<td><fmt:message key="title.order.date_in" />:</td>'+
				'<td>' + d[3].substring(0,16) + '</td>'+
			'</tr>'+
			'<tr>'+
				'<td><fmt:message key="title.order.date_out" />:</td>'+
				'<td>' + d[4] + '</td>'+
			'</tr>'+
			'<tr>'+
				'<td><fmt:message key="title.order.fine" />:</td>'+
				'<td>' + d[5] + '</td>'+
			'</tr>'+
   		'</table>';
		}
	
	
	var sort = false;
	var column = [ {
		"className":      'details-control',
		"orderable":      false,
		"data":           null,
		"defaultContent": ''
	},
	 { "data": null, render: function ( data, type, row ) {
		 return row[2][1][1]; }
    },
    { "data": null, render: function ( data, type, row ) {
		 return row[2][1][4]; }
    },
    { "data": null, render: function ( data, type, row ) {
		 return row[2][1][5]; }
    },
    { "data": null, render: function ( data, type, row ) {
		 return row[2][1][6]; }
    } ];
	<c:if test ="${role eq 'librarian'}">
		sort = [-1];
		column
			.push( {
				"data": null, render: function ( data, type, row ) {
	        	 var done = "";  
		        	if(row[6] === 'active') {
		        	 done = "<a href='doneOrder?id=" + row[0] + 
							 "'><span class='glyphicon glyphicon-home'></span>";
		        	}														 
		        	return done; }
		        });
	</c:if>
	
	
	 	
	var table = table("./ajaxListAllOrders", sort,column
		,[
		  {
			extend : 'excelHtml5',
			text : '<i class="fa fa-file-excel-o"></i>',
			titleAttr : 'Excel'
		}, {
			extend : 'csvHtml5',
			text : '<i class="fa fa-file-text-o"></i>',
			titleAttr : 'CSV'
		}, {
			extend : 'pdfHtml5',
			text : '<i class="fa fa-file-pdf-o"></i>',
			titleAttr : 'PDF'
		}]);
		
		detailsTable("#table", table, "details-control", format);
	</script>

</body>
</html>