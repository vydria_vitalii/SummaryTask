<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:choose>
	<c:when test="${empty user || empty role}">
		<c:set var="login" value="false" scope="session"/>
	</c:when>
	<c:otherwise>
		<c:set var="login" value="true" scope="session"/>
	</c:otherwise>
</c:choose>