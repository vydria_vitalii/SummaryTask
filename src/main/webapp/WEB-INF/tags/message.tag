<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ attribute name="type" required="true" rtexprvalue="true"
	type="java.lang.String"%>

<%@ attribute name="title" required="true" rtexprvalue="true"
	type="java.lang.String"%>

<%@ attribute name="key" required="true" rtexprvalue="true"
	type="java.lang.String"%>

<c:if test="${not empty key}" >
	<div class="alert-box ${type}">
		<span> 
			<c:out value="${title}: " />
		</span>
		<fmt:message key="${key}" />
	</div>
</c:if>
