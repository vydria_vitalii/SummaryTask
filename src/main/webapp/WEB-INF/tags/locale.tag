<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:set var="currentLocale" value="${pageContext.request.locale}" />
<c:set var="locales"
	value="${pageContext.request.locales}" />

<select id="lang-select" name="lang-select" onchange="changeLang(this)">
	<c:forEach items="${locales}" var="locale">
		<option value="${locale}"
			${(currentLocale eq locale) ? 'selected' : ''}>
			${locale.getDisplayLanguage(locale).toUpperCase()}</option>
	</c:forEach>
</select>

<script type="text/javascript" src="<c:url value='/resources/js/lang.js'/>"></script>
