package ua.nure.vydrya.SummaryTask4.quartz.job.fine;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.service.OrderService;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class FineJob implements Job {
	private static final Logger LOG = Logger.getLogger(FineJob.class);

	private OrderService orderService;
	private int repeatCount;
	private long timeWait;

	private void init(JobExecutionContext context) {
		LOG.debug("init start");
		context.getMergedJobDataMap();
		LOG.debug("init done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.debug("execute start");
		init(context);
		int count = repeatCount;
		while (true) {
			try {
				orderService.fineAccrual();
				break;
			} catch (TransactionException e) {
				try {
					Thread.sleep(timeWait);
				} catch (InterruptedException e1) {
					LOG.warn("Error sleep Thread", e1);
					throw new JobExecutionException("Error sleep Thread", e);
				}
				if (--count <= 0) {
					throw new JobExecutionException("Error execute fine accrual", e);
				}
			}
		}
		LOG.debug("execute done");
	}

	/**
	 * @return the orderService
	 */
	public OrderService getOrderService() {
		return orderService;
	}

	/**
	 * @param orderService
	 *            the orderService to set
	 */
	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	/**
	 * @return the repeatCount
	 */
	public int getRepeatCount() {
		return repeatCount;
	}

	/**
	 * @param repeatCount
	 *            the repeatCount to set
	 */
	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	/**
	 * @return the timeWait
	 */
	public long getTimeWait() {
		return timeWait;
	}

	/**
	 * @param timeWait
	 *            the timeWait to set
	 */
	public void setTimeWait(long timeWait) {
		this.timeWait = timeWait;
	}
}
