package ua.nure.vydrya.SummaryTask4.web.converter;

import java.io.Serializable;

/**
 * Converter interface.
 * 
 * @param <T>
 *            - is convert.
 * @param <E>
 *            - to convert.
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface Converter<T extends Serializable, E extends Serializable> {
	/**
	 * Converts bean to entity.
	 *
	 * @param t
	 *            the bean.
	 * @return the entity.
	 */
	E convert(T t);
}
