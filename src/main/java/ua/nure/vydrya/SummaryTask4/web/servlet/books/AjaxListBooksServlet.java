package ua.nure.vydrya.SummaryTask4.web.servlet.books;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.DTService;
import ua.nure.vydrya.SummaryTask4.util.constants.AjaxConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.servlet.admin.AjaxListUsersServlet;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AjaxListBooksServlet extends DependencyInjectionServlet {
	private static final long serialVersionUID = -4720924371827258297L;
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(AjaxListUsersServlet.class);

	@Inject("DTService")
	private DTService service;

	@Inject("DTExtractor")
	private WebExtractor<DTBean> extractor;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {
			@Override
			public void execute() throws Exception {
				LOG.debug("operation start");
				DTBean bean = extractor.extract(req);
				String res = service.getDataTableResponseBooks(bean);
				LOG.info("result: " + res);
				resp.setContentType(AjaxConstants.CONTENT_TYPE);
				resp.setCharacterEncoding(AjaxConstants.CHARACTER_ENCODING);
				resp.getWriter().print(res);
				LOG.debug("operation start");
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doGet done");
	}

	// /**
	// * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	// * response)
	// */
	// @Override
	// protected void doPost(HttpServletRequest request, HttpServletResponse
	// response)
	// throws ServletException, IOException {
	// LOG.debug("doPost start");
	// doGet(request, response);
	// LOG.debug("doPost done");
	// }

}
