package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ParamIdValidator extends Validator<Boolean, String> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public Boolean validation(String e) {
		return isValidByPattern(e, RegexConstants.NUMBER_PATTERN);
	}

}
