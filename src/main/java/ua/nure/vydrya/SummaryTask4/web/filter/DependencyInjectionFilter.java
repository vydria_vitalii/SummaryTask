package ua.nure.vydrya.SummaryTask4.web.filter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.DependencyInjection;
import ua.nure.vydrya.SummaryTask4.exception.DependencyInjectionException;
import ua.nure.vydrya.SummaryTask4.util.constants.DependencyInjectionConstants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class DependencyInjectionFilter extends AbstractFilter {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(DependencyInjectionFilter.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOG.debug("init start");
		ServletContext context = filterConfig.getServletContext();
		DependencyInjection di = (DependencyInjection) context
				.getAttribute(DependencyInjectionConstants.DEPENDENCY_INJECTION);
		try {
			di.inject(this.getClass(), DependencyInjectionFilter.class, this);
		} catch (DependencyInjectionException e) {
			LOG.error("error inject", e);
			throw new ServletException("error inject", e);
		}
		LOG.debug("init done");
	}
}
