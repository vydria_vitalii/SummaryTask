package ua.nure.vydrya.SummaryTask4.web.servlet.user;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.util.constants.Pages;

/**
 * Servlet implementation class LogoutServlet
 */
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = -5260511781288303624L;
	private static final Logger LOG = Logger.getLogger(LogoutServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		HttpSession session = request.getSession();
		if (Objects.nonNull(session)) {
			session.invalidate();
		}
		response.sendRedirect(Pages.INDEX);
		LOG.debug("doGet done");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}
}
