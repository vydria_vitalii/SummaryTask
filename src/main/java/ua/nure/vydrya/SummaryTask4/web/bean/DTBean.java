/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ua.nure.vydrya.SummaryTask4.repository.entity.User;

/**
 * @author vydrya_vitaliy.
 *
 */
public class DTBean implements Serializable {
	private static final long serialVersionUID = -1334286895851164236L;
	private User user;
	private int displayStart;
	private int displayLength;
	private int echo;

	private int sortingCols = 1;
	private int columns;

	private String globalSearch;

	private List<Integer> sCols = new ArrayList<>();
	private List<String> sDirs = new ArrayList<>();
	private List<String> sSearchs = new ArrayList<>();

	public List<Integer> getCols() {
		if (Objects.isNull(sCols)) {
			sCols = new ArrayList<>();
		}
		return sCols;
	}

	public List<String> getDirs() {
		if (Objects.isNull(sDirs)) {
			sDirs = new ArrayList<>();
		}
		return sDirs;
	}

	public List<String> getSearchs() {
		if (Objects.isNull(sSearchs)) {
			sSearchs = new ArrayList<>();
		}
		return sSearchs;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the displayStart
	 */
	public int getDisplayStart() {
		return displayStart;
	}

	/**
	 * @param displayStart
	 *            the displayStart to set
	 */
	public void setDisplayStart(int displayStart) {
		this.displayStart = displayStart;
	}

	/**
	 * @return the displayLength
	 */
	public int getDisplayLength() {
		return displayLength;
	}

	/**
	 * @param displayLength
	 *            the displayLength to set
	 */
	public void setDisplayLength(int displayLength) {
		this.displayLength = displayLength;
	}

	/**
	 * @return the echo
	 */
	public int getEcho() {
		return echo;
	}

	/**
	 * @param echo
	 *            the echo to set
	 */
	public void setEcho(int echo) {
		this.echo = echo;
	}

	/**
	 * @return the sortingCols
	 */
	public int getSortingCols() {
		return sortingCols;
	}

	/**
	 * @param sortingCols
	 *            the sortingCols to set
	 */
	public void setSortingCols(int sortingCols) {
		this.sortingCols = sortingCols;
	}

	/**
	 * @return the columns
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	public void setColumns(int columns) {
		this.columns = columns;
	}

	/**
	 * @return the globalSearch
	 */
	public String getGlobalSearch() {
		return globalSearch;
	}

	/**
	 * @param globalSearch
	 *            the globalSearch to set
	 */
	public void setGlobalSearch(String globalSearch) {
		this.globalSearch = globalSearch;
	}

	/**
	 * @return the sCols
	 */
	public List<Integer> getsCols() {
		return sCols;
	}

	/**
	 * @param sCols
	 *            the sCols to set
	 */
	public void setsCols(List<Integer> sCols) {
		this.sCols = sCols;
	}

	/**
	 * @return the sDirs
	 */
	public List<String> getsDirs() {
		return sDirs;
	}

	/**
	 * @param sDirs
	 *            the sDirs to set
	 */
	public void setsDirs(List<String> sDirs) {
		this.sDirs = sDirs;
	}

	/**
	 * @return the sSearchs
	 */
	public List<String> getsSearchs() {
		return sSearchs;
	}

	/**
	 * @param sSearchs
	 *            the sSearchs to set
	 */
	public void setsSearchs(List<String> sSearchs) {
		this.sSearchs = sSearchs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + columns;
		result = prime * result + displayLength;
		result = prime * result + displayStart;
		result = prime * result + echo;
		result = prime * result + ((globalSearch == null) ? 0 : globalSearch.hashCode());
		result = prime * result + ((sCols == null) ? 0 : sCols.hashCode());
		result = prime * result + ((sDirs == null) ? 0 : sDirs.hashCode());
		result = prime * result + ((sSearchs == null) ? 0 : sSearchs.hashCode());
		result = prime * result + sortingCols;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DTBean)) {
			return false;
		}
		DTBean other = (DTBean) obj;
		if (columns != other.columns) {
			return false;
		}
		if (displayLength != other.displayLength) {
			return false;
		}
		if (displayStart != other.displayStart) {
			return false;
		}
		if (echo != other.echo) {
			return false;
		}
		if (globalSearch == null) {
			if (other.globalSearch != null) {
				return false;
			}
		} else if (!globalSearch.equals(other.globalSearch)) {
			return false;
		}
		if (sCols == null) {
			if (other.sCols != null) {
				return false;
			}
		} else if (!sCols.equals(other.sCols)) {
			return false;
		}
		if (sDirs == null) {
			if (other.sDirs != null) {
				return false;
			}
		} else if (!sDirs.equals(other.sDirs)) {
			return false;
		}
		if (sSearchs == null) {
			if (other.sSearchs != null) {
				return false;
			}
		} else if (!sSearchs.equals(other.sSearchs)) {
			return false;
		}
		if (sortingCols != other.sortingCols) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DTBean [");
		if (user != null) {
			builder.append("user=");
			builder.append(user);
			builder.append(", ");
		}
		builder.append("displayStart=");
		builder.append(displayStart);
		builder.append(", displayLength=");
		builder.append(displayLength);
		builder.append(", echo=");
		builder.append(echo);
		builder.append(", sortingCols=");
		builder.append(sortingCols);
		builder.append(", columns=");
		builder.append(columns);
		builder.append(", ");
		if (globalSearch != null) {
			builder.append("globalSearch=");
			builder.append(globalSearch);
			builder.append(", ");
		}
		if (sCols != null) {
			builder.append("sCols=");
			builder.append(sCols);
			builder.append(", ");
		}
		if (sDirs != null) {
			builder.append("sDirs=");
			builder.append(sDirs);
			builder.append(", ");
		}
		if (sSearchs != null) {
			builder.append("sSearchs=");
			builder.append(sSearchs);
		}
		builder.append("]");
		return builder.toString();
	}
}
