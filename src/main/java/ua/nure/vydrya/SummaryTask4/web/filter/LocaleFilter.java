package ua.nure.vydrya.SummaryTask4.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.locale.LocaleStorage;
import ua.nure.vydrya.SummaryTask4.service.LocaleService;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;

/**
 * Servlet Filter implementation class LocaleFilter
 */
public class LocaleFilter extends DependencyInjectionFilter {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(LocaleFilter.class);

	@Inject("LocaleStorage")
	private LocaleStorage localeStorage;

	@Inject("LocaleService")
	private LocaleService localeService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.filter.DependencyInjectionFilter#doFilter
	 * (javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("doFilter start");
		Locale locale = getLocale(req);
		LOG.info("locale: " + locale);

		localeStorage.sendLocale(locale, req, resp, WebConstants.LOCALE,
				localeService.getMaxAgeLocale());

		HttpServletRequestWrapperImpl impl = new HttpServletRequestWrapperImpl(req);
		impl.setLocale(locale);
		impl.setLocales(localeService.getAvailableLocales());

		chain.doFilter(impl, resp);
		LOG.debug("doFilter done");
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		LOG.debug("destroy start");
		LOG.debug("destroy done");
	}

	private Locale getLocale(final HttpServletRequest req) {
		LOG.debug("getLocale start");
		final String lang = req.getParameter(WebConstants.LANG);
		if (!StringUtils.isEmpty(lang) && localeService.isSupported(lang)) {
			LOG.debug("getLocale done");
			return new Locale(lang);
		}
		Locale locale = localeStorage.getLocale(req, WebConstants.LOCALE);
		if (Objects.isNull(locale)) {
			locale = localeService.getMostSuitableLocale(req.getLocales());
		}
		LOG.debug("getLocale done");
		return locale;
	}

	private class HttpServletRequestWrapperImpl extends HttpServletRequestWrapper {
		private Locale locale;
		private Set<Locale> locales;

		/**
		 * @param request
		 */
		public HttpServletRequestWrapperImpl(HttpServletRequest request) {
			super(request);
			locales = new HashSet<>();
		}

		public void setLocale(final Locale locale) {
			this.locale = locale;
		}

		public void setLocales(final Set<Locale> locales) {
			this.locales = locales;
		}

		public void addLocale(final Locale locale) {
			locales.add(locale);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.servlet.ServletRequestWrapper#getLocale()
		 */
		@Override
		public Locale getLocale() {
			if (Objects.nonNull(locale)) {
				return locale;
			}
			return super.getLocale();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.servlet.ServletRequestWrapper#getLocales()
		 */
		@Override
		public Enumeration<Locale> getLocales() {
			if (Objects.nonNull(locales) && !locales.isEmpty()) {
				return Collections.enumeration(locales);
			} else if (Objects.nonNull(locale)) {
				List<Locale> list = new ArrayList<>();
				list.add(locale);
				return Collections.enumeration(list);
			}
			return super.getLocales();
		}
	}
}
