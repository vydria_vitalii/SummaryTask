package ua.nure.vydrya.SummaryTask4.web.servlet.books;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.service.BooksService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.bean.ExemplarBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class EditBooksServlet extends DependencyInjectionServlet {
	private static final long serialVersionUID = -3719572337744117781L;
	private static final Logger LOG = Logger.getLogger(EditBooksServlet.class);

	@Inject("EditBooksExtractor")
	private WebExtractor<ExemplarBean> extractor;

	@Inject("AddBooksValidator")
	private Validator<ErrorHolder, Books> validator;

	@Inject("BooksService")
	private BooksService booksService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");
				ExemplarBean exemplar = extractor.extract(req);
				ErrorHolder error = validator.validation(exemplar.getBooks());
				if (error.isEmpty()) {
					LOG.info("Edit Books result:" + booksService.editBooksExemplar(exemplar));
					sendRedirect(Pages.LIST_EXEMPLARS, resp);
					LOG.debug("execute done");
					return;
				}
				setError(req, error);
				sendRedirect(Pages.EDIT_BOOKS + "?id=" + exemplar.getIdExemplar(), resp);
				LOG.debug("execute done");
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doGet done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(req, resp);
		LOG.debug("doPost done");
	}
}
