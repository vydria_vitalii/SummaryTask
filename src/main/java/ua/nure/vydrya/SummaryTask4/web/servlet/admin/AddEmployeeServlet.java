package ua.nure.vydrya.SummaryTask4.web.servlet.admin;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.service.AdminService;
import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.bean.AddUserBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.servlet.user.RegistrationsUserServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * Servlet implementation class AddEmployeeServlet
 */
public class AddEmployeeServlet extends DependencyInjectionServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(RegistrationsUserServlet.class);
	private static final long serialVersionUID = -1241551287054218571L;

	@Inject("AdminService")
	private AdminService adminService;

	@Inject("AddEmployeeExtractor")
	private WebExtractor<AddUserBean> extractor;

	@Inject("AddUserBeanValidator")
	private Validator<ErrorHolder, AddUserBean> validator;

	@Inject("AddUserBeanToUserConverter")
	private Converter<AddUserBean, User> converter;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");

				AddUserBean bean = extractor.extract(req);
				ErrorHolder error = validator.validation(bean);

				if (error.isEmpty()) {
					User user = converter.convert(bean);
					user = adminService.addUser(user);
					if (Objects.nonNull(user)) {
						final boolean flagEmail = Objects.nonNull(user.getEmail());
						final boolean flagPassport = Objects.nonNull(user.getPassportID());
						if (flagEmail && flagPassport) {
							LOG.info("add user user: " + user);
							sendRedirect(Pages.LIST_USERS, resp);
							LOG.debug("execute done");
							return;
						}
						if (!flagEmail) {
							error.add(ErrorConstants.ERROR_REGISTARTIONS_EMAIL,
									ErrorConstants.ERROR_REGISTARTIONS_EMAIL_KEY);
						}
						if (!flagPassport) {
							error.add(ErrorConstants.ERROR_REGISTARTIONS_PASSPORT,
									ErrorConstants.ERROR_REGISTARTIONS_PASSPORT_KEY);
						}
					}
				} else {
					error.add(ErrorConstants.ERROR_ADD_EMPLOYEE, ErrorConstants.ERROR_ADD_EMPLOYEE);
				}
				setError(req, error, bean);
				sendRedirect(Pages.ADD_EMPLOYEE, resp);
				LOG.debug("execute done");
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doGet done");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}
}
