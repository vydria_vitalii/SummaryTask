package ua.nure.vydrya.SummaryTask4.web.servlet.user;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.service.UserService;
import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.bean.UserLoginBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends DependencyInjectionServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(LoginServlet.class);
	private static final long serialVersionUID = 4723324714763775556L;

	@Inject("UserLoginBeanExtractor")
	private WebExtractor<UserLoginBean> extractor;

	@Inject("LoginUserBeanValidator")
	private Validator<ErrorHolder, UserLoginBean> validator;

	@Inject("UserService")
	private UserService userService;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		ServletOperation operation = new ServletOperation() {
			@Override
			public void execute() throws Exception {
				LOG.debug("execupe start");
				UserLoginBean userLoginBean = extractor.extract(req);
				ErrorHolder error = validator.validation(userLoginBean);
				if (error.isEmpty()) {
					User user = userService.login(userLoginBean);
					if (Objects.nonNull(user)) {
						HttpSession session = req.getSession(true);
						LOG.info("login user: " + user);
						session.setAttribute(WebConstants.USER_ID, user.getId());
						session.setAttribute(WebConstants.USER, user);
						session.setAttribute(WebConstants.ROLE, user.getRole().value());
						LOG.debug("execupe done");
						sendRedirect(Pages.INDEX, resp);
						return;
					} else {
						error.add(ErrorConstants.ERROR_NOT_USER, ErrorConstants.ERROR_NOT_USER_KEY);
					}
				}
				LOG.trace("not user:" + userLoginBean.toString());
				userLoginBean.setPassword("");
				setError(req, error, userLoginBean);
				sendRedirect(Pages.LOGIN, resp);
				LOG.debug("execupe done");
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doPost done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		doPost(req, resp);
		LOG.debug("doGet done");
	}

}
