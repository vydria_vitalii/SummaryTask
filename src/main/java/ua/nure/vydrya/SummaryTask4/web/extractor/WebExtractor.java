package ua.nure.vydrya.SummaryTask4.web.extractor;

import javax.servlet.http.HttpServletRequest;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface WebExtractor<T> {
	T extract(HttpServletRequest req);
}
