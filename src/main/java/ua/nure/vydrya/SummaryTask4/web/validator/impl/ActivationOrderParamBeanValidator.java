package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ActivationOrderParamBeanValidator
		extends Validator<ErrorHolder, ActivationOrderParamBean> {
	private static final int PLUS_DAY_TO_CURRENT = 3;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public ErrorHolder validation(ActivationOrderParamBean e) {
		ErrorHolder error = new ErrorHolder();

		final boolean validDate = isValidByPattern(e.getDateOut(), RegexConstants.DATE_PATTERN);
		if (validDate) {
			Date currentDate = new Date();
			try {
				Date dateForm = DateUtils.parseDate(e.getDateOut(), WebConstants.DATE_FORMAT);
				dateForm = DateUtils.addDays(dateForm, PLUS_DAY_TO_CURRENT);
				long time = dateForm.getTime() - currentDate.getTime();
				if (time < 0) {
					error.add(ErrorConstants.ERROR_ORDER_DATE_OUT,
							ErrorConstants.ERROR_ORDER_DATE_OUT_KEY);
				}
			} catch (ParseException e1) {
				throw new IllegalArgumentException(e1);
			}
		} else {
			error.add(ErrorConstants.ERROR_ORDER_DATE_OUT, ErrorConstants.ERROR_ORDER_DATE_OUT_KEY);
		}
		return error;
	}
}
