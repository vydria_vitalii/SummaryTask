package ua.nure.vydrya.SummaryTask4.web.servlet.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.OrderService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderBean;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * Servlet implementation class OpenOrderServlet
 */
public class OpenOrderServlet extends DependencyInjectionServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(OpenOrderServlet.class);
	private static final long serialVersionUID = 345098906748067502L;

	@Inject("OpenOrderParamBeanExtractor")
	private WebExtractor<OpenOrderParamBean> extractor;

	@Inject("OpenOrderParamBeanValidator")
	private Validator<ErrorHolder, OpenOrderParamBean> validator;

	@Inject("OpenOrderParamBeanToOpenOrderBeanConverter")
	private Converter<OpenOrderParamBean, OpenOrderBean> converter;

	@Inject("OrderService")
	private OrderService orderService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doPost satrt");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute satrt");
				OpenOrderParamBean bean = extractor.extract(req);
				LOG.trace("OpenOrderParamBean: " + bean);
				ErrorHolder error = validator.validation(bean);
				if (error.isEmpty()) {
					OpenOrderBean orderBean = converter.convert(bean);
					LOG.info("Open Order: " + orderService.openOrder(orderBean));
				}
				LOG.info("Error: " + error);
				LOG.debug("execute done");
				sendRedirect(Pages.INDEX, resp);
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doPost done");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}
}