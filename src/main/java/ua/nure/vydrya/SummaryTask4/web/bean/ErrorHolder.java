package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Error holder implementation.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ErrorHolder implements Serializable {
	private static final long serialVersionUID = -6301626597388759951L;
	private Map<String, String> errors = new HashMap<>();

	/**
	 * Adds the error.
	 *
	 * @param errorKey
	 *            the error key.
	 * @param formKey
	 *            the name error.
	 */
	public void add(String formKey, String errorKey) {
		errors.put(formKey, errorKey);
	}

	/**
	 * Adds all errors.
	 *
	 * @param errorHolder
	 *            error holder
	 */
	public void addAll(ErrorHolder errorHolder) {
		errors.putAll(errorHolder.getErrors());
	}

	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return errors.isEmpty();
	}

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public Map<String, String> getErrors() {
		return errors;
	}

	/**
	 * Clears error holder.
	 */
	public void clear() {
		errors.clear();
	}

	/**
	 * Getter message error by name.
	 * 
	 * @param nameError
	 *            - name error.
	 * @return the message error or null;
	 */
	public String getError(String nameError) {
		return errors.get(nameError);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((errors == null) ? 0 : errors.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ErrorHolder)) {
			return false;
		}
		ErrorHolder other = (ErrorHolder) obj;
		if (errors == null) {
			if (other.errors != null) {
				return false;
			}
		} else if (!errors.equals(other.errors)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ErrorHolder [");
		if (errors != null) {
			builder.append("errors=");
			builder.append(errors);
		}
		builder.append("]");
		return builder.toString();
	}
}