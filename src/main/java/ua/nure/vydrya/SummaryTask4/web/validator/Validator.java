package ua.nure.vydrya.SummaryTask4.web.validator;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Bean validator interface.
 *
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class Validator<T, E> {
	/**
	 * Validate user form bean.
	 *
	 * @param e
	 *            the form bean
	 * @return the error holder
	 */
	public abstract T validation(E e);

	protected boolean isValidByPattern(final String input, final String regEx) {
		return !(StringUtils.isEmpty(input) || !Pattern.matches(regEx, input));
	}

}