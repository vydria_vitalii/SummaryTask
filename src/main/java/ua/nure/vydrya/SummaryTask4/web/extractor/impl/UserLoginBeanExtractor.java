/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.UserLoginBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class UserLoginBeanExtractor implements WebExtractor<UserLoginBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public UserLoginBean extract(HttpServletRequest req) {
		UserLoginBean loginBean = new UserLoginBean();
		loginBean.setLogin(req.getParameter(WebConstants.EMAIL));
		loginBean.setPassword(req.getParameter(WebConstants.PASSWORD));
		loginBean.setCaptca(req.getParameter(WebConstants.PARAM_RECAPTCHA));
		return loginBean;
	}

}
