package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class CancelOrderExtractor implements WebExtractor<Long> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public Long extract(HttpServletRequest req) {
		return Long.valueOf(req.getParameter(WebConstants.ID));
	}

}
