package ua.nure.vydrya.SummaryTask4.web.servlet.books;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.BooksService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;

/**
 * Servlet implementation class RemoveBooksServlet
 */
public class RemoveBooksServlet extends DependencyInjectionServlet {
	private static final Logger LOG = Logger.getLogger(RemoveBooksServlet.class);
	private static final long serialVersionUID = 7533012683574040892L;

	@Inject("RemoveBooksExtractor")
	private WebExtractor<Long> extractor;

	@Inject("BooksService")
	private BooksService booksService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");
				final Long id = extractor.extract(req);
				LOG.info("remove books:" + booksService.deletedBooksExemplar(id));
				sendRedirect(Pages.LIST_EXEMPLARS, resp);
				LOG.debug("execute done");
			}
		};
		LOG.debug("doGet doen");
		executeOperation(operation, req, resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}

}
