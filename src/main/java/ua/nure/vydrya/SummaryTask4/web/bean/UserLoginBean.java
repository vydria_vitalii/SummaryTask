/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class UserLoginBean implements Serializable {
	private static final long serialVersionUID = -2005330831282991269L;
	private String login;
	private String password;
	private String captca;

	/**
	 * Default constructor.
	 */
	public UserLoginBean() {
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the captca
	 */
	public String getCaptca() {
		return captca;
	}

	/**
	 * @param captca
	 *            the captca to set
	 */
	public void setCaptca(String captca) {
		this.captca = captca;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((captca == null) ? 0 : captca.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserLoginBean)) {
			return false;
		}
		UserLoginBean other = (UserLoginBean) obj;
		if (captca == null) {
			if (other.captca != null) {
				return false;
			}
		} else if (!captca.equals(other.captca)) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserLoginBean [");
		if (login != null) {
			builder.append("login=");
			builder.append(login);
			builder.append(", ");
		}
		if (password != null) {
			builder.append("password=");
			builder.append(password);
			builder.append(", ");
		}
		if (captca != null) {
			builder.append("captca=");
			builder.append(captca);
		}
		builder.append("]");
		return builder.toString();
	}
}
