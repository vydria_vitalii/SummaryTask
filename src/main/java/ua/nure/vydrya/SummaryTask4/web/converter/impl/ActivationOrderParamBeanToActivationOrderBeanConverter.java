package ua.nure.vydrya.SummaryTask4.web.converter.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ActivationOrderParamBeanToActivationOrderBeanConverter
		implements Converter<ActivationOrderParamBean, ActivationOrderBean> {
	private static final SimpleDateFormat SDF = new SimpleDateFormat(WebConstants.DATE_FORMAT);

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.web.converter.Converter#convert(java.io.
	 * Serializable)
	 */
	@Override
	public ActivationOrderBean convert(ActivationOrderParamBean e) {
		ActivationOrderBean bean = new ActivationOrderBean();
		bean.setIdOrder(Long.valueOf(e.getOrderId()));
		bean.setDateIn(new Timestamp(System.currentTimeMillis()));
		try {
			final Date parsedDate = SDF.parse(e.getDateOut());
			bean.setDateOut(new java.sql.Date(parsedDate.getTime()));
		} catch (ParseException e1) {
			throw new IllegalArgumentException("error parse date: " + e.getDateOut());
		}
		return bean;
	}
}
