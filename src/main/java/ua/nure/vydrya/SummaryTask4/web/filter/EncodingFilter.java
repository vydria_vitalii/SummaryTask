package ua.nure.vydrya.SummaryTask4.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.util.constants.InitParamConstants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class EncodingFilter extends AbstractFilter {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(EncodingFilter.class);
	private String characterEncoding;
	private String contentType;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOG.debug("init start");
		characterEncoding = filterConfig.getInitParameter(InitParamConstants.CHARACTER_ENCODING);
		contentType = filterConfig.getInitParameter(InitParamConstants.CONTENT_TYPE);
		LOG.debug("init done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * web.filter.AbstractFilter#doFilter(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("doFilter strat");
		req.setCharacterEncoding(characterEncoding);
		chain.doFilter(req, resp);
		resp.setContentType(contentType);
		LOG.debug("doFilter done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		LOG.debug("destroy strat");
		LOG.debug("destroy done");
	}
}
