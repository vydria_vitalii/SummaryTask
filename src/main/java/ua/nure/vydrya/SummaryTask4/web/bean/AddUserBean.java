package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AddUserBean implements Serializable {
	private static final long serialVersionUID = 8396959980132608495L;
	private String email;
	private String password;
	private String passwordConf;
	private String name;
	private String surname;
	private String patronomic;
	private String passportID;
	private String adress;
	private String role;
	private String captcha;

	/**
	 * Default constructor.
	 */
	public AddUserBean() {
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the passwordConf
	 */
	public String getPasswordConf() {
		return passwordConf;
	}

	/**
	 * @param passwordConf
	 *            the passwordConf to set
	 */
	public void setPasswordConf(String passwordConf) {
		this.passwordConf = passwordConf;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *            the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the patronomic
	 */
	public String getPatronomic() {
		return patronomic;
	}

	/**
	 * @param patronomic
	 *            the patronomic to set
	 */
	public void setPatronomic(String patronomic) {
		this.patronomic = patronomic;
	}

	/**
	 * @return the passportID
	 */
	public String getPassportID() {
		return passportID;
	}

	/**
	 * @param passportID
	 *            the passportID to set
	 */
	public void setPassportID(String passportID) {
		this.passportID = passportID;
	}

	/**
	 * @return the adress
	 */
	public String getAdress() {
		return adress;
	}

	/**
	 * @param adress
	 *            the adress to set
	 */
	public void setAdress(String adress) {
		this.adress = adress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegistrationsBean [");
		if (email != null) {
			builder.append("email=");
			builder.append(email);
			builder.append(", ");
		}
		if (password != null) {
			builder.append("password=");
			builder.append(password);
			builder.append(", ");
		}
		if (passwordConf != null) {
			builder.append("passwordConf=");
			builder.append(passwordConf);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (surname != null) {
			builder.append("surname=");
			builder.append(surname);
			builder.append(", ");
		}
		if (patronomic != null) {
			builder.append("patronomic=");
			builder.append(patronomic);
			builder.append(", ");
		}
		if (passportID != null) {
			builder.append("passportID=");
			builder.append(passportID);
			builder.append(", ");
		}
		if (adress != null) {
			builder.append("adress=");
			builder.append(adress);
			builder.append(", ");
		}
		if (role != null) {
			builder.append("role=");
			builder.append(role);
			builder.append(", ");
		}
		if (captcha != null) {
			builder.append("captcha=");
			builder.append(captcha);
		}
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the captcha
	 */
	public String getCaptcha() {
		return captcha;
	}

	/**
	 * @param captcha
	 *            the captcha to set
	 */
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adress == null) ? 0 : adress.hashCode());
		result = prime * result + ((captcha == null) ? 0 : captcha.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((passportID == null) ? 0 : passportID.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((passwordConf == null) ? 0 : passwordConf.hashCode());
		result = prime * result + ((patronomic == null) ? 0 : patronomic.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AddUserBean)) {
			return false;
		}
		AddUserBean other = (AddUserBean) obj;
		if (adress == null) {
			if (other.adress != null) {
				return false;
			}
		} else if (!adress.equals(other.adress)) {
			return false;
		}
		if (captcha == null) {
			if (other.captcha != null) {
				return false;
			}
		} else if (!captcha.equals(other.captcha)) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (passportID == null) {
			if (other.passportID != null) {
				return false;
			}
		} else if (!passportID.equals(other.passportID)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (passwordConf == null) {
			if (other.passwordConf != null) {
				return false;
			}
		} else if (!passwordConf.equals(other.passwordConf)) {
			return false;
		}
		if (patronomic == null) {
			if (other.patronomic != null) {
				return false;
			}
		} else if (!patronomic.equals(other.patronomic)) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		if (surname == null) {
			if (other.surname != null) {
				return false;
			}
		} else if (!surname.equals(other.surname)) {
			return false;
		}
		return true;
	}
}
