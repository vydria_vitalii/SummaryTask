package ua.nure.vydrya.SummaryTask4.web.servlet.books;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.service.BooksService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.servlet.user.RegistrationsUserServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * Servlet implementation class AddBooksServlet
 */
public class AddBooksServlet extends DependencyInjectionServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(RegistrationsUserServlet.class);
	private static final long serialVersionUID = -5406560718140124229L;

	@Inject("AddBooksExtractor")
	private WebExtractor<Books> extractor;

	@Inject("BooksService")
	private BooksService booksService;

	@Inject("AddBooksValidator")
	private Validator<ErrorHolder, Books> validator;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");
				Books books = extractor.extract(req);
				ErrorHolder error = validator.validation(books);
				if (error.isEmpty()) {
					booksService.addBooks(books);
					sendRedirect(Pages.LIST_EXEMPLARS, resp);
					LOG.debug("execute done");
					return;
				}
				setError(req, error);
				sendRedirect(Pages.ADD_BOOKS, resp);
				LOG.debug("execute done");
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doGet done");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}
}
