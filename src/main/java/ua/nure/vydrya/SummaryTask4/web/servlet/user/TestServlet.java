package ua.nure.vydrya.SummaryTask4.web.servlet.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.UserService;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;

/**
 * Servlet implementation class TestServlet
 */
public class TestServlet extends DependencyInjectionServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(TestServlet.class);
	private static final long serialVersionUID = 1L;

	@Inject("UserService")
	private UserService userService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				req.setAttribute("test", userService.testService());
				forvard("test.jsp", req, resp);
			}
		};

		executeOperation(operation, req, resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
