package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;

import ua.nure.vydrya.SummaryTask4.repository.entity.Books;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ExemplarBean implements Serializable {
	private static final long serialVersionUID = 4440755011773119173L;
	private long idExemplar;
	private Books books;

	public ExemplarBean() {
	}

	/**
	 * @return the idExemplar
	 */
	public long getIdExemplar() {
		return idExemplar;
	}

	/**
	 * @param idExemplar
	 *            the idExemplar to set
	 */
	public void setIdExemplar(long idExemplar) {
		this.idExemplar = idExemplar;
	}

	/**
	 * @return the books
	 */
	public Books getBooks() {
		return books;
	}

	/**
	 * @param books
	 *            the books to set
	 */
	public void setBooks(Books books) {
		this.books = books;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((books == null) ? 0 : books.hashCode());
		result = prime * result + (int) (idExemplar ^ (idExemplar >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ExemplarBean)) {
			return false;
		}
		ExemplarBean other = (ExemplarBean) obj;
		if (books == null) {
			if (other.books != null) {
				return false;
			}
		} else if (!books.equals(other.books)) {
			return false;
		}
		if (idExemplar != other.idExemplar) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExemplarBean [idExemplar=");
		builder.append(idExemplar);
		builder.append(", ");
		if (books != null) {
			builder.append("books=");
			builder.append(books);
		}
		builder.append("]");
		return builder.toString();
	}
}
