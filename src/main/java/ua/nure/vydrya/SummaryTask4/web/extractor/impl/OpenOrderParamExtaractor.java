/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class OpenOrderParamExtaractor implements WebExtractor<OpenOrderParamBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public OpenOrderParamBean extract(HttpServletRequest req) {
		OpenOrderParamBean bean = new OpenOrderParamBean();

		HttpSession session = req.getSession();
		if (Objects.nonNull(session)) {
			bean.setIdUser(Objects.toString((session.getAttribute(WebConstants.USER_ID))));
		}
		bean.setIdBook(req.getParameter(WebConstants.BOOK_ID));
		return bean;
	}
}
