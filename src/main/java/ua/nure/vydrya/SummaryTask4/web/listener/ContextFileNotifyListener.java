package ua.nure.vydrya.SummaryTask4.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import ua.nure.vydrya.SummaryTask4.util.constants.DependencyInjectionConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.InitParamConstants;

/**
 * Application Lifecycle Listener implementation class ContextFileNotifyListener
 *
 */
public class ContextFileNotifyListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(ContextFileNotifyListener.class);
	private int watchId;

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOG.debug("contextDestroyed start");

		System.load("D:/jnotify.dll");

		final ServletContext context = sce.getServletContext();
		final String path = context.getInitParameter(InitParamConstants.PATH_FINE_FILE);

		JNotifyListener listener = (JNotifyListener) context
				.getAttribute(DependencyInjectionConstants.FINE_POLICY);

		final int mask = JNotify.FILE_MODIFIED;

		try {
			watchId = JNotify.addWatch(path, mask, false, listener);
		} catch (JNotifyException e) {
			LOG.error("Error JNotify addWatch");
			throw new ua.nure.vydrya.SummaryTask4.exception.JNotifyException(
					"Error JNotify addWatch", e);
		}
		LOG.debug("contextDestroyed dine");
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.debug("contextDestroyed  start");
		try {
			JNotify.removeWatch(watchId);
		} catch (JNotifyException e) {
			LOG.warn("Error JNotify removeWatch", e);
		}
		LOG.debug("contextDestroyed done");
	}
}
