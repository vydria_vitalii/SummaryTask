/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.converter.impl;

import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderBean;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;

/**
 * @author vydrya_vitaliy.
 *
 */
public class OpenOrderParamBeanToOpenOrderBeanConverter
		implements Converter<OpenOrderParamBean, OpenOrderBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.web.converter.Converter#convert(java.io.
	 * Serializable)
	 */
	@Override
	public OpenOrderBean convert(OpenOrderParamBean t) {
		OpenOrderBean bean = new OpenOrderBean();
		bean.setIdBook(Long.valueOf(t.getIdBook()));
		bean.setIdUser(Long.valueOf(t.getIdUser()));
		return bean;
	}

}
