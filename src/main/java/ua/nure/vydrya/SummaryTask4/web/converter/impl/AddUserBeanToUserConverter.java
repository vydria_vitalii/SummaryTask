/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.converter.impl;

import ua.nure.vydrya.SummaryTask4.repository.entity.Role;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.web.bean.AddUserBean;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;

/**
 * @author vydrya_vitaliy.
 *
 */
public class AddUserBeanToUserConverter implements Converter<AddUserBean, User> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public User convert(AddUserBean t) {
		User user = new User();
		user.setEmail(t.getEmail());
		user.setPassword(t.getPassword());
		user.setName(t.getName());
		user.setSurname(t.getSurname());
		user.setPatronomic(t.getPatronomic());
		user.setSurname(t.getSurname());
		user.setPassportID(t.getPassportID());
		user.setAdress(t.getAdress());
		user.setRole(Role.valueOf(t.getRole().toUpperCase()));
		return user;
	}
}
