package ua.nure.vydrya.SummaryTask4.web.servlet.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.AdminService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblockBean;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblocParamBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.servlet.user.RegistrationsUserServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BlockUnblockUserServlet extends DependencyInjectionServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(RegistrationsUserServlet.class);
	private static final long serialVersionUID = -1998503095303303802L;

	@Inject("BlockUnblocParamExtractor")
	private WebExtractor<BlockUnblocParamBean> extractor;

	@Inject("BlockUnblockUserValidator")
	private Validator<ErrorHolder, BlockUnblocParamBean> validator;

	@Inject("AdminService")
	private AdminService adminService;

	@Inject("BlockUnblocParamBeanToBlockUnblocBeanConverter")
	private Converter<BlockUnblocParamBean, BlockUnblockBean> converter;

	@Inject("ListBlockUsers")
	private List<Long> listBlockUsers;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");
				BlockUnblocParamBean param = extractor.extract(req);
				ErrorHolder error = validator.validation(param);
				LOG.info("error: " + error);
				if (error.isEmpty()) {
					BlockUnblockBean bean = converter.convert(param);
					LOG.info("BlockUnblocBean: " + bean);
					adminService.blockUnblockUser(bean);
					listBlockUsers.add(bean.getId());
					sendRedirect(Pages.LIST_USERS, resp);
				}
				LOG.debug("execute done");
			}
		};
		executeOperation(operation, req, resp);
		LOG.debug("doGet done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(req, resp);
		LOG.debug("doPost done");
	}

}
