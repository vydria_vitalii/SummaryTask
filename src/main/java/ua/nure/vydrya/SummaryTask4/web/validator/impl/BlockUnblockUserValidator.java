/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblocParamBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BlockUnblockUserValidator extends Validator<ErrorHolder, BlockUnblocParamBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public ErrorHolder validation(BlockUnblocParamBean e) {
		ErrorHolder error = new ErrorHolder();
		if (!isValidByPattern(e.getId(), RegexConstants.NUMBER_PATTERN)
				|| isValidByPattern(RegexConstants.BOOLEAN, e.getDeleted())) {
			error.add(ErrorConstants.ERROR_USER_DELETED, ErrorConstants.ERROR_USER_DELETED_KEY);
		}
		return error;
	}

}
