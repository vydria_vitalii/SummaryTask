package ua.nure.vydrya.SummaryTask4.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.repository.entity.Role;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.InitParamConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.util.url.UrlUtils;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AccessFilter extends AbstractFilter {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(AccessFilter.class);

	/** Access map containing allowed command for each user role. */
	private final Map<Role, List<String>> accessMap = new HashMap<>();

	/** List of common command. */
	private List<String> commons;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		LOG.debug("init start");
		commons = getCommandByRole(InitParamConstants.COMMONS, fConfig);
		LOG.info("commons: " + commons);

		for (Role item : Role.values()) {
			accessMap.put(item, getCommandByRole(item.value(), fConfig));
		}
		LOG.info("accessMap: " + accessMap);
		LOG.debug("init done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		LOG.debug("destroy start");
		LOG.debug("destroy done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.filter.AbstractFilter#doFilter(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("AccessFilter doFilter starts");
		if (accessAllowed(req)) {
			chain.doFilter(req, resp);
		} else {
			resp.sendError(ErrorConstants.ERROR_404);
		}
		LOG.debug("AccessFilter doFilter finished");
	}

	private List<String> getCommandByRole(final String role, final FilterConfig fConfig) {
		LOG.debug("getCommandRole starts");
		List<String> list = asList(fConfig.getInitParameter(role));
		LOG.debug("getCommandRole finished");
		return list;
	}

	/**
	 * Checks if access is allowed.
	 * 
	 * @param req
	 *            - {@link HttpServletRequest}.
	 * @return the status result access.
	 */
	private boolean accessAllowed(final HttpServletRequest req) {
		LOG.debug("AccessFilter accessAllowed starts");
		String nameCommand = UrlUtils.convertUrlToCommand(req.getServletPath());
		LOG.trace("name command: " + nameCommand);
		User user = (User) req.getSession().getAttribute(WebConstants.USER);
		boolean flagAccess = true;
		if (Objects.isNull(user) || Objects.isNull(user.getRole())) {
			if (commons.contains(nameCommand) || searchCommandByAllRole(nameCommand)) {
				flagAccess = false;
			}
		} else {
			if (!commons.contains(nameCommand)) {
				flagAccess = accessMap.get(user.getRole()).contains(nameCommand);
				if (!flagAccess && !searchCommandByAllRole(nameCommand)) {
					flagAccess = true;
				}
			}
		}
		LOG.trace("flag access: " + flagAccess);
		LOG.debug("AccessFilter accessAllowed finished");
		return flagAccess;
	}

	private boolean searchCommandByAllRole(final String nameCommand) {
		boolean flag = false;
		for (Map.Entry<Role, List<String>> entry : accessMap.entrySet()) {
			if (entry.getValue().contains(nameCommand)) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	/**
	 * Extracts parameter values from string.
	 *
	 * @param str
	 *            parameter values string.
	 * @return list of parameter values.
	 */
	private List<String> asList(final String str) {
		List<String> list = new ArrayList<>();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens()) {
			list.add(st.nextToken());
		}
		return list;
	}
}
