package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ExemplarBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class EditBooksExtractor implements WebExtractor<ExemplarBean> {
	private WebExtractor<Books> extractor;

	public EditBooksExtractor(WebExtractor<Books> addBooksExtractor) {
		this.extractor = addBooksExtractor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public ExemplarBean extract(HttpServletRequest req) {
		Books books = extractor.extract(req);
		ExemplarBean exemplar = new ExemplarBean();
		exemplar.setBooks(books);
		exemplar.setIdExemplar(Long.valueOf(req.getParameter(WebConstants.ID)));
		return exemplar;
	}
}
