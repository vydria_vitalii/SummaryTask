package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ActivationOrderParamBeanExtractor implements WebExtractor<ActivationOrderParamBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public ActivationOrderParamBean extract(HttpServletRequest req) {
		ActivationOrderParamBean bean = new ActivationOrderParamBean();
		bean.setOrderId(req.getParameter(WebConstants.ID));
		bean.setDateOut(req.getParameter(WebConstants.DATE_OUT));
		return bean;
	}
}
