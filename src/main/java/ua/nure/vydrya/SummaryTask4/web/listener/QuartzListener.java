package ua.nure.vydrya.SummaryTask4.web.listener;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.google.common.collect.Sets;

import ua.nure.vydrya.SummaryTask4.quartz.job.fine.FineJob;
import ua.nure.vydrya.SummaryTask4.service.OrderService;
import ua.nure.vydrya.SummaryTask4.util.constants.DependencyInjectionConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.InitParamConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.JobConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;

/**
 * Application Lifecycle Listener implementation class QuartzListener
 *
 */
public class QuartzListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(QuartzListener.class);
	private Scheduler scheduler = null;

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOG.debug("contextInitialized start");
		ServletContext context = sce.getServletContext();
		OrderService orderService = (OrderService) context
				.getAttribute(DependencyInjectionConstants.ORDER_SERVICE);
		final int repeatCount = Integer
				.parseInt(context.getInitParameter(InitParamConstants.REPEAT_COUNT));
		final long timeWait = Long.valueOf(context.getInitParameter(InitParamConstants.TIME_WAIT));
		final String cronSchedule = context.getInitParameter(InitParamConstants.CRON_SCHEDULE);

		try {
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put(JobConstants.ORDER_SERVICE, orderService);
			jobDataMap.put(JobConstants.REPEAT_COUNT, repeatCount);
			jobDataMap.put(JobConstants.TIME_WAIT, timeWait);

			// Setup the Job class and the Job group
			JobDetail job = newJob(FineJob.class)
					.withIdentity(JobConstants.NAME_JOB, JobConstants.GROUP)
					.usingJobData(jobDataMap).build();

			Map<JobDetail, Set<? extends Trigger>> triggersAndJobs = new HashMap<>();

			Trigger triggerNow = newTrigger()
					.withIdentity(JobConstants.NAME_TRIGGER + "0", JobConstants.GROUP).startNow()
					.build();
			Trigger trigger = newTrigger()
					.withIdentity(JobConstants.NAME_TRIGGER + "1", JobConstants.GROUP)
					.withSchedule(CronScheduleBuilder.cronSchedule(cronSchedule)).build();

			triggersAndJobs.put(job, Sets.newHashSet(new Trigger[] { triggerNow, trigger }));

			// Setup the Job and Trigger with Scheduler & schedule jobs
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJobs(triggersAndJobs, true);
		} catch (SchedulerException e) {
			LOG.warn("Scheduler problem", e);
			context.getRequestDispatcher(Pages.ERROR_404);
		}
		LOG.debug("contextInitialized done");
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.debug("contextDestroyed start");
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			LOG.warn("Error Destroyed job", e);
		}
		LOG.debug("contextDestroyed done");
	}
}