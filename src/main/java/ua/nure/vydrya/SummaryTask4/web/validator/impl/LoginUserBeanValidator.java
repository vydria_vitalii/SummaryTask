/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import org.apache.commons.lang3.StringUtils;

import ua.nure.vydrya.SummaryTask4.api.captcha.Captcha;
import ua.nure.vydrya.SummaryTask4.exception.CaptchaException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.bean.UserLoginBean;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 *
 */
public class LoginUserBeanValidator extends Validator<ErrorHolder, UserLoginBean> {
	private Captcha captcha;

	/**
	 * @param captcha
	 */
	public LoginUserBeanValidator(Captcha captcha) {
		this.captcha = captcha;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public ErrorHolder validation(UserLoginBean e) {
		ErrorHolder error = new ErrorHolder();
		boolean verifyCaptcha;
		try {
			verifyCaptcha = captcha.verify(e.getCaptca());
		} catch (CaptchaException e1) {
			throw new ResourceException("Error Captcha", e1);
		}
		if (!verifyCaptcha) {
			error.add(ErrorConstants.ERROR_CAPCHA, ErrorConstants.ERROR_CAPCHA_KEY);
		}

		if (!isValidByPattern(e.getLogin(), RegexConstants.EMAIL_PATTERN)) {
			error.add(ErrorConstants.ERROR_EMAIL, ErrorConstants.ERROR_EMAIL_KEY);
		}

		if (StringUtils.isEmpty(e.getPassword())) {
			error.add(ErrorConstants.ERROR_PASSWORD, ErrorConstants.ERROR_PASSWORD_KEY);
		}
		return error;
	}
}
