/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ActivationOrderBean implements Serializable {
	private static final long serialVersionUID = -3046914514898536338L;
	private Long idOrder;
	private Timestamp dateIn;
	private Date dateOut;

	/**
	 * Default constructor.
	 */
	public ActivationOrderBean() {
	}

	/**
	 * @return the idOrder
	 */
	public Long getIdOrder() {
		return idOrder;
	}

	/**
	 * @param idOrder
	 *            the idOrder to set
	 */
	public void setIdOrder(Long idOrder) {
		this.idOrder = idOrder;
	}

	/**
	 * @return the dateIn
	 */
	public Timestamp getDateIn() {
		return dateIn;
	}

	/**
	 * @param dateIn
	 *            the dateIn to set
	 */
	public void setDateIn(Timestamp dateIn) {
		this.dateIn = dateIn;
	}

	/**
	 * @return the dateOut
	 */
	public Date getDateOut() {
		return dateOut;
	}

	/**
	 * @param dateOut
	 *            the dateOut to set
	 */
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateIn == null) ? 0 : dateIn.hashCode());
		result = prime * result + ((dateOut == null) ? 0 : dateOut.hashCode());
		result = prime * result + ((idOrder == null) ? 0 : idOrder.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ActivationOrderBean)) {
			return false;
		}
		ActivationOrderBean other = (ActivationOrderBean) obj;
		if (dateIn == null) {
			if (other.dateIn != null) {
				return false;
			}
		} else if (!dateIn.equals(other.dateIn)) {
			return false;
		}
		if (dateOut == null) {
			if (other.dateOut != null) {
				return false;
			}
		} else if (!dateOut.equals(other.dateOut)) {
			return false;
		}
		if (idOrder == null) {
			if (other.idOrder != null) {
				return false;
			}
		} else if (!idOrder.equals(other.idOrder)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActivationOrderBean [");
		if (idOrder != null) {
			builder.append("idOrder=");
			builder.append(idOrder);
			builder.append(", ");
		}
		if (dateIn != null) {
			builder.append("dateIn=");
			builder.append(dateIn);
			builder.append(", ");
		}
		if (dateOut != null) {
			builder.append("dateOut=");
			builder.append(dateOut);
			builder.append(", ");
		}
		builder.append("]");
		return builder.toString();
	}
}
