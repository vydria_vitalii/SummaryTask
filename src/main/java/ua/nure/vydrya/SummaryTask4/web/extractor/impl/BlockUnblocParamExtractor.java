package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblocParamBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BlockUnblocParamExtractor implements WebExtractor<BlockUnblocParamBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public BlockUnblocParamBean extract(HttpServletRequest req) {
		BlockUnblocParamBean bean = new BlockUnblocParamBean();
		bean.setId(req.getParameter(WebConstants.ID));
		bean.setDeleted(req.getParameter(WebConstants.DELETED));
		return bean;
	}
}
