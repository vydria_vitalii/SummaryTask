/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;

import ua.nure.vydrya.SummaryTask4.repository.entity.User;

/**
 * @author vydrya_vitaliy.
 *
 */
public class TestBean implements Serializable {
	private static final long serialVersionUID = -5911507746806477644L;
	private User user;
	private int count;

	/**
	 * 
	 */

	public TestBean() {
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TestBean)) {
			return false;
		}
		TestBean other = (TestBean) obj;
		if (count != other.count) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TestBean [");
		if (user != null) {
			builder.append("user=");
			builder.append(user);
			builder.append(", ");
		}
		builder.append("count=");
		builder.append(count);
		builder.append("]");
		return builder.toString();
	}

}
