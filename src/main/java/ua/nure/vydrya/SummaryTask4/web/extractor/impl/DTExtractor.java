/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.util.constants.AjaxConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTExtractor implements WebExtractor<DTBean> {
	public static final String ASC = "asc";
	public static final String DESC = "desc";
	private int min;
	private int max;

	public DTExtractor(int min, int max) {
		this.min = min;
		this.max = max;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public DTBean extract(HttpServletRequest req) {
		DTBean bean = new DTBean();

		HttpSession session = req.getSession();
		if (Objects.nonNull(session) && Objects.nonNull(session.getAttribute(WebConstants.USER))) {
			bean.setUser((User) session.getAttribute(WebConstants.USER));
		}

		String start = req.getParameter(AjaxConstants.DISPLAY_START);
		int startTmp = 0;
		if (Objects.nonNull(start)) {
			startTmp = Integer.parseInt(start);
			if (startTmp < 0) {
				startTmp = 0;
			}
		}
		bean.setDisplayStart(startTmp);

		String dispLen = req.getParameter(AjaxConstants.DISPLAY_LENGTH);
		int amount = min;
		if (Objects.nonNull(dispLen)) {
			amount = Integer.parseInt(dispLen);
			if (amount < min || amount > max) {
				amount = min;
			}
		}
		bean.setDisplayLength(amount);

		String echo = req.getParameter(AjaxConstants.ECHO);
		int echoTmp = 0;
		if (Objects.nonNull(echo)) {
			echoTmp = Integer.parseInt(echo);
		}
		bean.setEcho(echoTmp);

		String columns = req.getParameter(AjaxConstants.COLUMNS);
		if (Objects.nonNull(columns)) {
			bean.setColumns(Integer.valueOf(columns));
		} else {
			bean.setColumns(0);
		}

		bean.setGlobalSearch(req.getParameter(AjaxConstants.GLOBAL_SEARCH));

		String sortCols = req.getParameter(AjaxConstants.SORTING_COLS);
		int cols = 1;
		if (Objects.nonNull(sortCols)) {
			cols = Integer.parseInt(sortCols);
		}
		bean.setSortingCols(cols);

		for (int i = 0; i < bean.getColumns(); i++) {
			bean.getSearchs().add(req.getParameter(AjaxConstants.SEARCH_COL + String.valueOf(i)));
		}

		StringBuilder str = new StringBuilder();
		for (int i = 0; i < bean.getSortingCols(); i++) {
			str.append(req.getParameter(AjaxConstants.SORD_DIR + String.valueOf(i)));
			if (Objects.equals(str.toString(), ASC)) {
				bean.getDirs().add(ASC.toUpperCase());
			} else {
				bean.getDirs().add(DESC.toUpperCase());
			}
			str.setLength(0);

			str.append(req.getParameter(AjaxConstants.SORTING_COL + String.valueOf(i)));

			if (!StringUtils.isEmpty(str.toString().replace("null", ""))) {
				bean.getCols().add(Integer.parseInt(str.toString()));
			} else {
				bean.getCols().add(0);
			}
			str.setLength(0);
		}
		return bean;
	}

}
