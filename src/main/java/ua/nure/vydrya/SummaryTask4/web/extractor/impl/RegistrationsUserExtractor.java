package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.repository.entity.Role;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.AddUserBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class RegistrationsUserExtractor implements WebExtractor<AddUserBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public AddUserBean extract(HttpServletRequest req) {
		AddUserBean bean = new AddUserBean();
		bean.setEmail(req.getParameter(WebConstants.EMAIL));
		bean.setPassword(req.getParameter(WebConstants.PASSWORD_1));
		bean.setPasswordConf(req.getParameter(WebConstants.PASSWORD_2));
		bean.setName(req.getParameter(WebConstants.NAME));
		bean.setSurname(req.getParameter(WebConstants.SURNAME));
		bean.setPatronomic(req.getParameter(WebConstants.PATRONOMIC));
		bean.setPassportID(req.getParameter(WebConstants.PASSPOTR_ID).replaceAll("\\s", " ").trim()
				.toUpperCase());
		bean.setAdress(req.getParameter(WebConstants.ADRESS));
		bean.setRole(Role.READER.value());
		bean.setCaptcha(req.getParameter(WebConstants.PARAM_RECAPTCHA));
		return bean;
	}
}
