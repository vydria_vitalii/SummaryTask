/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import ua.nure.vydrya.SummaryTask4.generator.password.PasswordGenerator;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.AddUserBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AddEmployeeExtractor extends RegistrationsUserExtractor {
	private PasswordGenerator generatro;

	public AddEmployeeExtractor(PasswordGenerator generator) {
		this.generatro = generator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.impl.RegistrationsUserExtractor
	 * #extract(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public AddUserBean extract(HttpServletRequest req) {
		AddUserBean bean = super.extract(req);
		bean.setRole(req.getParameter(WebConstants.ROLE));
		final String password = generatro.generatePassword();
		bean.setPassword(password);
		bean.setPasswordConf(password);
		return bean;
	}
}
