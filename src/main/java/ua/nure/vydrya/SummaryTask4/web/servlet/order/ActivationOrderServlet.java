package ua.nure.vydrya.SummaryTask4.web.servlet.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.OrderService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * Servlet implementation class ActivationOrderServlet
 */
public class ActivationOrderServlet extends DependencyInjectionServlet {
	private static final long serialVersionUID = -2582986737988072203L;
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(ActivationOrderServlet.class);

	@Inject("ActivationOrderParamBeanExtractor")
	private WebExtractor<ActivationOrderParamBean> extractor;

	@Inject("ActivationOrderParamBeanValidator")
	private Validator<ErrorHolder, ActivationOrderParamBean> validator;

	@Inject("ActivationOrderParamBeanToActivationOrderBeanConverter")
	private Converter<ActivationOrderParamBean, ActivationOrderBean> converter;

	@Inject("OrderService")
	private OrderService orderService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet satrt");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute satrt");
				ActivationOrderParamBean bean = extractor.extract(req);
				ErrorHolder error = validator.validation(bean);
				if (error.isEmpty()) {
					ActivationOrderBean activationOrder = converter.convert(bean);
					LOG.info(
							"activationOrder res:" + orderService.activationOrder(activationOrder));
					LOG.debug("execute done");
					sendRedirect(Pages.LIST_ORDERS_ACTIVATION, resp);
					return;
				}
				setError(req, error);
				sendRedirect(Pages.ACTIVATION_ORDER + "?id=" + bean.getOrderId(), resp);
				LOG.debug("execute done");
			}
		};
		LOG.debug("doGet done");
		executeOperation(operation, req, resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}
}
