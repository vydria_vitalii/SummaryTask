package ua.nure.vydrya.SummaryTask4.web.servlet.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.OrderService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;

/**
 * Servlet implementation class DoneOrderServlet
 */
public class DoneOrderServlet extends DependencyInjectionServlet {
	private static final Logger LOG = Logger.getLogger(DoneOrderServlet.class);
	private static final long serialVersionUID = -6958118278085790208L;

	@Inject("OrderService")
	private OrderService orderService;
	@Inject("DoneOrderExtractor")
	private WebExtractor<Long> extractor;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {
			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");
				Long idOrder = extractor.extract(req);
				LOG.info("resul done order: " + orderService.doneOrder(idOrder));
				sendRedirect(Pages.LIST_ALL_ORDERS, resp);
				LOG.debug("execute done");
			}
		};
		LOG.debug("doGet done");
		executeOperation(operation, req, resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(request, response);
		LOG.debug("doPost done");
	}
}
