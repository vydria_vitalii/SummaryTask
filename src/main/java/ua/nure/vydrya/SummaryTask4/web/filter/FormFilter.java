package ua.nure.vydrya.SummaryTask4.web.filter;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;

/**
 * Servlet Filter implementation class FormFilter
 */
public class FormFilter extends AbstractFilter {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(FormFilter.class);

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		LOG.debug("init start");
		LOG.debug("init done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.filter.AbstractFilter#doFilter(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("doFilter start");
		HttpSession session = req.getSession();
		if (Objects.nonNull(session)) {
			ErrorHolder error = (ErrorHolder) session.getAttribute(ErrorConstants.ERROR);
			if (Objects.nonNull(error)) {
				req.setAttribute(ErrorConstants.ERROR, error.getErrors());
				session.removeAttribute(ErrorConstants.ERROR);
			}
			Object formBean = session.getAttribute(WebConstants.FORM_BEAN);
			if (Objects.nonNull(formBean)) {
				req.setAttribute(WebConstants.FORM_BEAN, formBean);
				session.removeAttribute(WebConstants.FORM_BEAN);
			}
		}
		chain.doFilter(req, resp);
		LOG.debug("doFilter done");
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		LOG.debug("destroy start");
		LOG.debug("FormFilter done");
	}

}
