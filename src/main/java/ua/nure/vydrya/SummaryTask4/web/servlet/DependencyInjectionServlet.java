package ua.nure.vydrya.SummaryTask4.web.servlet;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.DependencyInjection;
import ua.nure.vydrya.SummaryTask4.exception.DependencyInjectionException;
import ua.nure.vydrya.SummaryTask4.util.constants.DependencyInjectionConstants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class DependencyInjectionServlet extends AbstractServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(DependencyInjectionServlet.class);
	private static final long serialVersionUID = -7074927227415722611L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		LOG.debug("init start");
		DependencyInjection di = (DependencyInjection) getServletContext()
				.getAttribute(DependencyInjectionConstants.DEPENDENCY_INJECTION);
		try {
			di.inject(this.getClass(), DependencyInjectionServlet.class, this);
		} catch (DependencyInjectionException e) {
			LOG.error("Error inject", e);
			throw new ServletException("Error inject", e);
		}
		LOG.debug("init done");
	}
}
