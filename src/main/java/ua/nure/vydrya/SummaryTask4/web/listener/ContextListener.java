package ua.nure.vydrya.SummaryTask4.web.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.books.BooksAPI;
import ua.nure.vydrya.SummaryTask4.api.books.google.GoogleBooksAPI;
import ua.nure.vydrya.SummaryTask4.api.captcha.Captcha;
import ua.nure.vydrya.SummaryTask4.api.captcha.CaptchaFactory;
import ua.nure.vydrya.SummaryTask4.api.captcha.CreatorCaptcha;
import ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory;
import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor;
import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc.DTBooksExtractor;
import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc.DTExemplarExtractor;
import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc.DTOrderExtractor;
import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc.DTUserExtractor;
import ua.nure.vydrya.SummaryTask4.api.mail.CreatorMailSetting;
import ua.nure.vydrya.SummaryTask4.api.mail.MailFactory;
import ua.nure.vydrya.SummaryTask4.api.mail.MailSettings;
import ua.nure.vydrya.SummaryTask4.api.mail.Sender;
import ua.nure.vydrya.SummaryTask4.di.DependencyInjection;
import ua.nure.vydrya.SummaryTask4.di.context.AppContext;
import ua.nure.vydrya.SummaryTask4.di.context.servlet.AppServletContext;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.fine.FinePolicy;
import ua.nure.vydrya.SummaryTask4.fine.file.json.FinePolicyImpl;
import ua.nure.vydrya.SummaryTask4.generator.password.PasswordGenerator;
import ua.nure.vydrya.SummaryTask4.generator.password.impl.PasswordGeneratorImpl;
import ua.nure.vydrya.SummaryTask4.hash.Hash;
import ua.nure.vydrya.SummaryTask4.hash.sha256.Sha256;
import ua.nure.vydrya.SummaryTask4.locale.LocaleFactory;
import ua.nure.vydrya.SummaryTask4.locale.LocaleStorage;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.repository.transaction.jdbc.JdbcTransactionManager;
import ua.nure.vydrya.SummaryTask4.service.AdminService;
import ua.nure.vydrya.SummaryTask4.service.DTService;
import ua.nure.vydrya.SummaryTask4.service.LocaleService;
import ua.nure.vydrya.SummaryTask4.service.impl.AdminServiceImpl;
import ua.nure.vydrya.SummaryTask4.service.impl.BooksServiceImpl;
import ua.nure.vydrya.SummaryTask4.service.impl.DTServiceImpl;
import ua.nure.vydrya.SummaryTask4.service.impl.LocaleServiceImpl;
import ua.nure.vydrya.SummaryTask4.service.impl.OrderServiceImpl;
import ua.nure.vydrya.SummaryTask4.service.impl.UserServiceImpl;
import ua.nure.vydrya.SummaryTask4.util.constants.DependencyInjectionConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.FileConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.InitParamConstants;
import ua.nure.vydrya.SummaryTask4.web.converter.impl.ActivationOrderParamBeanToActivationOrderBeanConverter;
import ua.nure.vydrya.SummaryTask4.web.converter.impl.AddUserBeanToUserConverter;
import ua.nure.vydrya.SummaryTask4.web.converter.impl.BlockUnblocParamBeanToBlockUnblocBeanConverter;
import ua.nure.vydrya.SummaryTask4.web.converter.impl.OpenOrderParamBeanToOpenOrderBeanConverter;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.ActivationOrderParamBeanExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.AddBooksExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.AddEmployeeExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.BlockUnblocParamExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.CancelOrderExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.DoneOrderExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.EditBooksExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.OpenOrderParamExtaractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.ParamIdExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.RegistrationsUserExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.RemoveBooksExtractor;
import ua.nure.vydrya.SummaryTask4.web.extractor.impl.UserLoginBeanExtractor;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.ActivationOrderParamBeanValidator;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.AddBooksValidator;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.AddUserBeanValidator;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.BlockUnblockUserValidator;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.LoginUserBeanValidator;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.OpenOrderParamBeanValidator;
import ua.nure.vydrya.SummaryTask4.web.validator.impl.ParamIdValidator;

/**
 * Application Lifecycle Listener implementation class ContextListener.
 * 
 * @version 1.0.
 */
public class ContextListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(ContextListener.class);

	private DependencyInjection di;
	private LocaleStorage localeStorage;
	private LocaleService localeService;

	private Hash hash;
	private DaoFactory daoFactory;
	private TransactionManager transaction;
	private Captcha captcha;
	private MailSettings settings;
	private PasswordGenerator generator;
	private BooksAPI booksAPI;
	private WebExtractor<Books> addBooksExtractor;
	private FinePolicy finePolicy;

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOG.debug("start contextInitialized");
		ServletContext context = sce.getServletContext();
		initListBlockUsers(context);

		// // -------------------
		initDependencyInjection(context);

		// // ------------------
		initLocaleStorage(context);

		// // ------------------------
		initLocaleService(context);

		initCaptcha(context);

		initHash(context);

		// ------------------------
		initLoginUserBeanValidator(context);
		// ----------------------
		initUserLoginBeanExtractor(context);

		initDB(context);

		initMailSettings(context);

		initUserService(context);

		initRegistrationsUserExtractor(context);

		initAddUserValidator(context);

		initAddUserBeanToUserConverter(context);

		initDTService(context);

		initDTExtractor(context);

		intiAdminService(context);

		initPasswordGenerator(context);

		initAddEmployeeExtratctor(context);

		initBooksAPI(context);

		initAddBooksExtractor(context);

		initBooksService(context);

		initAddBooksValidator(context);

		initEditBooksExtractor(context);

		initBlockUnblocParamBean(context);

		intiBlockUnblockUserValidator(context);

		initBlockUnblocParamBeanToBlockUnblocBeanConverter(context);

		initParamIdExtractor(context);

		initParamIdValidator(context);

		initFinePolicy(context);

		initOrderService(context);

		intiOpenOrderParamBeanExtractor(context);

		initOpenOrderParamBeanValidator(context);

		intiOpenOrderParamBeanToOpenOrderBeanConverter(context);

		initCancelOrderExtractor(context);

		initActivationOrderParamBeanToActivationOrderBeanConverter(context);

		initActivationOrderParamBeanValidator(context);

		initActivationOrderParamBeanExtractor(context);

		initDoneOrderExtractor(context);

		initRemoveBooksExtractor(context);

		LOG.debug("done contextInitialized");
	}

	/**
	 * @param context
	 */
	private void initListBlockUsers(ServletContext context) {
		LOG.debug("initListBlockUsers start");
		context.setAttribute(DependencyInjectionConstants.LIST_BLOCK_USERS, new ArrayList<Long>());
		LOG.debug("initListBlockUsers done");
	}

	/**
	 * @param context
	 */
	private void initFinePolicy(ServletContext context) {
		LOG.debug("initFinePolicy start");
		final String path = context.getInitParameter(InitParamConstants.PATH_FINE_FILE);
		final String schema = context.getRealPath(FileConstants.FILE_SCHEMA);
		finePolicy = new FinePolicyImpl(path, schema);
		context.setAttribute(DependencyInjectionConstants.FINE_POLICY, finePolicy);
		LOG.debug("initFinePolicy done");
	}

	/**
	 * @param context
	 */
	private void initRemoveBooksExtractor(ServletContext context) {
		LOG.debug("initRemoveBooksExtractor start");
		context.setAttribute(DependencyInjectionConstants.REMOVE_BOOKS_EXTRACTOR,
				new RemoveBooksExtractor());
		LOG.debug("initRemoveBooksExtractor done");
	}

	/**
	 * @param context
	 */
	private void initDoneOrderExtractor(ServletContext context) {
		LOG.debug("initDoneOrderExtractor start");
		context.setAttribute(DependencyInjectionConstants.DONE_ORDER_EXTARCTOR,
				new DoneOrderExtractor());
		LOG.debug("initDoneOrderExtractor done");
	}

	/**
	 * @param context
	 */
	private void initActivationOrderParamBeanExtractor(ServletContext context) {
		LOG.debug("initActivationOrderParamBeanExtractor start");
		context.setAttribute(DependencyInjectionConstants.ACTIVATION_ORDER_PARAM_BEAN_EXTRACTOR,
				new ActivationOrderParamBeanExtractor());
		LOG.debug("initActivationOrderParamBeanExtractor done");
	}

	/**
	 * @param context
	 */
	private void initActivationOrderParamBeanValidator(ServletContext context) {
		LOG.debug("initActivationOrderParamBeanValidator start");
		context.setAttribute(DependencyInjectionConstants.ACTIVATION_ORDER_PARAM_BEAN_VALIDATOR,
				new ActivationOrderParamBeanValidator());
		LOG.debug("initActivationOrderParamBeanValidator done");
	}

	/**
	 * @param context
	 */
	private void initActivationOrderParamBeanToActivationOrderBeanConverter(
			ServletContext context) {
		LOG.debug("initActivationOrderParamBeanToActivationOrderBean start");
		context.setAttribute(
				DependencyInjectionConstants.ACTIVATION_ORDER_PARAM_BEAN_TO_ACTIVATION_ORDER_BEAN_CONVERTER,
				new ActivationOrderParamBeanToActivationOrderBeanConverter());
		LOG.debug("initActivationOrderParamBeanToActivationOrderBean done");
	}

	/**
	 * @param context
	 */
	private void initCancelOrderExtractor(ServletContext context) {
		LOG.debug("initCancelOrderExtractor start");
		context.setAttribute(DependencyInjectionConstants.CANCEL_ORDER_EXTRACTOR,
				new CancelOrderExtractor());
		LOG.debug("initCancelOrderExtractor done");
	}

	/**
	 * @param context
	 */
	private void intiOpenOrderParamBeanToOpenOrderBeanConverter(ServletContext context) {
		LOG.debug("intiOpenOrderParamBeanToOpenOrderBeanConverter start");
		context.setAttribute(
				DependencyInjectionConstants.OPEN_ORDER_PARAM_BEAN_TO_OPEN_ORDER_BEAN_CONVERTOR,
				new OpenOrderParamBeanToOpenOrderBeanConverter());
		LOG.debug("intiOpenOrderParamBeanToOpenOrderBeanConverter done");
	}

	/**
	 * @param context
	 */
	private void initOpenOrderParamBeanValidator(ServletContext context) {
		LOG.debug("initOpenOrderParamBeanValidator start");
		context.setAttribute(DependencyInjectionConstants.OPEN_ORDER_PARAM_BEAN_VALIDATOR,
				new OpenOrderParamBeanValidator());
		LOG.debug("initOpenOrderParamBeanValidator done");
	}

	/**
	 * @param context
	 */
	private void intiOpenOrderParamBeanExtractor(ServletContext context) {
		LOG.debug("intiOpenOrderParamBeanExtractor start");
		context.setAttribute(DependencyInjectionConstants.OPEN_ORDER_PARAM_BEAN_EXTARCTOR,
				new OpenOrderParamExtaractor());
		LOG.debug("intiOpenOrderParamBeanExtractor done");
	}

	/**
	 * @param context
	 */
	private void initOrderService(ServletContext context) {
		LOG.debug("initOrderService start");
		context.setAttribute(DependencyInjectionConstants.ORDER_SERVICE,
				new OrderServiceImpl(transaction, daoFactory, finePolicy));
		LOG.debug("initOrderService done");
	}

	/**
	 * @param context
	 */
	private void initParamIdValidator(ServletContext context) {
		LOG.debug("initParamIdValidator start");
		context.setAttribute(DependencyInjectionConstants.PARAM_ID_VALIDATOR,
				new ParamIdValidator());
		LOG.debug("initParamIdValidator done");
	}

	/**
	 * @param context
	 */
	private void initParamIdExtractor(ServletContext context) {
		LOG.debug("initParamIdExtractor start");
		context.setAttribute(DependencyInjectionConstants.PARAM_ID_EXTRACTOR,
				new ParamIdExtractor());
		LOG.debug("initParamIdExtractor done");
	}

	/**
	 * @param context
	 */
	private void initBlockUnblocParamBeanToBlockUnblocBeanConverter(ServletContext context) {
		LOG.debug("initBlockUnblocParamBeanToBlockUnblocBeanConverter start");
		context.setAttribute(
				DependencyInjectionConstants.BLOCK_UNBLOCK_PARAM_BEAN_TOBLOCK_UNBLOCK_BEAN_CONVERTER,
				new BlockUnblocParamBeanToBlockUnblocBeanConverter());
		LOG.debug("initBlockUnblocParamBeanToBlockUnblocBeanConverter done");
	}

	/**
	 * @param context
	 */
	private void intiBlockUnblockUserValidator(ServletContext context) {
		LOG.debug("intiBlockUnblockUserValidator start");
		context.setAttribute(DependencyInjectionConstants.BLOCK_UNBLOCK_USER_VALIDATOR,
				new BlockUnblockUserValidator());
		LOG.debug("intiBlockUnblockUserValidator done");
	}

	/**
	 * @param context
	 */
	private void initBlockUnblocParamBean(ServletContext context) {
		LOG.debug("initBlockUnblocParamBean start");
		context.setAttribute(DependencyInjectionConstants.BLOCK_UNBLOCK_PARAM_EXTRATOR,
				new BlockUnblocParamExtractor());
		LOG.debug("initBlockUnblocParamBean done");
	}

	/**
	 * @param context
	 */
	private void initEditBooksExtractor(ServletContext context) {
		LOG.debug("initEditBooksExtractor start");
		context.setAttribute(DependencyInjectionConstants.EDIT_BOOKS_EXTRACTOR,
				new EditBooksExtractor(addBooksExtractor));

		LOG.debug("initEditBooksExtractor done");
	}

	/**
	 * @param context
	 */
	private void initAddBooksValidator(ServletContext context) {
		LOG.debug("initAddBooksValidator start");
		context.setAttribute(DependencyInjectionConstants.ADD_BOOKS_VALIDATOR,
				new AddBooksValidator());
		LOG.debug("initAddBooksValidator done");
	}

	/**
	 * @param context
	 */
	private void initBooksService(ServletContext context) {
		LOG.debug("initBooksService start");
		context.setAttribute(DependencyInjectionConstants.BOOKS_SERVICE,
				new BooksServiceImpl(transaction, daoFactory));
		LOG.debug("initBooksService done");
	}

	/**
	 * @param context
	 */
	private void initAddBooksExtractor(ServletContext context) {
		LOG.debug("initBooksExtractor start");
		addBooksExtractor = new AddBooksExtractor(booksAPI);
		context.setAttribute(DependencyInjectionConstants.ADD_BOOKS_EXTRACTOR, addBooksExtractor);
		LOG.debug("initBooksExtractor done");
	}

	/**
	 * @param con
	 */
	private void initBooksAPI(ServletContext context) {
		LOG.debug("initBooksAPI start");
		booksAPI = new GoogleBooksAPI(
				context.getInitParameter(InitParamConstants.GOOGLE_BOOKS_API_KEY));
		LOG.debug("initBooksAPI done");
	}

	/**
	 * @param context
	 */
	private void initPasswordGenerator(ServletContext context) {
		LOG.debug("initPasswordGenerator start");
		int len = Integer.parseInt(context.getInitParameter(InitParamConstants.LENGTH_PASSWORD));
		generator = new PasswordGeneratorImpl(len);
		LOG.debug("initPasswordGenerator done");
	}

	/**
	 * @param context
	 */
	private void initAddEmployeeExtratctor(ServletContext context) {
		LOG.debug("initAddEmployeeExtratctor  start");
		context.setAttribute(DependencyInjectionConstants.ADD_EMPLOYEE_EXTRACTOR,
				new AddEmployeeExtractor(generator));
		LOG.debug("initAddEmployeeExtratctor done");
	}

	/**
	 * @param context
	 */
	private void intiAdminService(ServletContext context) {
		LOG.debug("intiAdminService start");
		AdminService service = new AdminServiceImpl(hash, settings, daoFactory, transaction);
		context.setAttribute(DependencyInjectionConstants.ADMIN_SERVICE, service);
		LOG.debug("intiAdminService done");
	}

	/**
	 * @param context
	 */
	private void initDTExtractor(ServletContext context) {
		LOG.debug("initDTExtractor start");
		int min = Integer.parseInt(context.getInitParameter(InitParamConstants.MIN_DT));
		int max = Integer.parseInt(context.getInitParameter(InitParamConstants.MAX_DT));
		context.setAttribute(DependencyInjectionConstants.DT_EXTRACTOR,
				new ua.nure.vydrya.SummaryTask4.web.extractor.impl.DTExtractor(min, max));
		LOG.debug("initDTExtractor done");
	}

	/**
	 * @param context
	 */
	private void initDTService(ServletContext context) {
		LOG.debug("initDTService");
		String dt = context.getInitParameter(InitParamConstants.DT);
		AbstractDTFactory<?> factory = AbstractDTFactory.getInstance(dt);

		DTExtractor<User> userExtractor = new DTUserExtractor();
		DTExtractor<Books> booksExtractor = new DTBooksExtractor();
		DTExtractor<Exemplar> exemplarExtarctro = new DTExemplarExtractor(daoFactory,
				booksExtractor);
		DTExtractor<Order> orderExtractor = new DTOrderExtractor(daoFactory, userExtractor,
				exemplarExtarctro);

		DTService service = new DTServiceImpl(transaction, daoFactory, factory, userExtractor,
				booksExtractor, exemplarExtarctro, orderExtractor);

		context.setAttribute(DependencyInjectionConstants.DT_SERVICE, service);
		LOG.debug("initDTService");
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.debug("strat contextDestroyed");
		LOG.debug("done contextDestroyed");
	}

	private void initDependencyInjection(final ServletContext context) {
		LOG.debug("initDependencyInjection start");
		AppContext app = new AppServletContext(context);
		di = new DependencyInjection(app);
		context.setAttribute(DependencyInjectionConstants.DEPENDENCY_INJECTION, di);
		LOG.debug("initDependencyInjection done");
	}

	private void initLocaleStorage(final ServletContext context) {
		LOG.debug("initLocaleStorage start");
		String whichFactoryLocale = context.getInitParameter(InitParamConstants.LOCALE_STORAGE);
		localeStorage = LocaleFactory.getLocaleStorage(whichFactoryLocale);
		context.setAttribute(DependencyInjectionConstants.LOCALE_STORAGE, localeStorage);
		LOG.debug("initLocaleStorage done");
	}

	private void initLocaleService(final ServletContext context) {
		LOG.debug("initLocaleService start");
		Set<Locale> locales = new HashSet<>();
		String[] availableLocales = context.getInitParameter(InitParamConstants.AVAILABLE_LOCALES)
				.split(" ");
		for (String locale : availableLocales) {
			locales.add(new Locale(locale));
		}
		checkLocale(locales);
		final int indexDefLocale = Integer
				.parseInt(context.getInitParameter(InitParamConstants.DEFAULT_LOCALE));
		String defaulLocale = availableLocales[indexDefLocale];
		String value = context.getInitParameter(InitParamConstants.INTERVAL_INVALID_LOCALE_STORAGE);

		final Integer maxAge = Integer.valueOf(value);
		this.localeService = new LocaleServiceImpl(new Locale(defaulLocale), locales, maxAge);
		context.setAttribute(DependencyInjectionConstants.LOCALE_SERVICE, this.localeService);
		LOG.debug("initLocaleService done");
	}

	private void initLoginUserBeanValidator(final ServletContext context) {
		LOG.debug("initLoginUserBeanValidator start");
		context.setAttribute(DependencyInjectionConstants.LOGIN_USER_BEAN_VALIDATOR,
				new LoginUserBeanValidator(captcha));
		LOG.debug("initLoginUserBeanValidator done");
	}

	private void initUserLoginBeanExtractor(final ServletContext context) {
		LOG.debug("initUserLoginBeanExtractor start");
		context.setAttribute(DependencyInjectionConstants.USER_LOGIN_BEAN_EXTRACTOR,
				new UserLoginBeanExtractor());
		LOG.debug("initUserLoginBeanExtractor done");
	}

	private void initHash(final ServletContext context) {
		LOG.debug("initHash start");
		hash = new Sha256();
		LOG.debug("initHash done");
	}

	private void initDB(final ServletContext context) {
		LOG.debug("initDB start");
		String db = context.getInitParameter(InitParamConstants.DB);
		daoFactory = DaoFactory.getDaoFactory(db);

		String dataSourceName = context.getInitParameter(InitParamConstants.DATA_SOURCE_NAME);
		Context initContext;
		DataSource dataSource;
		try {
			initContext = new InitialContext();
			dataSource = (DataSource) initContext.lookup(dataSourceName);
		} catch (NamingException e) {
			LOG.error("No  init Context", e);
			throw new ResourceException("No  init Context", e);

		}
		transaction = new JdbcTransactionManager(dataSource);
		LOG.debug("initDB done");
	}

	private void initCaptcha(final ServletContext context) {
		LOG.debug("initCaptcha start");
		CreatorCaptcha creatorRacaptcha = CaptchaFactory
				.getInstance(context.getInitParameter(InitParamConstants.CAPTCHA));
		this.captcha = creatorRacaptcha
				.factoryMethod(context.getInitParameter(InitParamConstants.SECRET_KEY));
		context.setAttribute(DependencyInjectionConstants.CAPTCHA, captcha);
		LOG.debug("initCaptcha done");
	}

	private void initUserService(final ServletContext context) {
		LOG.debug("initUserService start");
		context.setAttribute(DependencyInjectionConstants.USER_SERVICE,
				new UserServiceImpl(transaction, daoFactory, hash, this.settings));
		LOG.debug("initUserService done");
	}

	private void initRegistrationsUserExtractor(final ServletContext context) {
		LOG.debug("initRegistrationsUserExtractor start");
		context.setAttribute(DependencyInjectionConstants.REGISTARATIONS_USER_EXTRACTOR,
				new RegistrationsUserExtractor());
		LOG.debug("initRegistrationsUserExtractor done");
	}

	private void initAddUserValidator(final ServletContext context) {
		LOG.debug("initRegistrationsValidator start");
		context.setAttribute(DependencyInjectionConstants.ADD_USER_VALIDATOR,
				new AddUserBeanValidator(captcha));
		LOG.debug("RegistrationsValidator done");
	}

	private void initAddUserBeanToUserConverter(final ServletContext context) {
		LOG.debug("RegistartionsBeanToUserConverter start");
		context.setAttribute(DependencyInjectionConstants.ADD_USER_BEAN_TO_USER_CONVERTER,
				new AddUserBeanToUserConverter());
		LOG.debug("RegistartionsBeanToU serConverter done");
	}

	/**
	 * @param context
	 */
	private void initMailSettings(ServletContext context) {
		LOG.debug("initMailSettings star");
		String mail = context.getInitParameter(InitParamConstants.MAIL);

		Sender sender = new Sender();
		sender.setLogin(context.getInitParameter(InitParamConstants.LOGIN));
		sender.setPassword(context.getInitParameter(InitParamConstants.PASSWORD));

		CreatorMailSetting creatorMailSetting = MailFactory.getInstance(mail);
		settings = creatorMailSetting.factoryMethod(sender);
		LOG.debug("initMailSettings done");
	}

	/**
	 * Check Locale.
	 * 
	 * @param locales
	 *            {@link Collection} {@link Locale}.
	 */
	private void checkLocale(Collection<Locale> locales) {
		LOG.debug("checkLocale strat");
		for (Locale locale : locales) {
			ResourceBundle.getBundle(InitParamConstants.RESOURCE_BUNDLE + "_" + locale);
		}
		LOG.debug("checkLocale done");
	}
}
