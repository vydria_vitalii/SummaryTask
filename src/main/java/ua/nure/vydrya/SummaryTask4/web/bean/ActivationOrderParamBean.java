package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;

/**
 * @author vydrya_vitaliy.
 *
 */
public class ActivationOrderParamBean implements Serializable {
	private static final long serialVersionUID = 2809440718236760502L;
	private String orderId;
	private String dateOut;

	public ActivationOrderParamBean() {
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId
	 *            the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the dateOut
	 */
	public String getDateOut() {
		return dateOut;
	}

	/**
	 * @param dateOut
	 *            the dateOut to set
	 */
	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOut == null) ? 0 : dateOut.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ActivationOrderParamBean)) {
			return false;
		}
		ActivationOrderParamBean other = (ActivationOrderParamBean) obj;
		if (dateOut == null) {
			if (other.dateOut != null) {
				return false;
			}
		} else if (!dateOut.equals(other.dateOut)) {
			return false;
		}
		if (orderId == null) {
			if (other.orderId != null) {
				return false;
			}
		} else if (!orderId.equals(other.orderId)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActivationOrderParamBean [");
		if (orderId != null) {
			builder.append("orderId=");
			builder.append(orderId);
			builder.append(", ");
		}
		if (dateOut != null) {
			builder.append("dateOut=");
			builder.append(dateOut);
			builder.append(", ");
		}
		builder.append("]");
		return builder.toString();
	}
}
