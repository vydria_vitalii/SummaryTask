package ua.nure.vydrya.SummaryTask4.web.servlet.exemplar;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.DTService;
import ua.nure.vydrya.SummaryTask4.util.constants.AjaxConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AjaxListExemplarsServlet extends DependencyInjectionServlet {
	private static final long serialVersionUID = -8226126669437713395L;
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(AjaxListExemplarsServlet.class);

	@Inject("DTService")
	private DTService service;

	@Inject("DTExtractor")
	private WebExtractor<DTBean> extractor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute satrt");
				DTBean table = extractor.extract(req);
				String res = service.getDataTableResponseExemplars(table);
				LOG.info("result: " + res);
				resp.setContentType(AjaxConstants.CONTENT_TYPE);
				resp.setCharacterEncoding(AjaxConstants.CHARACTER_ENCODING);
				resp.getWriter().print(res);
				LOG.debug("execute done");
			}
		};
		LOG.debug("doGet done");
		executeOperation(operation, req, resp);
	}
}
