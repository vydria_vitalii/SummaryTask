package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.api.client.repackaged.com.google.common.base.Objects;

import ua.nure.vydrya.SummaryTask4.api.captcha.Captcha;
import ua.nure.vydrya.SummaryTask4.exception.CaptchaException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Role;
import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.AddUserBean;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AddUserBeanValidator extends Validator<ErrorHolder, AddUserBean> {
	private static final int PASS_LEN = 20;
	private Captcha captcha;

	/**
	 * @param captcha
	 */
	public AddUserBeanValidator(Captcha captcha) {
		this.captcha = captcha;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public ErrorHolder validation(AddUserBean e) {
		ErrorHolder error = new ErrorHolder();
		boolean valid;
		try {
			valid = captcha.verify(e.getCaptcha());
		} catch (CaptchaException e1) {
			throw new ResourceException("Error captcha", e1);
		}
		if (!valid) {
			error.add(ErrorConstants.ERROR_CAPCHA, ErrorConstants.ERROR_CAPCHA_KEY);
		}

		if (!isValidByPattern(e.getEmail(), RegexConstants.EMAIL_PATTERN)) {
			error.add(ErrorConstants.ERROR_EMAIL, ErrorConstants.ERROR_EMAIL_KEY);
		}

		boolean flagError = false;

		if (!Objects.equal(e.getPassword(), e.getPasswordConf())
				|| e.getPassword().length() > PASS_LEN || StringUtils.isEmpty(e.getPassword())) {
			flagError = true;
		}

		if (StringUtils.isEmpty(e.getAdress())) {
			flagError = true;
		}

		if (StringUtils.isEmpty(e.getName()) || StringUtils.isEmpty(e.getSurname())
				|| StringUtils.isEmpty(e.getPatronomic())) {
			flagError = true;
		}

		if (!isValidByPattern(e.getPassportID(), RegexConstants.PASSPORT_PATTERN)) {
			error.add(ErrorConstants.ERROR_PASSPORT_INCORRECT,
					ErrorConstants.ERROR_PASSPORT_INCORRECT_KEY);
		}

		if (!EnumUtils.isValidEnum(Role.class, e.getRole().toUpperCase())) {
			flagError = true;
		}

		if (flagError) {
			error.add(ErrorConstants.ERROR_REG_FORM, ErrorConstants.ERROR_REG_FORM_KEY);
		}

		return error;
	}

}
