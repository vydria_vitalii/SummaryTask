package ua.nure.vydrya.SummaryTask4.web.servlet.order;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.service.OrderService;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.servlet.DependencyInjectionServlet;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class CancelOrderServlet extends DependencyInjectionServlet {
	private static final Logger LOG = Logger.getLogger(CancelOrderServlet.class);
	private static final long serialVersionUID = -5842678104676454814L;

	@Inject("OrderService")
	private OrderService orderService;

	@Inject("CancelOrderExtractor")
	private WebExtractor<Long> extractor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doGet start");
		ServletOperation operation = new ServletOperation() {

			@Override
			public void execute() throws Exception {
				LOG.debug("execute start");
				Long id = extractor.extract(req);
				LOG.info("CancelOrder: " + orderService.cancellOrder(id));
				LOG.debug("execute done");
				sendRedirect(Pages.LIST_ORDERS, resp);
			}
		};
		LOG.debug("doGet done");
		executeOperation(operation, req, resp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOG.debug("doPost start");
		doGet(req, resp);
		LOG.debug("doPost done");
	}

}
