package ua.nure.vydrya.SummaryTask4.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class IdValidationFilter extends DependencyInjectionFilter {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(IdValidationFilter.class);

	@Inject("ParamIdExtractor")
	private WebExtractor<String> extractor;

	@Inject("ParamIdValidator")
	private Validator<Boolean, String> validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.filter.AbstractFilter#doFilter(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("doFilter satrt");
		String id = extractor.extract(req);
		if (validator.validation(id)) {
			LOG.trace("id valid");
			req.setAttribute(WebConstants.ID, Long.valueOf(id));
			chain.doFilter(req, resp);
		} else {
			LOG.trace("id unvalid");
			resp.sendRedirect(Pages.INDEX);
		}
		LOG.debug("doFilter done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		LOG.debug("destroy start");
		LOG.debug("destroy done");
	}
}
