/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import java.util.Objects;

import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 *
 */
public class AddBooksValidator extends Validator<ErrorHolder, Books> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public ErrorHolder validation(Books e) {
		ErrorHolder error = new ErrorHolder();
		if (Objects.isNull(e)) {
			error.add(ErrorConstants.ERROR_BOOKS, ErrorConstants.ERROR_ADD_BOOKS_KEY);
		}
		return error;
	}
}
