package ua.nure.vydrya.SummaryTask4.web.bean;

import java.io.Serializable;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class OpenOrderParamBean implements Serializable {
	private static final long serialVersionUID = 1755022575295688892L;
	private String idUser;
	private String idBook;

	/**
	 * Default constructor.
	 */
	public OpenOrderParamBean() {
	}

	/**
	 * @return the idUser
	 */
	public String getIdUser() {
		return idUser;
	}

	/**
	 * @param idUser
	 *            the idUser to set
	 */
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	/**
	 * @return the idBook
	 */
	public String getIdBook() {
		return idBook;
	}

	/**
	 * @param idBook
	 *            the idBook to set
	 */
	public void setIdBook(String idBook) {
		this.idBook = idBook;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idBook == null) ? 0 : idBook.hashCode());
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OpenOrderParamBean)) {
			return false;
		}
		OpenOrderParamBean other = (OpenOrderParamBean) obj;
		if (idBook == null) {
			if (other.idBook != null) {
				return false;
			}
		} else if (!idBook.equals(other.idBook)) {
			return false;
		}
		if (idUser == null) {
			if (other.idUser != null) {
				return false;
			}
		} else if (!idUser.equals(other.idUser)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OpenOrderParamBean [");
		if (idUser != null) {
			builder.append("idUser=");
			builder.append(idUser);
			builder.append(", ");
		}
		if (idBook != null) {
			builder.append("idBook=");
			builder.append(idBook);
		}
		builder.append("]");
		return builder.toString();
	}
}
