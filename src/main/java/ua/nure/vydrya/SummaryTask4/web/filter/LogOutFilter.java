package ua.nure.vydrya.SummaryTask4.web.filter;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.util.constants.Pages;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;

/**
 * Servlet Filter implementation class LogOutFilter
 */
public class LogOutFilter extends DependencyInjectionFilter {
	private static final Logger LOG = Logger.getLogger(LogOutFilter.class);

	@Inject("ListBlockUsers")
	private List<Long> listBlockUsers;
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.filter.AbstractFilter#doFilter(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("doFilter start");
		HttpSession session = req.getSession();
		boolean logOutFlag = false;
		int i = 0;
		if (Objects.nonNull(session)) {
			User user = (User) session.getAttribute(WebConstants.USER);
			if (Objects.nonNull(user)) {
				for (Long item : listBlockUsers) {
					if (Objects.equals(item, user.getId())) {
						logOutFlag = true;
						break;
					}
					i++;
				}
			}
		}
		if (logOutFlag) {
			listBlockUsers.remove(i);
			resp.sendRedirect(Pages.LOG_OUT);
		} else {
			chain.doFilter(req, resp);
		}
		LOG.debug("doFilter done");
	}

	@Override
	public void destroy() {
		LOG.debug("destroy start");
		LOG.debug("destroy done");
	}
}
