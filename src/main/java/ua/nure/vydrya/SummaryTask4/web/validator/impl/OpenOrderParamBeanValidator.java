/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.validator.impl;

import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.RegexConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderParamBean;
import ua.nure.vydrya.SummaryTask4.web.validator.Validator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class OpenOrderParamBeanValidator extends Validator<ErrorHolder, OpenOrderParamBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.validator.Validator#validation(java.lang.
	 * Object)
	 */
	@Override
	public ErrorHolder validation(OpenOrderParamBean e) {
		ErrorHolder errorHolder = new ErrorHolder();

		if (!isValidByPattern(e.getIdUser(), RegexConstants.NUMBER_PATTERN)) {
			errorHolder.add(ErrorConstants.ERROR_OPEN_ORDER_USER_ID,
					ErrorConstants.ERROR_OPEN_ORDER_USER_ID_KEY);
		}

		if (!isValidByPattern(e.getIdBook(), RegexConstants.NUMBER_PATTERN)) {
			errorHolder.add(ErrorConstants.ERROR_OPEN_ORDER_BOOK_ID,
					ErrorConstants.ERROR_OPEN_ORDER_BOOK_ID);
		}

		return errorHolder;
	}

}
