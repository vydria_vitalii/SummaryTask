package ua.nure.vydrya.SummaryTask4.web.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.util.constants.ErrorConstants;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.ErrorHolder;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class AbstractServlet extends HttpServlet {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(AbstractServlet.class);
	private static final long serialVersionUID = -1128762647502487761L;

	protected void setError(HttpServletRequest req, ErrorHolder error, Object bean) {
		LOG.debug("setError start");
		HttpSession session = req.getSession(true);
		setError(req, error);
		session.setAttribute(WebConstants.FORM_BEAN, bean);
		LOG.debug("setError done");
	}

	protected void setError(HttpServletRequest req, ErrorHolder error) {
		LOG.debug("setError start");
		HttpSession session = req.getSession(true);
		LOG.info("error: " + error);
		session.setAttribute(ErrorConstants.ERROR, error);
		LOG.debug("setError done");
	}

	protected void sendRedirect(String path, HttpServletResponse resp) throws IOException {
		resp.sendRedirect(path);
	}

	protected void forvard(String path, HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher(path);
		dispatcher.forward(req, resp);
	}

	protected void executeOperation(ServletOperation operation, HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		LOG.debug("executeOperation start");
		try {
			operation.execute();
		} catch (Exception e) {
			LOG.warn("Error executeOperation", e);
			resp.sendError(ErrorConstants.ERROR_500);
		}
		LOG.debug("executeOperation done");
	}

	protected interface ServletOperation {
		void execute() throws Exception;
	}
}
