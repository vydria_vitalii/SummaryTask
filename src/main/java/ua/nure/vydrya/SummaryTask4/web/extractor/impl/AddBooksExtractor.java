/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.web.extractor.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import ua.nure.vydrya.SummaryTask4.api.books.BooksAPI;
import ua.nure.vydrya.SummaryTask4.exception.BooksAPIException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.util.constants.WebConstants;
import ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AddBooksExtractor implements WebExtractor<Books> {
	private BooksAPI booksAPI;

	/**
	*
	*/
	public AddBooksExtractor(BooksAPI booksAPI) {
		this.booksAPI = booksAPI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.extractor.WebExtractor#extract(javax.
	 * servlet.http.HttpServletRequest)
	 */
	@Override
	public Books extract(HttpServletRequest req) {
		Books books = null;
		try {
			String id = req.getParameter(WebConstants.BOOK_ID);
			if (!StringUtils.isEmpty(id)) {
				books = booksAPI.getBook(id);
			}
		} catch (BooksAPIException e) {
			throw new ResourceException("Error BooksAPI", e);
		}
		return books;
	}
}
