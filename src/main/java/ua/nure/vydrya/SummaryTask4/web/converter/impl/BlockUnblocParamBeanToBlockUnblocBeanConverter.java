package ua.nure.vydrya.SummaryTask4.web.converter.impl;

import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblockBean;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblocParamBean;
import ua.nure.vydrya.SummaryTask4.web.converter.Converter;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BlockUnblocParamBeanToBlockUnblocBeanConverter
		implements Converter<BlockUnblocParamBean, BlockUnblockBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.web.converter.Converter#convert(java.io.
	 * Serializable)
	 */
	@Override
	public BlockUnblockBean convert(BlockUnblocParamBean t) {
		BlockUnblockBean bean = new BlockUnblockBean();
		bean.setId(Long.parseLong(t.getId()));
		bean.setDeleted(Boolean.parseBoolean(t.getDeleted()));
		return bean;
	}
}
