/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.dtable.jdbc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import ua.nure.vydrya.SummaryTask4.api.dtable.CreatorDTSpecification;
import ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification;
import ua.nure.vydrya.SummaryTask4.util.db.SqlFunction;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

/**
 * @author vydrya_vitaliy.
 *
 */
public abstract class AbstractJdbcDTSpecification
		implements DTSpecification<String>, CreatorDTSpecification<String> {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(JdbcDTSpecificationUsers.class);
	private ThreadLocal<String> tLocaleGlobaleSearchQuery = new ThreadLocal<>();
	private ThreadLocal<List<String>> tLocaleNameColumn = new ThreadLocal<>();
	private ThreadLocal<DTBean> tLocaleTable = new ThreadLocal<>();
	private ThreadLocal<String> tLocaleIndividualSearch = new ThreadLocal<>();

	protected void setGlobaleSearchQuery(String query) {
		tLocaleGlobaleSearchQuery.set(query);
	}

	protected String getGlobaleSearchQuery() {
		return tLocaleGlobaleSearchQuery.get();
	}

	protected void setNameColumn(List<String> list) {
		tLocaleNameColumn.set(list);
	}

	protected List<String> getNameColumn() {
		return tLocaleNameColumn.get();
	}

	protected void setTable(DTBean table) {
		tLocaleTable.set(table);
	}

	protected DTBean getTable() {
		return tLocaleTable.get();
	}

	protected void setIndividualSearch(String individualSearch) {
		tLocaleIndividualSearch.set(individualSearch);
	}

	protected String getIndividualSearch() {
		return tLocaleIndividualSearch.get();
	}

	protected void initIndividualSearch(DTBean table, String[] nameColumn, int[] indexColumn) {
		LOG.debug("initIndividualSearch satrt");
		LOG.debug("table search: " + table.getSearchs());
		StringBuilder str = new StringBuilder();
		StringBuilder tmp = new StringBuilder();
		int j = 0;
		for (int i = 0; i < indexColumn.length; i++) {
			tmp.append(table.getSearchs().get(i));
			LOG.info("tmp: " + tmp);
			if (!Strings.isNullOrEmpty(tmp.toString())) {
				if (j++ != 0) {
					str.append(" AND ");
				}
				str.append(nameColumn[indexColumn[i]] + " "
						+ SqlFunction.like("'%" + tmp.toString() + "%'"));
			}
			tmp.setLength(0);
		}
		setIndividualSearch(str.toString());
		LOG.debug("initIndividualSearch done");
	}

	protected void initGlobaleSearchQuery(DTBean table, String[] nameColumn) {
		LOG.debug("initGlobaleSearchQuery start");
		StringBuilder globalSearch = new StringBuilder();
		if (!StringUtils.isEmpty(table.getGlobalSearch())) {
			int i = 0;
			for (String var : nameColumn) {
				globalSearch.append(var + " " + SqlFunction.like("?"));
				if (nameColumn.length - 1 != i++) {
					globalSearch.append(" OR ");
				}
			}
		}
		setGlobaleSearchQuery(globalSearch.toString());
		LOG.debug("initGlobaleSearchQuery done");
	}

	protected List<String> getNameColumnSort(List<Integer> numberColumn, String[] nameColumn) {
		LOG.debug("getNameColumnSort start");
		ArrayList<String> list = new ArrayList<>();
		for (Integer number : numberColumn) {
			list.add(nameColumn[number]);
		}
		LOG.debug("getNameColumnSort done");
		return list;
	}
}
