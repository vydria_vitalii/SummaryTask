package ua.nure.vydrya.SummaryTask4.api.dtable;

import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface CreatorDTSpecification<E> {
	DTSpecification<? extends E> setDBbean(DTBean bean);
}
