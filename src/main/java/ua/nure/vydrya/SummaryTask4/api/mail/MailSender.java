package ua.nure.vydrya.SummaryTask4.api.mail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class MailSender {
	private static final Logger LOG = Logger.getLogger(MailSender.class);
	private Session session;
	private String emailRecipient;
	private String mailSubject;
	private String mailContent;
	private Thread thread;

	/**
	 * @param session
	 *            - the {@link #session}.
	 * @param emailRecipient
	 * @param mailSubject
	 * @param mailContent
	 */
	public MailSender(Session session, String emailRecipient, String mailSubject,
			String mailContent) {
		LOG.debug("MailSender constructor strat");
		this.session = session;
		this.emailRecipient = emailRecipient;
		this.mailSubject = mailSubject;
		this.mailContent = mailContent;

		thread = new Thread(new RunMailSender());
		thread.setDaemon(true);
		LOG.debug("MailSender constructor done");
	}

	/**
	 * Execute start thread.
	 */
	public void execute() {
		thread.start();
	}

	/**
	 * Implementation run send mail. <br>
	 * RunMailSender implements {@link Runnable}.
	 * 
	 * @author vydrya_vitaliy.
	 * @version 1.0.
	 */
	private class RunMailSender implements Runnable {
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			try {
				Message msg = new MimeMessage(session);
				// кому
				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(emailRecipient));
				// тема сообщения
				msg.setSubject(mailSubject);
				// текст
				msg.setText(mailContent);
				// отправляем сообщение
				Transport.send(msg);
			} catch (MessagingException e) {
				LOG.error("Error run mail", e);
				throw new RuntimeException("Error ran mail", e);
			}
		}
	}
}
