/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.dtable.extratcor;

import javax.json.JsonArray;

import ua.nure.vydrya.SummaryTask4.exception.DTException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface DTExtractor<E extends Entity> {
	JsonArray extract(E e) throws DTException;
}
