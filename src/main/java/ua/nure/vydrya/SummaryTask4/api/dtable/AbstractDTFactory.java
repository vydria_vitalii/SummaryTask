package ua.nure.vydrya.SummaryTask4.api.dtable;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.dtable.jdbc.JdbcDTFactory;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class AbstractDTFactory<E> {
	private static final Logger LOG = Logger.getLogger(AbstractDTFactory.class);
	public static final String DT_JDBC = "DT_JBDC";

	public abstract CreatorDTSpecification<? extends E> getSpecificationUsers();

	public abstract CreatorDTSpecification<? extends E> getSpecificationExemplars();

	public abstract CreatorDTSpecification<? extends E> getSpecificationBooks();

	public abstract CreatorDTSpecification<? extends E> getSpecificationOrdersByUser();

	public abstract CreatorDTSpecification<? extends E> getSpecificationOrdersActivation();

	public abstract CreatorDTSpecification<? extends E> getSpecificationAllOrders();

	public static AbstractDTFactory<?> getInstance(final String whichFactory) {
		LOG.debug("start getInstance");
		LOG.info("whichFactory: " + whichFactory);
		if (Objects.equals(DT_JDBC, whichFactory)) {
			return new JdbcDTFactory();
		} else {
			LOG.warn("implementation is not found - number: " + whichFactory);
			throw new IllegalArgumentException("implementation is not found");
		}
	}
}
