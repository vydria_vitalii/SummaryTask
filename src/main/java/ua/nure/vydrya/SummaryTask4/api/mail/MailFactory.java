package ua.nure.vydrya.SummaryTask4.api.mail;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.captcha.CaptchaFactory;
import ua.nure.vydrya.SummaryTask4.api.mail.gmail.GmailSettings;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class MailFactory {
	private static final Logger LOG = Logger.getLogger(CaptchaFactory.class);
	public static final String GMAIL = "gmail";

	public static CreatorMailSetting getInstance(final String whichFactory) {
		LOG.debug("start getInstance");
		LOG.info("whichFactory: " + whichFactory);

		CreatorMailSetting creator;
		if (Objects.equals(whichFactory, GMAIL)) {
			creator = new CreatorMailSetting() {
				@Override
				public MailSettings factoryMethod(Sender sender) {
					return new GmailSettings(sender);
				}
			};
		} else {
			LOG.error("implementation is not found - " + whichFactory);
			throw new IllegalArgumentException("implementation is not found - " + whichFactory);
		}
		LOG.debug("whichFactory done");
		return creator;
	}
}
