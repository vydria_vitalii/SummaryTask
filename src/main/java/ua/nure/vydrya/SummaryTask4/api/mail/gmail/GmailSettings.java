package ua.nure.vydrya.SummaryTask4.api.mail.gmail;

import java.util.Objects;
import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import ua.nure.vydrya.SummaryTask4.api.mail.MailSettings;
import ua.nure.vydrya.SummaryTask4.api.mail.Sender;

/**
 * Implementations GmailSettings. <br>
 * GmailSettings implements {@link MailSettings}.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class GmailSettings implements MailSettings {
	private Session session;
	private Sender sender;

	public GmailSettings(final Sender sender) {
		this.sender = sender;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.mail.MailSettings#getSession(ua.nure.vydrya.
	 * SummaryTask4.mail.Sender)
	 */
	@Override
	public Session getSession() {
		if (Objects.isNull(session)) {
			session = Session.getDefaultInstance(getProperties(), new javax.mail.Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(sender.getLogin(), sender.getPassword());
				}
			});
		}
		return session;
	}

	/**
	 * Returns properties for mail connection.
	 *
	 * @return the {@link Properties}.
	 */
	protected Properties getProperties() {
		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");
		return properties;
	}
}
