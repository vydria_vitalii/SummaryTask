package ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor;
import ua.nure.vydrya.SummaryTask4.exception.DTException;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTUserExtractor implements DTExtractor<User> {

	// private DaoFactory factory;
	//
	// public DTUserExtractor(DaoFactory factory) {
	// this.factory = factory
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.dtable.extratcor.DTExtractor#extract(ua.nure.
	 * vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	public JsonArray extract(User user) throws DTException {
		JsonArrayBuilder ja = Json.createArrayBuilder();
		ja.add(user.getId());
		ja.add(user.getEmail());
		ja.add(user.getName());
		ja.add(user.getSurname());
		ja.add(user.getPatronomic());
		ja.add(user.getLibraryCard());
		ja.add(user.getPassportID());
		ja.add(user.getAdress());
		ja.add(user.getRole().value().toUpperCase());
		ja.add(user.isDeleted());
		return ja.build();
	}
}
