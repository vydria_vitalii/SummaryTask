/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.dtable;

import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface DTSpecification<E> {

	Specification<? extends E> getTotal();

	Specification<? extends E> getData();

	Specification<? extends E> getTotalAfterFilter();
}
