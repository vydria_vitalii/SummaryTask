package ua.nure.vydrya.SummaryTask4.api.mail;

import javax.mail.Session;

/**
 * Implementation MailSettings.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface MailSettings {
	/**
	 * Returns session of e-mail writer.
	 *
	 * @return the {@link Session}.
	 */
	Session getSession();
}
