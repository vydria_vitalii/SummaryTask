/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc;

import java.util.Objects;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import org.apache.commons.lang3.StringUtils;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor;
import ua.nure.vydrya.SummaryTask4.exception.DTException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTBooksExtractor implements DTExtractor<Books> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor#extract(ua.
	 * nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	public JsonArray extract(Books e) throws DTException {
		JsonArrayBuilder ja = Json.createArrayBuilder();
		ja.add(e.getId());
		ja.add(e.getTitle());
		ja.add(Strings.nullToEmpty(e.getSubtile()));
		ja.add(Strings.nullToEmpty(e.getDescription()));
		ja.add(Strings.nullToEmpty(e.getPublicsher()));
		ja.add(Strings.nullToEmpty(e.getAuthor()));
		ja.add(Strings.nullToEmpty(Objects.isNull(e.getPublishedDate()) ? StringUtils.EMPTY
				: e.getPublishedDate().toString()));
		ja.add(Strings.nullToEmpty(e.getImageLink()));
		ja.add(e.getLanguage());
		ja.add(Strings.nullToEmpty(Objects.toString(e.getPages())));
		ja.add(Strings.nullToEmpty(e.getIsbn10()));
		ja.add(Strings.nullToEmpty(e.getIsbn13()));
		ja.add(Strings.nullToEmpty(e.getOtherIdentifiers()));
		ja.add(e.isDeleted());
		return ja.build();
	}

}
