package ua.nure.vydrya.SummaryTask4.api.captcha;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.captcha.google.GoogleRecaptcha;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class CaptchaFactory {
	private static final Logger LOG = Logger.getLogger(CaptchaFactory.class);
	public static final String GOOGLE = "google";

	public static CreatorCaptcha getInstance(final String whichFactory) {
		LOG.debug("start getInstance");
		LOG.info("whichFactory: " + whichFactory);
		LOG.debug("whichFactory done");
		if (Objects.equals(whichFactory, GOOGLE)) {
			return new CreatorCaptcha() {
				@Override
				public Captcha factoryMethod(final String secretKey) {
					return new GoogleRecaptcha(secretKey);
				}
			};
		} else {
			LOG.error("implementation is not found - " + whichFactory);
			throw new IllegalArgumentException("implementation is not found - " + whichFactory);
		}
	}
}
