package ua.nure.vydrya.SummaryTask4.api.dtable.jdbc;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory;
import ua.nure.vydrya.SummaryTask4.api.dtable.CreatorDTSpecification;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class JdbcDTFactory extends AbstractDTFactory<String> {
	private static final Logger LOG = Logger.getLogger(JdbcDTFactory.class);

	private JdbcDTSpecificationUsers user;
	private JdbcDTSpecificationExemplars exemplar;
	private JdbcDTSpecificationBooks books;
	private JdbcDTSpecificationOrdersByUser ordersByUser;
	private JdbcDTSpecificationOrdersActivation ordersActivation;
	private JdbcDTSpecificationAllOrders allOrders;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.dtable.DTFactory#SpecificationUsers()
	 */
	@Override
	public CreatorDTSpecification<String> getSpecificationUsers() {
		LOG.debug("CreatorDTSpecification satrt");
		if (Objects.isNull(user)) {
			LOG.info("create JdbcDTSpecificationUsers");
			user = new JdbcDTSpecificationUsers();
		}
		LOG.debug("JdbcDTSpecificationUsers done");
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory#
	 * getSpecificationExemplars()
	 */
	@Override
	public CreatorDTSpecification<? extends String> getSpecificationExemplars() {
		LOG.debug("getSpecificationExemplars start");
		if (Objects.isNull(exemplar)) {
			LOG.info("create exemplar");
			exemplar = new JdbcDTSpecificationExemplars();
		}
		LOG.debug("getSpecificationExemplars done");
		return exemplar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory#
	 * getgetSpecificationBooks()
	 */
	@Override
	public CreatorDTSpecification<? extends String> getSpecificationBooks() {
		LOG.debug("getgetSpecificationBooks start");
		if (Objects.isNull(books)) {
			LOG.info("create books");
			books = new JdbcDTSpecificationBooks();
		}
		LOG.debug("getgetSpecificationBooks done");
		return books;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory#
	 * getSpecificationOrdersByUser()
	 */
	@Override
	public CreatorDTSpecification<? extends String> getSpecificationOrdersByUser() {
		LOG.debug("getSpecificationOrdersByUser start");
		if (Objects.isNull(ordersByUser)) {
			LOG.info("create ordersByUser");
			ordersByUser = new JdbcDTSpecificationOrdersByUser();
		}
		LOG.debug("getSpecificationOrdersByUser done");
		return ordersByUser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory#
	 * JdbcDTSpecificationOrdersActivation()
	 */
	@Override
	public CreatorDTSpecification<? extends String> getSpecificationOrdersActivation() {
		LOG.debug("JdbcDTSpecificationOrdersActivation satrt");
		if (Objects.isNull(ordersActivation)) {
			LOG.info("create ordersActivation");
			ordersActivation = new JdbcDTSpecificationOrdersActivation();
		}
		LOG.debug("JdbcDTSpecificationOrdersActivation done");
		return ordersActivation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory#
	 * getSpecificationAllOrders()
	 */
	@Override
	public CreatorDTSpecification<? extends String> getSpecificationAllOrders() {
		LOG.debug("getSpecificationAllOrders satrt");
		if (Objects.isNull(allOrders)) {
			LOG.info("create allOrders");
			allOrders = new JdbcDTSpecificationAllOrders();
		}
		LOG.debug("getSpecificationAllOrders done");
		return allOrders;
	}

}
