/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc;

import java.util.Objects;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import org.apache.commons.lang3.StringUtils;

import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor;
import ua.nure.vydrya.SummaryTask4.exception.DTException;
import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTOrderExtractor implements DTExtractor<Order> {
	private DTExtractor<User> userExtactor;
	private UserDao userDao;

	private DTExtractor<Exemplar> exemplarExtractor;
	private ExemplarDao exemplarDao;

	/**
	 * 
	 */
	public DTOrderExtractor(DaoFactory daoFactory, DTExtractor<User> userExtarctor,
			DTExtractor<Exemplar> exemplarExtractor) {
		this.exemplarExtractor = exemplarExtractor;
		this.userExtactor = userExtarctor;

		userDao = daoFactory.getUserDao();
		exemplarDao = daoFactory.getExemplarDao();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor#extract(ua.
	 * nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	public JsonArray extract(Order e) throws DTException {
		JsonArrayBuilder ja = Json.createArrayBuilder();
		try {
			ja.add(e.getId());
			ja.add(userExtactor.extract(userDao.findById(e.getUserId())));
			ja.add(exemplarExtractor.extract(exemplarDao.findById(e.getExemplarId())));
			ja.add(Objects.nonNull(e.getDateIn()) ? e.getDateIn().toString() : StringUtils.EMPTY);
			ja.add(Objects.nonNull(e.getDateOut()) ? e.getDateOut().toString() : StringUtils.EMPTY);
			ja.add(e.getFine() != 0 ? e.getFine().toString() : StringUtils.EMPTY);

			// final Long employeeIn = e.getEmployeeInId();
			// if (employeeIn != 0) {
			// ja.add(userExtactor.extract(userDao.findById(employeeIn)));
			// } else {
			// ja.add(StringUtils.EMPTY);
			// }
			//
			// final Long employeeOut = e.getEmployeeOutId();
			// if (employeeOut != 0) {
			// ja.add(userExtactor.extract(userDao.findById(employeeOut)));
			// } else {
			// ja.add(StringUtils.EMPTY);
			// }

			ja.add(e.getStatusOrder().value());
			ja.add(e.isDeleted());
		} catch (DaoException e1) {
			throw new DTException("Error DTOrderExtractor", e1);
		}
		return ja.build();
	}
}
