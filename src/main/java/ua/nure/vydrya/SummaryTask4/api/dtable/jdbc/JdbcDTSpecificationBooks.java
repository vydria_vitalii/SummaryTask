/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.dtable.jdbc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.query.builder.SelectBuilder;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusExemplar;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;
import ua.nure.vydrya.SummaryTask4.util.db.SqlFunction;
import ua.nure.vydrya.SummaryTask4.util.db.Tables;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

/**
 * @author vydrya_vitaliy.
 *
 */
public class JdbcDTSpecificationBooks extends AbstractJdbcDTSpecification {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(JdbcDTSpecificationBooks.class);
	private static final String[] GLOBAL_SEARCH = { Fields.TITLE, Fields.SUBTITLE,
			Fields.DESCRIPTION, Fields.PUBLICSHER, Fields.AUTHOR, Fields.PUBLISHED_DATE,
			Fields.PAGES, Fields.LANGUAGE, Fields.ISBN_10, Fields.ISBN_13,
			Fields.OTHER_IDENTIFIERS };

	private static final String[] BOOK = { StringUtils.EMPTY, Fields.TITLE, Fields.PUBLICSHER,
			Fields.AUTHOR, Fields.PUBLISHED_DATE };

	private static final int[] BOOKS_INDIVIDUAL_SEARCH_INDEX = { 1, 2, 3, 4 };

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification#getTotal()
	 */
	@Override
	public Specification<? extends String> getTotal() {
		LOG.debug("getTotal start");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.column(SqlFunction.count("*")).from(Tables.BOOKS)
						.join(Tables.EXEMPLAR + " ON " + Fields.ID_BOOKS + " = " + Fields.BOOK_ID)
						.where(Fields.STATUS_EXEMPLAR + " = '" + StatusExemplar.PRESENT.value()
								+ "'")
						.where(Fields.DELETED_EXEMPLAR + " = false").groupBy(Fields.ISBN_10)
						.groupBy(Fields.ISBN_13).groupBy(Fields.OTHER_IDENTIFIERS);

				return getCountQueryGroupBy(builder, Tables.BOOKS);
			}

			@Override
			public List<Object> getParameters() {
				return Collections.emptyList();
			}
		};
		LOG.debug("getTotal done");
		return sp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification#getData()
	 */
	@Override
	public Specification<String> getData() {
		LOG.debug("getData start");

		final String globeSearchQuery = getGlobaleSearchQuery();
		final DTBean table = getTable();
		final List<String> nameColumn = getNameColumn();
		final String individualSearch = getIndividualSearch();

		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.from(Tables.BOOKS)
						.join(Tables.EXEMPLAR + " ON " + Fields.ID_BOOKS + " = " + Fields.BOOK_ID)
						.where(Fields.STATUS_EXEMPLAR + " = '" + StatusExemplar.PRESENT.value()
								+ "'")
						.where(Fields.DELETED_EXEMPLAR + " = false");
				LOG.debug("builder :" + builder.toString());
				if (StringUtils.isNoneEmpty(globeSearchQuery)) {
					builder.where("(" + globeSearchQuery + ")");
				}
				if (StringUtils.isNoneEmpty(individualSearch)) {
					builder.where(individualSearch);
				}

				builder.groupBy(Fields.ISBN_10).groupBy(Fields.ISBN_13)
						.groupBy(Fields.OTHER_IDENTIFIERS);

				for (int i = 0; i < table.getSortingCols(); i++) {
					builder.orderBy(nameColumn.get(i) + " " + table.getDirs().get(i));
				}
				builder.limit(table.getDisplayStart(), table.getDisplayLength());

				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				List<Object> list;
				if (!Strings.isNullOrEmpty(globeSearchQuery)) {
					list = new ArrayList<>();
					for (int i = 0; i < GLOBAL_SEARCH.length; i++) {
						list.add("%" + table.getGlobalSearch() + "%");
					}
				} else {
					list = Collections.emptyList();
				}
				return list;
			}
		};
		LOG.debug("getData done");
		return sp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification#
	 * getTotalAfterFilter()
	 */
	@Override
	public Specification<String> getTotalAfterFilter() {
		LOG.debug("getTotalAfterFilter start");
		final String globeSearchQuery = getGlobaleSearchQuery();
		final String individyalSearch = getIndividualSearch();
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.column(SqlFunction.count("*")).from(Tables.BOOKS)
						.join(Tables.EXEMPLAR + " ON " + Fields.ID_BOOKS + " = " + Fields.BOOK_ID)
						.where(Fields.STATUS_EXEMPLAR + " = '" + StatusExemplar.PRESENT.value()
								+ "'")
						.where(Fields.DELETED_EXEMPLAR + " = false");
				if (StringUtils.isNoneEmpty(globeSearchQuery)) {
					builder.where("(" + globeSearchQuery + ")");
				}
				if (StringUtils.isNoneEmpty(individyalSearch)) {
					builder.where(individyalSearch);
				}
				builder.groupBy(Fields.ISBN_10).groupBy(Fields.ISBN_13)
						.groupBy(Fields.OTHER_IDENTIFIERS);

				return getCountQueryGroupBy(builder, Tables.BOOKS);
			}

			@Override
			public List<Object> getParameters() {
				return getData().getParameters();
			}
		};
		LOG.debug("getTotalAfterFilter done");
		return sp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.api.dtable.CreatorDTSpecification#setDBbean(
	 * ua.nure.vydrya.SummaryTask4.web.bean.DTBean)
	 */
	@Override
	public DTSpecification<String> setDBbean(DTBean table) {
		LOG.debug("setDBbean start");
		setNameColumn(getNameColumnSort(table.getCols(), BOOK));
		setTable(table);
		initGlobaleSearchQuery(table, GLOBAL_SEARCH);
		initIndividualSearch(table, BOOK, BOOKS_INDIVIDUAL_SEARCH_INDEX);
		LOG.debug("setDBbean done");
		return this;
	}

	private String getCountQueryGroupBy(final SelectBuilder builder, final String table) {
		final SelectBuilder countBuilder = new SelectBuilder();
		countBuilder.column(SqlFunction.count("*")).from("(" + builder.toString() + ")" + table);
		return countBuilder.toString();
	}

}
