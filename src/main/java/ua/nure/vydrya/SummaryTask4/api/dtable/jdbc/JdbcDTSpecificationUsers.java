package ua.nure.vydrya.SummaryTask4.api.dtable.jdbc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.query.builder.SelectBuilder;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;
import ua.nure.vydrya.SummaryTask4.util.db.SqlFunction;
import ua.nure.vydrya.SummaryTask4.util.db.Tables;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class JdbcDTSpecificationUsers extends AbstractJdbcDTSpecification {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(JdbcDTSpecificationUsers.class);
	private static final String[] USER = { Fields.EMAIL, Fields.NAME, Fields.SURNAME,
			Fields.PATRONOMIC, Fields.LIBRARY_CARD, Fields.PASSPORT_ID, Fields.ADRESS,
			Fields.ROLE };

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.dtable.DTSpecification#getTotal()
	 */
	@Override
	public Specification<String> getTotal() {
		LOG.debug("getTotal start");
		final DTBean table = getTable();
		Specification<String> total = new Specification<String>() {
			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.column(SqlFunction.count("*")).from(Tables.USER)
						.where(Fields.ID_USER + " != " + table.getUser().getId());
				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				return Collections.emptyList();
			}
		};
		LOG.debug("getTotal done");
		return total;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.dtable.DTSpecification#getData()
	 */
	@Override
	public Specification<String> getData() {
		LOG.debug("getData");
		final String globeSearchQuery = getGlobaleSearchQuery();
		final DTBean table = getTable();
		final List<String> nameColumn = getNameColumn();

		Specification<String> search = new Specification<String>() {
			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.from(Tables.USER);
				if (!StringUtils.isEmpty(globeSearchQuery)) {
					builder.where("(" + globeSearchQuery + ")");
				}
				builder.where(Fields.ID_USER + " != " + table.getUser().getId());
				for (int i = 0; i < table.getSortingCols(); i++) {
					builder.orderBy(nameColumn.get(i) + " " + table.getDirs().get(i));
				}
				builder.limit(table.getDisplayStart(), table.getDisplayLength());
				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				List<Object> list;
				if (!StringUtils.isEmpty(globeSearchQuery)) {
					list = new ArrayList<>();
					for (int i = 0; i < USER.length; i++) {
						list.add("%" + table.getGlobalSearch() + "%");
					}
				} else {
					list = Collections.emptyList();
				}
				return list;
			}
		};
		LOG.debug("getDate done");
		return search;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.dtable.DTSpecification#getTotalAfterFilter()
	 */
	@Override
	public Specification<String> getTotalAfterFilter() {
		LOG.debug("getTotalAfterFilter start");
		final String globeSearchQuery = getGlobaleSearchQuery();
		final DTBean table = getTable();
		Specification<String> totalAfterFilterSpec = new Specification<String>() {
			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.column(SqlFunction.count("*")).from(Tables.USER);
				if (!StringUtils.isEmpty(globeSearchQuery)) {
					builder.where("(" + globeSearchQuery + ")");
				}
				builder.where(Fields.ID_USER + " != " + table.getUser().getId());
				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				return getData().getParameters();
			}
		};
		LOG.debug("getTotalAfterFilter done");
		return totalAfterFilterSpec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.dtable.CreatorDTSpecification#setDBbean(ua.
	 * nure.vydrya.SummaryTask4.web.bean.DTBean)
	 */
	@Override
	public DTSpecification<String> setDBbean(DTBean table) {
		LOG.debug("DTSpecification start ");
		setNameColumn(getNameColumnSort(table.getCols(), USER));
		setTable(table);
		initGlobaleSearchQuery(table, USER);

		LOG.debug("DTSpecification done");
		return this;
	}
}