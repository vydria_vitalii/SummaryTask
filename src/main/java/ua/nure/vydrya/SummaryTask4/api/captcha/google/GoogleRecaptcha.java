package ua.nure.vydrya.SummaryTask4.api.captcha.google;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Objects;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.captcha.Captcha;
import ua.nure.vydrya.SummaryTask4.exception.CaptchaException;

/**
 * Implementation GoogleRecaptcha. <br>
 * GoogleRecaptcha implements {@link Captcha}.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class GoogleRecaptcha implements Captcha {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(GoogleRecaptcha.class);
	private static final String SITE_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";
	private String secretKey;

	/**
	 * Constructor with parameters.
	 * 
	 * @param secretKey
	 *            - secret key google recaptcha.
	 */
	public GoogleRecaptcha(String secretKey) {
		this.secretKey = secretKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.recaptcha.Recaptcha#verify(java.lang.String)
	 */
	@Override
	public boolean verify(String gRecaptchaResponse) throws CaptchaException {
		LOG.debug("verify start");
		if (StringUtils.isEmpty(gRecaptchaResponse)) {
			return false;
		}
		try {
			URL verifyUrl = new URL(SITE_VERIFY_URL);

			HttpsURLConnection con = null;
			// Open Connection to URL
			try {
				con = (HttpsURLConnection) verifyUrl.openConnection();

				// Add Request Header
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", "Mozilla/5.0");
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

				// Data will be sent to the server.
				StringBuilder postParams = new StringBuilder();
				postParams.append("secret=").append(secretKey).append("&response=")
						.append(gRecaptchaResponse);

				// Send Request
				con.setDoOutput(true);

				// Get the output stream of Connection
				// Write data in this stream, which means to send data to
				// Server.
				try (OutputStream outStream = con.getOutputStream()) {
					outStream.write(postParams.toString().getBytes());
					outStream.flush();
				}

				// Response code return from server.
				int responseCode = con.getResponseCode();
				LOG.info("responseCode=" + responseCode);

				// Get the InputStream from Connection to read data sent from
				// the
				// server.
				InputStream is = con.getInputStream();
				JsonObject jsonObject = null;

				try (JsonReader jsonReader = Json.createReader(is)) {
					jsonObject = jsonReader.readObject();
				}
				LOG.info("Response: " + jsonObject);
				boolean success = jsonObject.getBoolean("success");
				LOG.debug("verify done");
				return success;
			} finally {
				if (Objects.nonNull(con)) {
					con.disconnect();
				}
			}
		} catch (Exception e) {
			LOG.warn("problem verify GoogleRecaptcha", e);
			throw new CaptchaException("problem verify GoogleRecaptcha", e);
		}
	}
}
