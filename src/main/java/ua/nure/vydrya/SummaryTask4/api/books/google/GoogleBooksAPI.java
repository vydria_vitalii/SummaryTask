package ua.nure.vydrya.SummaryTask4.api.books.google;

import java.time.Year;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.books.BooksAPI;
import ua.nure.vydrya.SummaryTask4.exception.BooksAPIException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.util.constants.GoogleAPIConstatnts;
import ua.nure.vydrya.SummaryTask4.util.date.DateUtils;
import ua.nure.vydrya.SummaryTask4.util.json.JsonUtils;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class GoogleBooksAPI implements BooksAPI {
	private static final Logger LOG = Logger.getLogger(GoogleBooksAPI.class);
	private static final int TIMEOUT = 5000;
	private static final String LINK = "https://www.googleapis.com/books/v1/volumes/";
	private String key;

	/**
	 * 
	 */
	public GoogleBooksAPI(final String key) {
		this.key = key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.api.books.BooksAPI#getBook(java.lang.String)
	 */
	@Override
	public Books getBook(String id) throws BooksAPIException {
		LOG.debug("getBook start");
		try {
			String jsonStr = JsonUtils.getStringJsonByUrl(LINK + id + "?key=" + key, TIMEOUT,
					"GET");
			LOG.debug("JSON: " + jsonStr);
			Books books = null;
			if (!StringUtils.isEmpty(jsonStr)) {

				JsonObject item = JsonUtils.jsonFromString(jsonStr);
				if (!item.containsKey(GoogleAPIConstatnts.TOTAL_ITEMS)
						&& !item.containsKey(GoogleAPIConstatnts.ERROR)) {

					JsonObject volumeInfo = item.getJsonObject(GoogleAPIConstatnts.VOLUME_INFO);
					books = new Books();
					books.setTitle(volumeInfo.getString(GoogleAPIConstatnts.TITLE));

					if (volumeInfo.containsKey(GoogleAPIConstatnts.SUBTITLE)) {
						books.setSubtile(volumeInfo.getString(GoogleAPIConstatnts.SUBTITLE));
					}

					if (volumeInfo.containsKey(GoogleAPIConstatnts.DESCRIPTION)) {
						books.setDescription(volumeInfo.getString(GoogleAPIConstatnts.DESCRIPTION));
					}

					if (volumeInfo.containsKey(GoogleAPIConstatnts.IMAGE_LINKS)) {
						books.setImageLink(volumeInfo.getJsonObject(GoogleAPIConstatnts.IMAGE_LINKS)
								.getString(GoogleAPIConstatnts.SMALL_THUMBNAIL));
					}
					if (volumeInfo.containsKey(GoogleAPIConstatnts.AUTHORS)) {
						books.setAuthor(volumeInfo.getJsonArray(GoogleAPIConstatnts.AUTHORS)
								.toString().replaceAll("\\[|\\]", ""));
					}

					if (volumeInfo.containsKey(GoogleAPIConstatnts.PUBLISHER)) {
						books.setPublicsher(volumeInfo.getString(GoogleAPIConstatnts.PUBLISHER));
					}

					if (volumeInfo.containsKey(GoogleAPIConstatnts.PUBLISHED_DATE)) {
						books.setPublishedDate(Year.parse(DateUtils.dateToYear(
								volumeInfo.getString(GoogleAPIConstatnts.PUBLISHED_DATE))));
					}

					if (volumeInfo.containsKey(GoogleAPIConstatnts.INDUSTRY_IDENTIFIERS)) {
						JsonArray industryIdentifiers = volumeInfo
								.getJsonArray(GoogleAPIConstatnts.INDUSTRY_IDENTIFIERS);

						JsonObject objTmp;
						StringBuilder strType = new StringBuilder();
						StringBuilder strIdentifier = new StringBuilder();

						for (int i = 0; i < industryIdentifiers.size(); i++) {
							objTmp = industryIdentifiers.getJsonObject(i);
							strType.append(objTmp.getString(GoogleAPIConstatnts.TYPE));
							strIdentifier.append(objTmp.getString(GoogleAPIConstatnts.INDENTIFIER));

							switch (strType.toString()) {
							case GoogleAPIConstatnts.ISBN_13:
								books.setIsbn13(strIdentifier.toString());
								break;
							case GoogleAPIConstatnts.ISBN_10:
								books.setIsbn10(strIdentifier.toString());
								break;
							case GoogleAPIConstatnts.OTHER:
								books.setOtherIdentifiers(strIdentifier.toString());
								break;
							default:
								break;
							}
							strType.setLength(0);
							strIdentifier.setLength(0);
						}
					} else {
						books.setOtherIdentifiers(item.getString(GoogleAPIConstatnts.ID));
					}

					if (volumeInfo.containsKey(GoogleAPIConstatnts.PAGE_COUNT)) {
						books.setPages(volumeInfo.getJsonNumber(GoogleAPIConstatnts.PAGE_COUNT)
								.longValueExact());
					}
					books.setLanguage(volumeInfo.getString(GoogleAPIConstatnts.LANGUAGE));
				}
			}
			LOG.info("books:" + books);
			LOG.debug("getBook done");
			return books;

		} catch (Exception e) {
			LOG.warn("Error getBook", e);
			throw new BooksAPIException("Error getBook", e);
		}
	}
}
