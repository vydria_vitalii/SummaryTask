package ua.nure.vydrya.SummaryTask4.api.captcha;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface CreatorCaptcha {
	Captcha factoryMethod(String secretKey);
}
