package ua.nure.vydrya.SummaryTask4.api.captcha;

import ua.nure.vydrya.SummaryTask4.exception.CaptchaException;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface Captcha {
	boolean verify(String gRecaptchaResponse) throws CaptchaException;
}
