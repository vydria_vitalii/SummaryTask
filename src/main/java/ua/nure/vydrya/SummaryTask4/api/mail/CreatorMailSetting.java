/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.mail;

/**
 * @author vydrya_vitaliy.
 *
 */
public interface CreatorMailSetting {
	MailSettings factoryMethod(Sender sender);
}
