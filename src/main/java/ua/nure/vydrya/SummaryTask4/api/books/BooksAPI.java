/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.api.books;

import ua.nure.vydrya.SummaryTask4.exception.BooksAPIException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface BooksAPI {
	Books getBook(String id) throws BooksAPIException;
}
