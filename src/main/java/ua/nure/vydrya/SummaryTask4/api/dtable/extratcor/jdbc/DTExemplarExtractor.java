package ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.jdbc;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor;
import ua.nure.vydrya.SummaryTask4.exception.DTException;
import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.BooksDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTExemplarExtractor implements DTExtractor<Exemplar> {
	private BooksDao booksDao;
	private DTExtractor<Books> booksExtractor;

	/**
	 * 
	 */
	public DTExemplarExtractor(DaoFactory daoFactory, DTExtractor<Books> booksExtractor) {
		this.booksDao = daoFactory.getBooksDao();
		this.booksExtractor = booksExtractor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor#extract(ua.
	 * nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	public JsonArray extract(Exemplar e) throws DTException {
		JsonArrayBuilder ja = Json.createArrayBuilder();
		ja.add(e.getId());
		try {
			ja.add(booksExtractor.extract(booksDao.findById(e.getBookId())));
		} catch (DaoException e1) {
			throw new DTException("Error DTExemplarExtractor", e1);
		}
		ja.add(e.getStatusExemplar().value());
		ja.add(e.isDeleted());
		return ja.build();
	}

}
