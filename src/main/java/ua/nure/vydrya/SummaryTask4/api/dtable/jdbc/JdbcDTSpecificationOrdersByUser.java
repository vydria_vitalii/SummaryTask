package ua.nure.vydrya.SummaryTask4.api.dtable.jdbc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.query.builder.SelectBuilder;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusOrder;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;
import ua.nure.vydrya.SummaryTask4.util.db.SqlFunction;
import ua.nure.vydrya.SummaryTask4.util.db.Tables;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class JdbcDTSpecificationOrdersByUser extends AbstractJdbcDTSpecification {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(JdbcDTSpecificationOrdersByUser.class);

	private static final String[] BOOKS = { StringUtils.EMPTY, Fields.TITLE, Fields.PUBLICSHER,
			Fields.AUTHOR, Fields.PUBLISHED_DATE };

	private static final String[] GLOBAL_SEARCH = { Fields.TITLE, Fields.SUBTITLE,
			Fields.DESCRIPTION, Fields.PUBLICSHER, Fields.AUTHOR, Fields.PUBLISHED_DATE,
			Fields.PAGES, Fields.LANGUAGE, Fields.ISBN_10, Fields.ISBN_13, Fields.OTHER_IDENTIFIERS,
			Fields.DATE_IN, Fields.DATE_OUT, Fields.STATUS_ORDER, Fields.FINE };

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification#getTotal()
	 */
	@Override
	public Specification<String> getTotal() {
		LOG.debug("getTotal satrt");
		final DTBean table = getTable();
		Specification<String> sp = new Specification<String>() {
			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.column(SqlFunction.count("*")).from(Tables.ORDER)
						.where(Fields.STATUS_ORDER + " != '" + StatusOrder.DONE.value() + "'")
						.where(Fields.USER_ID + " = " + table.getUser().getId())
						.where(Fields.DELETED_ORDER + " = false");
				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				return Collections.emptyList();
			}
		};
		LOG.debug("getTotal done");
		return sp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification#getData()
	 */
	@Override
	public Specification<String> getData() {
		LOG.debug("getData satrt");
		final String globeSearchQuery = getGlobaleSearchQuery();
		final DTBean table = getTable();
		final List<String> nameColumn = getNameColumn();
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.from(Tables.ORDER)
						.join(Tables.EXEMPLAR + "  ON " + Fields.EXEMPLAR_ID + " = "
								+ Fields.ID_EXEMPLAR)
						.join(Tables.BOOKS + " ON " + Fields.ID_BOOKS + " = " + Fields.BOOK_ID)
						.where(Fields.STATUS_ORDER + " != '" + StatusOrder.DONE.value() + "'")
						.where(Fields.USER_ID + " = " + table.getUser().getId())
						.where(Fields.DELETED_ORDER + " = false");

				if (StringUtils.isNoneEmpty(globeSearchQuery)) {
					builder.where("(" + globeSearchQuery + ")");
				}

				for (int i = 0; i < table.getSortingCols(); i++) {
					builder.orderBy(nameColumn.get(i) + " " + table.getDirs().get(i));
				}
				builder.limit(table.getDisplayStart(), table.getDisplayLength());
				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				List<Object> list;
				if (!Strings.isNullOrEmpty(globeSearchQuery)) {
					list = new ArrayList<>();
					for (int i = 0; i < GLOBAL_SEARCH.length; i++) {
						list.add("%" + table.getGlobalSearch() + "%");
					}
				} else {
					list = Collections.emptyList();
				}
				return list;
			}
		};
		LOG.debug("getData done");
		return sp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification#
	 * getTotalAfterFilter()
	 */
	@Override
	public Specification<String> getTotalAfterFilter() {
		LOG.debug("getTotalAfterFilter start");
		final DTBean table = getTable();
		final String globeSearchQuery = getGlobaleSearchQuery();
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				SelectBuilder builder = new SelectBuilder();
				builder.column(SqlFunction.count("*")).from(Tables.ORDER)
						.join(Tables.EXEMPLAR + "  ON " + Fields.EXEMPLAR_ID + " = "
								+ Fields.ID_EXEMPLAR)
						.join(Tables.BOOKS + " ON " + Fields.ID_BOOKS + " = " + Fields.BOOK_ID)
						.where(Fields.STATUS_ORDER + " != '" + StatusOrder.DONE.value() + "'")
						.where(Fields.USER_ID + " = " + table.getUser().getId())
						.where(Fields.DELETED_ORDER + " = false");

				if (StringUtils.isNoneEmpty(globeSearchQuery)) {
					builder.where("(" + globeSearchQuery + ")");
				}

				return builder.toString();
			}

			@Override
			public List<Object> getParameters() {
				return getData().getParameters();
			}
		};
		LOG.debug("getTotalAfterFilter done");
		return sp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.api.dtable.CreatorDTSpecification#setDBbean(
	 * ua.nure.vydrya.SummaryTask4.web.bean.DTBean)
	 */
	@Override
	public DTSpecification<String> setDBbean(DTBean table) {
		LOG.debug("setDBbean start");
		setNameColumn(getNameColumnSort(table.getCols(), BOOKS));
		setTable(table);
		initGlobaleSearchQuery(table, GLOBAL_SEARCH);
		LOG.debug("setDBbean done");
		return this;
	}
}
