package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusExemplar;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ExemplarExtractor implements Extractor<Exemplar> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl.
	 * BooksExtractor#extract(java.sql.ResultSet)
	 */
	@Override
	public Exemplar extract(ResultSet res) throws SQLException {
		Exemplar exemplar = new Exemplar();
		exemplar.setId(res.getLong(Fields.ID_EXEMPLAR));
		exemplar.setStatusExemplar(
				StatusExemplar.valueOf(res.getString(Fields.STATUS_EXEMPLAR).toUpperCase()));
		exemplar.setBookId(res.getLong(Fields.BOOK_ID));
		exemplar.setDeleted(res.getBoolean(Fields.DELETED_EXEMPLAR));
		return exemplar;
	}
}
