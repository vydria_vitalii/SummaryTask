package ua.nure.vydrya.SummaryTask4.repository.entity;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class Order extends Entity {
	private static final long serialVersionUID = 8829504691791043764L;
	private Long userId;
	private Long exemplarId;
	private Timestamp dateIn;
	private Date dateOut;
	private Float fine;
	private StatusOrder statusOrder;

	public Order() {
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the exemplarId
	 */
	public Long getExemplarId() {
		return exemplarId;
	}

	/**
	 * @param exemplarId
	 *            the exemplarId to set
	 */
	public void setExemplarId(Long exemplarId) {
		this.exemplarId = exemplarId;
	}

	/**
	 * @return the dateIn
	 */
	public Timestamp getDateIn() {
		return dateIn;
	}

	/**
	 * @param dateIn
	 *            the dateIn to set
	 */
	public void setDateIn(Timestamp dateIn) {
		this.dateIn = dateIn;
	}

	/**
	 * @return the dateOut
	 */
	public Date getDateOut() {
		return dateOut;
	}

	/**
	 * @param dateOut
	 *            the dateOut to set
	 */
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	/**
	 * @return the fine
	 */
	public Float getFine() {
		return fine;
	}

	/**
	 * @param fine
	 *            the fine to set
	 */
	public void setFine(Float fine) {
		this.fine = fine;
	}

	/**
	 * @return the statusOrder
	 */
	public StatusOrder getStatusOrder() {
		return statusOrder;
	}

	/**
	 * @param statusOrder
	 *            the statusOrder to set
	 */
	public void setStatusOrder(StatusOrder statusOrder) {
		this.statusOrder = statusOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateIn == null) ? 0 : dateIn.hashCode());
		result = prime * result + ((dateOut == null) ? 0 : dateOut.hashCode());
		result = prime * result + ((exemplarId == null) ? 0 : exemplarId.hashCode());
		result = prime * result + ((fine == null) ? 0 : fine.hashCode());
		result = prime * result + ((statusOrder == null) ? 0 : statusOrder.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Order)) {
			return false;
		}
		Order other = (Order) obj;
		if (dateIn == null) {
			if (other.dateIn != null) {
				return false;
			}
		} else if (!dateIn.equals(other.dateIn)) {
			return false;
		}
		if (dateOut == null) {
			if (other.dateOut != null) {
				return false;
			}
		} else if (!dateOut.equals(other.dateOut)) {
			return false;
		}
		if (exemplarId == null) {
			if (other.exemplarId != null) {
				return false;
			}
		} else if (!exemplarId.equals(other.exemplarId)) {
			return false;
		}
		if (fine == null) {
			if (other.fine != null) {
				return false;
			}
		} else if (!fine.equals(other.fine)) {
			return false;
		}
		if (statusOrder != other.statusOrder) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("Order [");
		if (userId != null) {
			builder.append("userId=");
			builder.append(userId);
			builder.append(", ");
		}
		if (exemplarId != null) {
			builder.append("exemplarId=");
			builder.append(exemplarId);
			builder.append(", ");
		}
		if (dateIn != null) {
			builder.append("dateIn=");
			builder.append(dateIn);
			builder.append(", ");
		}
		if (dateOut != null) {
			builder.append("dateOut=");
			builder.append(dateOut);
			builder.append(", ");
		}
		if (fine != null) {
			builder.append("fine=");
			builder.append(fine);
			builder.append(", ");
		}
		if (statusOrder != null) {
			builder.append("statusOrder=");
			builder.append(statusOrder);
		}
		builder.append("]");
		return builder.toString();
	}

}
