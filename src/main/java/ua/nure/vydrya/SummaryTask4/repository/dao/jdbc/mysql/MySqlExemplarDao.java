package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl.ExemplarExtractor;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.util.constants.SqlConstants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class MySqlExemplarDao extends AbstractJDBCDao<Exemplar> implements ExemplarDao {
	private static final Logger LOG = Logger.getLogger(MySqlExemplarDao.class);

	public MySqlExemplarDao(String file) {
		super(new ExemplarExtractor(), file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * prepareStatementForInsert(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement prepareStatementForInsert(PreparedStatement ps, Exemplar obj)
			throws SQLException {
		LOG.debug("prepareStatementForInsert start");
		ps.setLong(1, obj.getBookId());
		LOG.debug("prepareStatementForInsert done");
		return ps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * preparedStatemnetForUpdate(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement preparedStatemnetForUpdate(PreparedStatement ps, Exemplar obj)
			throws SQLException {
		LOG.debug("preparedStatemnetForUpdate start");
		int i = 0;
		ps.setLong(++i, obj.getBookId());
		ps.setString(++i, obj.getStatusExemplar().value());
		ps.setLong(++i, obj.getId());
		LOG.debug("preparedStatemnetForUpdate done");
		return ps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getInsertQuery()
	 */
	@Override
	protected String getInsertQuery() {
		LOG.debug("getInsertQuery start");
		LOG.debug("getInsertQuery done");
		return getSqlQuery(SqlConstants.EXEMPLAR_INSERT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getUpdateQuery()
	 */
	@Override
	protected String getUpdateQuery() {
		LOG.debug("getUpdateQuery start");
		LOG.debug("getUpdateQuery done");
		return getSqlQuery(SqlConstants.EXEMPLAR_UPDATE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getSelectByIdQuery()
	 */
	@Override
	protected String getSelectByIdQuery() {
		LOG.debug("getSelectByIdQuery start");
		LOG.debug("getSelectByIdQuery done");
		return getSqlQuery(SqlConstants.EXEMPLAR_SELECT_BY_ID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao#
	 * findExemplarByIdBooksIsDeleted(long)
	 */
	@Override
	public Exemplar findExemplarIsDeletedByIdBooks(final Long idBooks) throws DaoException {
		LOG.debug("findExemplarByIdBooksIsDeleted start");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.EXEMPLAR_SELECT_IS_DELETEB_BY_BOOKS_ID);
			}

			@Override
			public List<Object> getParameters() {
				ArrayList<Object> list = new ArrayList<>();
				list.add(idBooks);
				return list;
			}
		};

		Exemplar exemplar = getResulIsList(query(sp));
		LOG.info("result: " + exemplar);
		LOG.debug("findExemplarByIdBooksIsDeleted done");
		return exemplar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getDeletedQuery()
	 */
	@Override
	protected String getDeletedQuery() {
		LOG.debug("getDeletedQuery start");
		LOG.debug("getDeletedQuery done");
		return getSqlQuery(SqlConstants.EXEMPLAR_DELETED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao#
	 * findExemplarByIdBooks(java.lang.Long)
	 */
	@Override
	public Exemplar findExemplarPresentByIdBooks(final Long idBooks) throws DaoException {
		LOG.debug("findExemplarByIdBooks satrt");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.EXEMPLAR_SELECT_PRESENR_BY_ID_BOOKS);
			}

			@Override
			public List<Object> getParameters() {
				ArrayList<Object> list = new ArrayList<>();
				list.add(idBooks);
				return list;
			}
		};
		Exemplar exemplar = getResulIsList(query(sp));
		LOG.info("result: " + exemplar);

		LOG.debug("findExemplarByIdBooks done");
		return exemplar;
	}

}
