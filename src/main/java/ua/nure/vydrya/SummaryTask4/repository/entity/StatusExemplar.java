/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public enum StatusExemplar {
	PRESENT("present"), ORDER("order"), ISSUED("issued");
	private String value;

	private StatusExemplar(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
