package ua.nure.vydrya.SummaryTask4.repository.transaction;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.exception.TransactionException;

/**
 * Implementation TransactionManager.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface TransactionManager {
	/**
	 * Execute Transaction.
	 * 
	 * @param operation
	 *            {@link Operation}.
	 * @return the <E>.
	 * @throws DbException
	 *             {@link DaoException}.
	 */
	<E> E execute(Operation<E> operation) throws TransactionException;
}