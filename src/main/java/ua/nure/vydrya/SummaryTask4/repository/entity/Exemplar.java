package ua.nure.vydrya.SummaryTask4.repository.entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class Exemplar extends Entity {
	private static final long serialVersionUID = 602887577724241039L;
	private Long bookId;
	private StatusExemplar statusExemplar;

	/**
	 * Default constructor.
	 */
	public Exemplar() {
	}

	/**
	 * @return the booksId
	 */
	public Long getBookId() {
		return bookId;
	}

	/**
	 * @param booksId
	 *            the booksId to set
	 */
	public void setBookId(Long booksId) {
		this.bookId = booksId;
	}

	/**
	 * @return the statusExemplar
	 */
	public StatusExemplar getStatusExemplar() {
		return statusExemplar;
	}

	/**
	 * @param statusExemplar
	 *            the statusExemplar to set
	 */
	public void setStatusExemplar(StatusExemplar statusExemplar) {
		this.statusExemplar = statusExemplar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		result = prime * result + ((statusExemplar == null) ? 0 : statusExemplar.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Exemplar)) {
			return false;
		}
		Exemplar other = (Exemplar) obj;
		if (bookId == null) {
			if (other.bookId != null) {
				return false;
			}
		} else if (!bookId.equals(other.bookId)) {
			return false;
		}
		if (statusExemplar != other.statusExemplar) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("Exemplar [");
		if (bookId != null) {
			builder.append("booksId=");
			builder.append(bookId);
			builder.append(", ");
		}
		if (statusExemplar != null) {
			builder.append("statusExemplar=");
			builder.append(statusExemplar);
		}
		builder.append("]");
		return builder.toString();
	}
}
