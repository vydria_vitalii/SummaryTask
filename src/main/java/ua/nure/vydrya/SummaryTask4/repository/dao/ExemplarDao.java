/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.dao;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface ExemplarDao extends GenericDao<Exemplar, String> {
	Exemplar findExemplarIsDeletedByIdBooks(Long idBooks) throws DaoException;

	Exemplar findExemplarPresentByIdBooks(Long idBooks) throws DaoException;
}
