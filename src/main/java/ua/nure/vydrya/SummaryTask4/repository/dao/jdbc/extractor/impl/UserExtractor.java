package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor;
import ua.nure.vydrya.SummaryTask4.repository.entity.Role;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class UserExtractor implements Extractor<User> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor#
	 * extract(java.sql.ResultSet)
	 */
	@Override
	public User extract(ResultSet res) throws SQLException {
		User user = new User();
		user.setId(res.getLong(Fields.ID_USER));
		user.setEmail(res.getString(Fields.EMAIL));
		user.setPassword(res.getString(Fields.PASSWORD));
		user.setLibraryCard(res.getLong(Fields.LIBRARY_CARD));
		user.setName(res.getString(Fields.SURNAME));
		user.setSurname(res.getString(Fields.SURNAME));
		user.setPatronomic(res.getString(Fields.PATRONOMIC));
		user.setPassportID(res.getString(Fields.PASSPORT_ID));
		user.setAdress(res.getString(Fields.ADRESS));
		user.setRole(Role.valueOf(res.getString(Fields.ROLE).toUpperCase()));
		user.setDeleted(res.getBoolean(Fields.DELETED_USER));
		return user;
	}

}
