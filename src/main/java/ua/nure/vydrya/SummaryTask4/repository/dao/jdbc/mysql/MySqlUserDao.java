/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.connection.StorageConnection;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl.UserExtractor;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.util.constants.SqlConstants;
import ua.nure.vydrya.SummaryTask4.util.db.DbUtils;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class MySqlUserDao extends AbstractJDBCDao<User> implements UserDao {
	private static final Logger LOG = Logger.getLogger(MySqlUserDao.class);

	public MySqlUserDao(final String file) {
		super(new UserExtractor(), file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * prepareStatementForInsert(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement prepareStatementForInsert(PreparedStatement ps, User obj)
			throws SQLException {
		LOG.debug("prepareStatementForInsert start");
		int i = 0;
		ps.setString(++i, obj.getEmail());
		ps.setString(++i, obj.getPassword());

		if (Objects.nonNull(obj.getLibraryCard())) {
			ps.setLong(++i, obj.getLibraryCard());
		} else {
			ps.setNull(++i, Types.INTEGER);
		}
		ps.setString(++i, obj.getName());
		ps.setString(++i, obj.getSurname());
		ps.setString(++i, obj.getPatronomic());
		ps.setString(++i, obj.getPassportID());
		ps.setString(++i, obj.getAdress());
		ps.setString(++i, obj.getRole().value());
		LOG.debug("prepareStatementForInsert done");
		return ps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * preparedStatemnetForUpdate(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement preparedStatemnetForUpdate(PreparedStatement ps, User obj)
			throws SQLException {
		LOG.debug("preparedStatemnetForUpdate start");
		int i = 0;
		ps.setString(++i, obj.getEmail());
		ps.setString(++i, obj.getPassword());

		if (Objects.nonNull(obj.getLibraryCard())) {
			ps.setLong(++i, obj.getLibraryCard());
		} else {
			ps.setNull(++i, Types.INTEGER);
		}
		ps.setString(++i, obj.getName());
		ps.setString(++i, obj.getSurname());
		ps.setString(++i, obj.getPatronomic());
		ps.setString(++i, obj.getPassportID());
		ps.setString(++i, obj.getAdress());
		ps.setString(++i, obj.getRole().value());
		ps.setLong(++i, obj.getId());
		LOG.debug("preparedStatemnetForUpdate done");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getInsertQuery()
	 */
	@Override
	protected String getInsertQuery() {
		LOG.debug("getInsertQuery start");
		String sql = getSqlQuery(SqlConstants.USER_INSERT);
		LOG.debug("getInsertQuery done");
		return sql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getUpdateQuery()
	 */
	@Override
	protected String getUpdateQuery() {
		LOG.debug("getUpdateQuery start");
		String sql = getSqlQuery(SqlConstants.USER_UPDATE);
		LOG.debug("getUpdateQuery done");
		return sql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getSelectByIdQuery()
	 */
	@Override
	protected String getSelectByIdQuery() {
		LOG.debug("getSelectByIdQuery start");
		String sql = getSqlQuery(SqlConstants.USER_SELECT_BY_ID);
		LOG.debug("getSelectByIdQuery done");
		return sql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.UserDao#findUserByEmail()
	 */
	@Override
	public User findUserByEmail(final String email) throws DaoException {
		LOG.debug("findUserByEmail start");
		Specification<String> sp = new Specification<String>() {
			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.USER_SELECT_EMAIL);
			}

			@Override
			public List<Object> getParameters() {
				ArrayList<Object> list = new ArrayList<>();
				list.add(email);
				return list;
			}
		};
		User user = getResulIsList(query(sp));

		// LOG.debug("findUserByEmail start");
		// String sql = getQuery(SqlConstants.USER_SELECT_EMAIL);
		// LOG.info("findUserByEmail sql: " + sql);
		// Connection con = StorageConnection.getConnection();
		// PreparedStatement ps = null;
		// ResultSet rs = null;
		// User user = null;
		// try {
		// ps = con.prepareStatement(sql);
		// ps.setString(1, email);
		// rs = ps.executeQuery();
		// if (rs.next()) {
		// user = parseResultSet(rs);
		// }
		// LOG.info("findUserByEmail result: " + user);
		// } catch (SQLException e) {
		// LOG.debug("Cannot findUserByEmail", e);
		// throw new DaoException("Cannot findUserByEmail", e);
		// } finally {
		// DbUtils.closingResources(rs, ps);
		// }
		// LOG.debug("findUserByEmail done");
		// return user;
		LOG.info("result: " + user);
		LOG.debug("findUserByEmail start");
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.dao.UserDao#findUserByPasportID(
	 * java.lang.String)
	 */
	@Override
	public User findUserByPasportID(final String passportID) throws DaoException {
		LOG.debug("findUserByPasportID start");

		Specification<String> sp = new Specification<String>() {
			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.USER_SELECT_EMAIL);
			}

			@Override
			public List<Object> getParameters() {
				ArrayList<Object> list = new ArrayList<>();
				list.add(passportID);
				return list;
			}
		};
		User user = getResulIsList(query(sp));
		LOG.info("result: " + user);
		LOG.debug("findUserByPasportID done");
		return user;
		// LOG.debug("findUserByPasportID start");
		// String sql = getSqlQuery(SqlConstants.USER_SELECT_PASSPORT_ID);
		// LOG.info("findUserByPasportID sql: " + sql);
		// Connection con = StorageConnection.getConnection();
		// PreparedStatement ps = null;
		// ResultSet rs = null;
		// User res = null;
		// try {
		// ps = con.prepareStatement(sql);
		// ps.setString(1, passportID);
		// rs = ps.executeQuery();
		// if (rs.next()) {
		// res = parseResultSet(rs);
		// }
		// LOG.info("findUserByPasportID res: " + res);
		// } catch (SQLException e) {
		// LOG.warn("Cannot findUserByPasportID", e);
		// throw new DaoException("Cannot findUserByPasportID", e);
		// } finally {
		// DbUtils.closingResources(rs, ps);
		// }
		// LOG.debug("findUserByPasportID done");
		// return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.dao.UserDao#findMaxLibraryCard()
	 */
	@Override
	public Long findMaxLibraryCard() throws DaoException {
		LOG.debug("findMaxLibraryCard satrt");
		String sql = getSqlQuery(SqlConstants.USER_SELECT_MAX_LIBRARY_CARD);
		LOG.info("sql: " + sql);
		Connection con = StorageConnection.getConnection();
		Statement st = null;
		ResultSet rs = null;
		Long res = 0L;
		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			if (rs.next()) {
				res = rs.getLong(Fields.LIBRARY_CARD);
			}
		} catch (SQLException e) {
			LOG.warn("Cannot findMaxLibraryCard", e);
			throw new DaoException("Cannot findMaxLibraryCard", e);
		} finally {
			DbUtils.closingResources(rs, st);
		}
		LOG.debug("findMaxLibraryCard done");
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getDeletedQuery()
	 */
	@Override
	protected String getDeletedQuery() {
		LOG.debug("getDeletedQuery start");
		LOG.debug("getDeletedQuery done");
		return getSqlQuery(SqlConstants.USER_DELETED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.UserDao#
	 * findAllActiveUserByOrder()
	 */
	@Override
	public List<User> findAllActiveUserByOrder() throws DaoException {
		LOG.debug("findAllActiveUserByOrder start");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.USER_TEST);
			}

			@Override
			public List<Object> getParameters() {
				return Collections.emptyList();
			}
		};
		LOG.debug("findAllActiveUserByOrder done");
		return query(sp);
	}
}
