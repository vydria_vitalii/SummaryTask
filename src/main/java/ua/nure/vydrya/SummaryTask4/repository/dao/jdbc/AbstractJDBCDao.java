package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.repository.dao.GenericDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.connection.StorageConnection;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.Entity;
import ua.nure.vydrya.SummaryTask4.util.db.DbUtils;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class AbstractJDBCDao<T extends Entity> implements GenericDao<T, String> {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(AbstractJDBCDao.class);
	private Extractor<T> extractor;
	private Properties prop;

	public AbstractJDBCDao(Extractor<T> extractor, final String file) {
		this.extractor = extractor;
		prop = initProperties(file);
	}

	protected String getSqlQuery(String nameQuery) {
		LOG.debug("getQuery start");
		String res = prop.getProperty(nameQuery);
		LOG.debug("getQuery done");
		return res;
	}

	protected T parseResultSet(final ResultSet rs) throws SQLException {
		LOG.debug("parseResultSet start");
		T res = extractor.extract(rs);
		LOG.info("result parseResultSet:" + res);
		LOG.debug("parseResultSet done");
		return res;
	}

	/**
	 * Parses ResultSet object and returns a list of relevant content ResultSet.
	 * 
	 * @param rs
	 *            - {@link ResultSet}.
	 * @return the {@link ResultSet}.
	 * @throws SQLException
	 *             - {@link SQLException}.
	 */
	protected List<T> parseListResultSet(final ResultSet rs) throws SQLException {
		LOG.debug("parseListResultSet start");
		List<T> list = new ArrayList<>();
		while (rs.next()) {
			list.add(parseResultSet(rs));
		}
		LOG.info("result parseListResultSet: " + list);
		LOG.debug("parseListResultSet done");
		return list;
	}

	protected abstract PreparedStatement prepareStatementForInsert(PreparedStatement ps, T obj)
			throws SQLException;

	protected abstract PreparedStatement preparedStatemnetForUpdate(PreparedStatement ps, T obj)
			throws SQLException;

	protected abstract String getInsertQuery();

	protected abstract String getUpdateQuery();

	protected abstract String getSelectByIdQuery();

	protected abstract String getDeletedQuery();

	private Properties initProperties(final String file) {
		LOG.debug("initProperties start");
		Properties propTmp = new Properties();
		try {
			propTmp.load(getClass().getClassLoader().getResourceAsStream(file));
		} catch (IOException e) {
			LOG.error("Error load prop: " + e);
			throw new ResourceException("Error load prop: " + e);
		}
		LOG.debug("initProperties done");
		return propTmp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see repository.dao.GenericDao#insert(repository.entity.Entity)
	 */
	@Override
	public T insert(T object) throws DaoException {
		LOG.debug("insert start");
		String sql = getInsertQuery();
		LOG.info("insert sql: " + sql);
		Connection con = StorageConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long id = null;
		try {
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps = prepareStatementForInsert(ps, object);
			ps.execute();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getLong(1);
			}
			object.setId(id);
			LOG.info("result insert: " + object);
		} catch (SQLException e) {
			LOG.warn("Cannot insert", e);
			throw new DaoException("Cannot insert", e);
		} finally {
			DbUtils.closingResources(rs, ps);
		}
		LOG.debug("insert done");
		return object;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see repository.dao.GenericDao#findById(java.lang.Long)
	 */
	@Override
	public T findById(final Long id) throws DaoException {
		LOG.debug("findById start");

		Specification<String> sp = new Specification<String>() {
			@Override
			public String getQuery() {
				return getSelectByIdQuery();
			}

			@Override
			public List<Object> getParameters() {
				ArrayList<Object> list = new ArrayList<>();
				list.add(id);
				return list;
			}
		};
		T t = getResulIsList(query(sp));
		LOG.info("findById result: " + t);

		LOG.debug("findById done");
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see repository.dao.GenericDao#update(repository.entity.Entity)
	 */
	@Override
	public boolean update(T object) throws DaoException {
		LOG.debug("update start");
		String sql = getUpdateQuery();
		LOG.info("update sql: " + sql);
		PreparedStatement ps = null;
		ResultSet rs = null;
		int res = -1;
		Connection con = StorageConnection.getConnection();
		try {
			ps = con.prepareStatement(sql);
			ps = preparedStatemnetForUpdate(ps, object);
			res = ps.executeUpdate();
		} catch (SQLException e) {
			LOG.warn("Cannot update", e);
			throw new DaoException("Canoot update", e);
		} finally {
			DbUtils.closingResources(rs, ps);
		}
		LOG.debug("update done");
		return res > 0 ? true : false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see repository.dao.GenericDao#deleted(java.lang.Long, java.lang.Boolean)
	 */
	@Override
	public boolean deleted(Long id, Boolean isDeleted) throws DaoException {
		LOG.debug("deleted start");
		String sql = getDeletedQuery();
		LOG.info("sql: " + sql);

		Connection con = StorageConnection.getConnection();
		PreparedStatement ps = null;
		int res = -1;
		try {
			ps = con.prepareStatement(sql);
			int i = 0;
			ps.setBoolean(++i, isDeleted);
			ps.setLong(++i, id);
			res = ps.executeUpdate();
		} catch (SQLException e) {
			LOG.warn("Cannot deleted", e);
			throw new DaoException("Cannot deleted", e);
		} finally {
			DbUtils.closeQuietly(ps);
		}
		LOG.info("result: " + res);
		LOG.debug("deleted done");
		return res >= 1 ? true : false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.GenericDao#query(ua.nure.
	 * vydrya.SummaryTask4.repository.dao.specification.Specification)
	 */
	@Override
	public List<T> query(Specification<? extends String> specification) throws DaoException {
		LOG.debug("query start");
		String sql = specification.getQuery();
		Connection con = StorageConnection.getConnection();
		LOG.info("sql: " + sql);
		Statement st = null;
		ResultSet rs = null;
		List<T> res = null;
		List<Object> param = specification.getParameters();
		try {
			if (Objects.nonNull(param)) {
				PreparedStatement ps = con.prepareStatement(sql);
				st = setQueryParameters(ps, param);
				rs = ps.executeQuery();
			} else {
				st = con.createStatement();
				rs = st.executeQuery(sql);
			}
			res = parseListResultSet(rs);
			LOG.info("res: " + res);
		} catch (SQLException e) {
			LOG.warn("Cannot query: ", e);
			throw new DaoException("Cannot query: ", e);
		} finally {
			DbUtils.closingResources(rs, st);
		}

		LOG.debug("query done");
		return res;
	}

	private PreparedStatement setQueryParameters(PreparedStatement ps, List<Object> param)
			throws SQLException {
		LOG.debug("setQueryParameters start");
		int i = 0;
		for (Object obj : param) {
			ps.setObject(++i, obj);
		}
		LOG.debug("setQueryParameters done");
		return ps;
	}

	protected T getResulIsList(List<T> res) {
		return res.isEmpty() ? null : res.iterator().next();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.GenericDao#count(ua.nure.
	 * vydrya.SummaryTask4.repository.dao.specification.Specification)
	 */
	@Override
	public int count(Specification<? extends String> specification) throws DaoException {
		LOG.debug("count start");
		String sql = specification.getQuery();
		LOG.info("sql: " + sql);
		Connection con = StorageConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		try {
			ps = con.prepareStatement(sql);
			ps = setQueryParameters(ps, specification.getParameters());
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOG.warn("Cannot count", e);
			throw new DaoException("Cannot count", e);
		} finally {
			DbUtils.closingResources(rs, ps);
		}
		LOG.debug("count done");
		return count;
	}
}
