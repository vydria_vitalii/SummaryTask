package ua.nure.vydrya.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface UserDao extends GenericDao<User, String> {

	/**
	 * @param email
	 * @return
	 * @throws DaoException
	 */
	User findUserByEmail(String email) throws DaoException;

	User findUserByPasportID(String passportID) throws DaoException;

	Long findMaxLibraryCard() throws DaoException;

	List<User> findAllActiveUserByOrder() throws DaoException;
}
