package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.BooksDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl.BooksExtractor;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.util.constants.SqlConstants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class MySqlDaoBooksDao extends AbstractJDBCDao<Books> implements BooksDao {
	private static final Logger LOG = Logger.getLogger(MySqlDaoBooksDao.class);

	public MySqlDaoBooksDao(String file) {
		super(new BooksExtractor(), file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.dao.BooksDao#findBooksByISBN(java.
	 * lang.String)
	 */
	@Override
	public Books findBooksByIdentifiers(final String isbn13, final String isbn10,
			final String other) throws DaoException {
		LOG.debug("findBooksByISBN start");

		Specification<String> specification = new Specification<String>() {

			@Override
			public String getQuery() {
				String sql = getSqlQuery(SqlConstants.BOOKS_SELECK_BY_IDENTIFIERS);
				LOG.info("sql: " + sql);
				return sql;
			}

			@Override
			public List<Object> getParameters() {
				ArrayList<Object> param = new ArrayList<>();
				param.add(Strings.emptyToNull(isbn13));
				param.add(Strings.emptyToNull(isbn10));
				param.add(Strings.emptyToNull(other));
				return param;
			}
		};
		Books books = getResulIsList(query(specification));
		LOG.info("result: " + books);
		LOG.debug("findBooksByISBN done");
		return books;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * prepareStatementForInsert(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement prepareStatementForInsert(PreparedStatement ps, Books obj)
			throws SQLException {
		LOG.debug("prepareStatementForInsert start");
		int i = 0;
		ps.setString(++i, obj.getTitle());

		setStringPrepareStatement(ps, obj.getSubtile(), Types.LONGVARCHAR, ++i);
		setStringPrepareStatement(ps, obj.getDescription(), Types.LONGVARCHAR, ++i);
		setStringPrepareStatement(ps, obj.getPublicsher(), Types.LONGVARCHAR, ++i);
		setStringPrepareStatement(ps, obj.getAuthor(), Types.LONGVARCHAR, ++i);

		if (Objects.nonNull(obj.getPublishedDate())) {
			ps.setString(++i, obj.getPublishedDate().toString());
		} else {
			ps.setNull(++i, Types.DATE);
		}

		setStringPrepareStatement(ps, obj.getImageLink(), Types.LONGVARCHAR, ++i);

		ps.setString(++i, obj.getLanguage());

		if (Objects.nonNull(obj.getPages())) {
			ps.setLong(++i, obj.getPages());
		} else {
			ps.setNull(++i, Types.INTEGER);
		}

		setStringPrepareStatement(ps, obj.getIsbn10(), Types.VARCHAR, ++i);
		setStringPrepareStatement(ps, obj.getIsbn13(), Types.VARCHAR, ++i);
		setStringPrepareStatement(ps, obj.getOtherIdentifiers(), Types.VARCHAR, ++i);

		LOG.debug("prepareStatementForInsert done");
		return ps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * preparedStatemnetForUpdate(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement preparedStatemnetForUpdate(PreparedStatement ps, Books obj)
			throws SQLException {
		LOG.error("non used this method");
		throw new UnsupportedOperationException("non used this method");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getInsertQuery()
	 */
	@Override
	protected String getInsertQuery() {
		LOG.debug("getInsertQuery start");
		LOG.debug("getInsertQuery done");
		return getSqlQuery(SqlConstants.BOOKS_INSERT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getUpdateQuery()
	 */
	@Override
	protected String getUpdateQuery() {
		LOG.error("non used this method");
		throw new UnsupportedOperationException("non used this method");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getSelectByIdQuery()
	 */
	@Override
	protected String getSelectByIdQuery() {
		LOG.debug("getSelectByIdQuery start");
		LOG.debug("getSelectByIdQuery done");
		return getSqlQuery(SqlConstants.BOOKS_SELECT_BY_ID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getDeletedQuery()
	 */
	@Override
	protected String getDeletedQuery() {
		LOG.error("non used this method");
		throw new UnsupportedOperationException("non used this method");
	}

	private void setStringPrepareStatement(PreparedStatement ps, String str, int types, int i)
			throws SQLException {
		LOG.debug("setStringPrepareStatement start");
		if (Objects.nonNull(str)) {
			ps.setString(i, str);
		} else {
			ps.setNull(i, types);
		}
		LOG.debug("setStringPrepareStatement done");
	}
}
