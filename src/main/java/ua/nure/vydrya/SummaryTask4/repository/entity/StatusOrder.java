package ua.nure.vydrya.SummaryTask4.repository.entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public enum StatusOrder {
	OPEN("open"), ACTIVE("active"), DONE("done");
	private String value;

	private StatusOrder(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
}
