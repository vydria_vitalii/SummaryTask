package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl.OrderExtractor;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.util.constants.SqlConstants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class MySqlOrderDao extends AbstractJDBCDao<Order> implements OrderDao {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(MySqlOrderDao.class);

	/**
	 * @param extractor
	 * @param file
	 */
	public MySqlOrderDao(String file) {
		super(new OrderExtractor(), file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * prepareStatementForInsert(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement prepareStatementForInsert(PreparedStatement ps, Order obj)
			throws SQLException {
		LOG.debug("prepareStatementForInsert start");
		int i = 0;
		ps.setLong(++i, obj.getUserId());
		ps.setLong(++i, obj.getExemplarId());
		LOG.debug("prepareStatementForInsert done");
		return ps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * preparedStatemnetForUpdate(java.sql.PreparedStatement,
	 * ua.nure.vydrya.SummaryTask4.repository.entity.Entity)
	 */
	@Override
	protected PreparedStatement preparedStatemnetForUpdate(PreparedStatement ps, Order obj)
			throws SQLException {
		LOG.debug("preparedStatemnetForUpdate start");
		int i = 0;
		ps.setLong(++i, obj.getUserId());
		ps.setLong(++i, obj.getExemplarId());

		if (Objects.nonNull(obj.getDateIn())) {
			ps.setTimestamp(++i, obj.getDateIn());
		} else {
			ps.setNull(++i, Types.TIMESTAMP);
		}

		if (Objects.nonNull(obj.getDateOut())) {
			ps.setDate(++i, obj.getDateOut());
		} else {
			ps.setNull(++i, Types.DATE);
		}

		ps.setFloat(++i, obj.getFine());
		ps.setString(++i, obj.getStatusOrder().value());
		ps.setLong(++i, obj.getId());
		LOG.debug("preparedStatemnetForUpdate done");
		return ps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getInsertQuery()
	 */
	@Override
	protected String getInsertQuery() {
		LOG.debug("getInsertQuery satrt");
		LOG.debug("getInsertQuery done");
		return getSqlQuery(SqlConstants.ORDER_INSERT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getUpdateQuery()
	 */
	@Override
	protected String getUpdateQuery() {
		LOG.debug("getUpdateQuery start");
		LOG.debug("getUpdateQuery done");
		return getSqlQuery(SqlConstants.ORDER_UPDATE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getSelectByIdQuery()
	 */
	@Override
	protected String getSelectByIdQuery() {
		LOG.debug("getSelectByIdQuery start");
		LOG.debug("getSelectByIdQuery done");
		return getSqlQuery(SqlConstants.ORDER_SELECT_BY_ID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.AbstractJDBCDao#
	 * getDeletedQuery()
	 */
	@Override
	protected String getDeletedQuery() {
		LOG.debug("getDeletedQuery start");
		LOG.debug("getDeletedQuery done");
		return getSqlQuery(SqlConstants.ORDER_DELETED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao#
	 * findOrdersByActiveAndDateOutOfBoundaries()
	 */
	@Override
	public List<Order> findFineOrders() throws DaoException {
		LOG.debug("findOrdersByActiveAndDateOutOfBoundaries start");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.ORDER_SELECT_FINE);
			}

			@Override
			public List<Object> getParameters() {
				return Collections.emptyList();
			}
		};
		List<Order> res = query(sp);
		LOG.info("findFineOrders: " + res);
		LOG.debug("findOrdersByActiveAndDateOutOfBoundaries done");
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao#findOrdersByUserId(
	 * java.lang.Long)
	 */
	@Override
	public List<Order> findOrdersByUserId(final Long userId) throws DaoException {
		LOG.debug("findOrdersByUserId start");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.ORDER_SELECT_FIND_BU_USER_ID);
			}

			@Override
			public List<Object> getParameters() {
				List<Object> list = new ArrayList<>();
				list.add(userId);
				return list;
			}
		};
		List<Order> res = query(sp);
		LOG.debug("findOrdersByUserId done");
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao#countOrdersByUser(
	 * java.lang.Long)
	 */
	@Override
	public int countOrdersByUser(final Long idUser) throws DaoException {
		LOG.debug("countOrdersByUser start");
		Specification<String> sp = new Specification<String>() {

			@Override
			public String getQuery() {
				return getSqlQuery(SqlConstants.ORDER_TEST);
			}

			@Override
			public List<Object> getParameters() {
				List<Object> list = new ArrayList<>();
				list.add(idUser);
				return list;
			}
		};
		LOG.debug("countOrdersByUser done");
		return count(sp);
	}
}
