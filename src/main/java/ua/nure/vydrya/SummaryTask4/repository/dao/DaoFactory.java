package ua.nure.vydrya.SummaryTask4.repository.dao;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.mysql.MySqlDaoFactory;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class DaoFactory {
	private static final Logger LOG = Logger.getLogger(DaoFactory.class);

	public static final String MY_SQL = "MySQL";

	public abstract UserDao getUserDao();

	public abstract BooksDao getBooksDao();

	public abstract ExemplarDao getExemplarDao();

	public abstract OrderDao getOrderDao();

	public static DaoFactory getDaoFactory(final String whichFactory) {
		if (Objects.equals(whichFactory, MY_SQL)) {
			return new MySqlDaoFactory();
		} else {
			LOG.warn("implementation is not found - number: " + whichFactory);
			throw new IllegalArgumentException("implementation is not found");
		}
	}
}
