package ua.nure.vydrya.SummaryTask4.repository.transaction.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.connection.StorageConnection;
import ua.nure.vydrya.SummaryTask4.repository.transaction.Operation;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.util.db.DbUtils;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class JdbcTransactionManager implements TransactionManager {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(JdbcTransactionManager.class);
	private final DataSource dataSource;

	/**
	 * @param dataSource
	 */
	public JdbcTransactionManager(javax.sql.DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager#
	 * execute(ua.nure.vydrya.SummaryTask4.repository.transaction.Operation)
	 */
	@Override
	public <E> E execute(final Operation<E> operation) throws TransactionException {
		Connection con = null;
		E res = null;
		try {
			con = dataSource.getConnection();
			StorageConnection.setConnection(con);
			res = operation.execute();
			commit(con);
		} catch (Exception e) {
			rollback(con);
			LOG.error("Error transaction", e);
			throw new TransactionException("Error transaction", e);
		} finally {
			StorageConnection.removeConnection();
			DbUtils.closeQuietly(con);
		}
		return res;
	}

	protected void commit(final Connection con) throws TransactionException {
		LOG.debug("commit strat");
		try {
			con.commit();
		} catch (SQLException e) {
			LOG.warn("Error commit", e);
			throw new TransactionException("Error commit", e);
		}
		LOG.debug("commit done");
	}

	protected void rollback(final Connection con) {
		LOG.debug("rollback start");
		if (Objects.nonNull(con)) {
			try {
				con.rollback();
			} catch (SQLException e) {
				LOG.warn("Error rollback", e);
			}
		}
		LOG.debug("rollback done");
	}
}
