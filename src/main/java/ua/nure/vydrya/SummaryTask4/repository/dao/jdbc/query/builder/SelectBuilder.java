/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.query.builder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * @author vydrya_vitaliy.
 *
 */
public class SelectBuilder implements Serializable {
	private static final long serialVersionUID = 895656510546625886L;

	private List<String> columns = new ArrayList<>();
	private List<String> tables = new ArrayList<>();
	private List<String> joins = new ArrayList<>();
	private List<String> leftJoins = new ArrayList<>();
	private List<String> wheres = new ArrayList<>();
	private List<String> orderBys = new ArrayList<>();
	private List<String> groupBys = new ArrayList<>();
	private List<String> havings = new ArrayList<>();
	private String limit;

	public SelectBuilder() {
	}

	public SelectBuilder(String table) {
		columns.add(table);
	}

	private void appendList(StringBuilder sql, List<String> list, String init, String sep) {
		boolean first = true;
		for (String s : list) {
			if (first) {
				sql.append(init);
			} else {
				sql.append(sep);
			}
			sql.append(s);
			first = false;
		}
	}

	public SelectBuilder column(String name) {
		columns.add(name);
		return this;
	}

	public SelectBuilder column(String name, boolean groupBy) {
		columns.add(name);
		if (groupBy) {
			groupBys.add(name);
		}
		return this;
	}

	public SelectBuilder limit(int amount) {
		limit = String.valueOf(amount);
		return this;
	}

	public SelectBuilder limit(int start, int amount) {
		limit = String.valueOf(start) + ", " + String.valueOf(amount);
		return this;
	}

	public SelectBuilder from(String table) {
		tables.add(table);
		return this;
	}

	public SelectBuilder groupBy(String expr) {
		groupBys.add(expr);
		return this;
	}

	public SelectBuilder having(String expr) {
		havings.add(expr);
		return this;
	}

	public SelectBuilder join(String join) {
		joins.add(join);
		return this;
	}

	public SelectBuilder leftJoin(String join) {
		leftJoins.add(join);
		return this;
	}

	public SelectBuilder orderBy(String name) {
		orderBys.add(name);
		return this;
	}

	public SelectBuilder where(String expr) {
		wheres.add(expr);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sql = new StringBuilder("SELECT ");
		if (columns.isEmpty()) {
			sql.append("*");
		} else {
			appendList(sql, columns, "", ", ");
		}

		appendList(sql, tables, " FROM ", ", ");
		appendList(sql, joins, " JOIN ", " JOIN ");
		appendList(sql, leftJoins, " LEFT JOIN ", " LEFT JOIN ");
		appendList(sql, wheres, " WHERE ", " AND ");
		appendList(sql, groupBys, " GROUP BY ", ", ");
		appendList(sql, havings, " HAVING ", " AND ");
		appendList(sql, orderBys, " ORDER BY ", ", ");

		if (!StringUtils.isEmpty(limit)) {
			sql.append(" LIMIT " + limit);
		}
		return sql.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columns == null) ? 0 : columns.hashCode());
		result = prime * result + ((groupBys == null) ? 0 : groupBys.hashCode());
		result = prime * result + ((havings == null) ? 0 : havings.hashCode());
		result = prime * result + ((joins == null) ? 0 : joins.hashCode());
		result = prime * result + ((leftJoins == null) ? 0 : leftJoins.hashCode());
		result = prime * result + ((orderBys == null) ? 0 : orderBys.hashCode());
		result = prime * result + ((tables == null) ? 0 : tables.hashCode());
		result = prime * result + ((wheres == null) ? 0 : wheres.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SelectBuilder)) {
			return false;
		}
		SelectBuilder other = (SelectBuilder) obj;
		if (columns == null) {
			if (other.columns != null) {
				return false;
			}
		} else if (!columns.equals(other.columns)) {
			return false;
		}
		if (groupBys == null) {
			if (other.groupBys != null) {
				return false;
			}
		} else if (!groupBys.equals(other.groupBys)) {
			return false;
		}
		if (havings == null) {
			if (other.havings != null) {
				return false;
			}
		} else if (!havings.equals(other.havings)) {
			return false;
		}
		if (joins == null) {
			if (other.joins != null) {
				return false;
			}
		} else if (!joins.equals(other.joins)) {
			return false;
		}
		if (leftJoins == null) {
			if (other.leftJoins != null) {
				return false;
			}
		} else if (!leftJoins.equals(other.leftJoins)) {
			return false;
		}
		if (orderBys == null) {
			if (other.orderBys != null) {
				return false;
			}
		} else if (!orderBys.equals(other.orderBys)) {
			return false;
		}
		if (tables == null) {
			if (other.tables != null) {
				return false;
			}
		} else if (!tables.equals(other.tables)) {
			return false;
		}
		if (wheres == null) {
			if (other.wheres != null) {
				return false;
			}
		} else if (!wheres.equals(other.wheres)) {
			return false;
		}
		return true;
	}
}
