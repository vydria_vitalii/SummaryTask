package ua.nure.vydrya.SummaryTask4.repository.entity;

import java.io.Serializable;

/**
 * Abstract entity.
 *
 * @author vydrya_vitaliy.
 * @version 1.0
 */
public abstract class Entity implements Serializable {
	private static final long serialVersionUID = -2297612942956027529L;

	/** Entity identification. */
	private Long id;

	/** Whether entity deleted or not. */
	private boolean deleted;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted
	 *            the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Entity [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		builder.append("deleted=");
		builder.append(deleted);
		builder.append("]");
		return builder.toString() + "\t";
	}
}
