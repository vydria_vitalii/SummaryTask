package ua.nure.vydrya.SummaryTask4.repository.dao.specification;

import java.util.List;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface Specification<E> {
	E getQuery();

	List<Object> getParameters();
}
