/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.entity;

import java.time.Year;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class Books extends Entity {
	private static final long serialVersionUID = -5489273692311826480L;
	private String title;
	private String subtile;
	private String description;
	private String publicsher;
	private String author;
	private Year publishedDate;
	private String imageLink;
	private String language;
	private Long pages;
	private String isbn10;
	private String isbn13;
	private String otherIdentifiers;

	/**
	 * Default constructor.
	 */
	public Books() {
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the publicsher
	 */
	public String getPublicsher() {
		return publicsher;
	}

	/**
	 * @param publicsher
	 *            the publicsher to set
	 */
	public void setPublicsher(String publicsher) {
		this.publicsher = publicsher;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the publishedDate
	 */
	public Year getPublishedDate() {
		return publishedDate;
	}

	/**
	 * @param publishedDate
	 *            the publishedDate to set
	 */
	public void setPublishedDate(Year publishedDate) {
		this.publishedDate = publishedDate;
	}

	/**
	 * @return the imageLink
	 */
	public String getImageLink() {
		return imageLink;
	}

	/**
	 * @param imageLink
	 *            the imageLink to set
	 */
	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the pages
	 */
	public Long getPages() {
		return pages;
	}

	/**
	 * @param pages
	 *            the pages to set
	 */
	public void setPages(Long pages) {
		this.pages = pages;
	}

	/**
	 * @return the isbn10
	 */
	public String getIsbn10() {
		return isbn10;
	}

	/**
	 * @param isbn10
	 *            the isbn10 to set
	 */
	public void setIsbn10(String isbn10) {
		this.isbn10 = isbn10;
	}

	/**
	 * @return the isbn13
	 */
	public String getIsbn13() {
		return isbn13;
	}

	/**
	 * @param isbn13
	 *            the isbn13 to set
	 */
	public void setIsbn13(String isbn13) {
		this.isbn13 = isbn13;
	}

	/**
	 * @return the otherIdentifiers
	 */
	public String getOtherIdentifiers() {
		return otherIdentifiers;
	}

	/**
	 * @param otherIdentifiers
	 *            the otherIdentifiers to set
	 */
	public void setOtherIdentifiers(String otherIdentifiers) {
		this.otherIdentifiers = otherIdentifiers;
	}

	/**
	 * @return the subtitile
	 */
	public String getSubtile() {
		return subtile;
	}

	/**
	 * @param subtitile
	 *            the subtitile to set
	 */
	public void setSubtile(String subtitile) {
		this.subtile = subtitile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((imageLink == null) ? 0 : imageLink.hashCode());
		result = prime * result + ((isbn10 == null) ? 0 : isbn10.hashCode());
		result = prime * result + ((isbn13 == null) ? 0 : isbn13.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((otherIdentifiers == null) ? 0 : otherIdentifiers.hashCode());
		result = prime * result + ((pages == null) ? 0 : pages.hashCode());
		result = prime * result + ((publicsher == null) ? 0 : publicsher.hashCode());
		result = prime * result + ((publishedDate == null) ? 0 : publishedDate.hashCode());
		result = prime * result + ((subtile == null) ? 0 : subtile.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Books)) {
			return false;
		}
		Books other = (Books) obj;
		if (author == null) {
			if (other.author != null) {
				return false;
			}
		} else if (!author.equals(other.author)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (imageLink == null) {
			if (other.imageLink != null) {
				return false;
			}
		} else if (!imageLink.equals(other.imageLink)) {
			return false;
		}
		if (isbn10 == null) {
			if (other.isbn10 != null) {
				return false;
			}
		} else if (!isbn10.equals(other.isbn10)) {
			return false;
		}
		if (isbn13 == null) {
			if (other.isbn13 != null) {
				return false;
			}
		} else if (!isbn13.equals(other.isbn13)) {
			return false;
		}
		if (language == null) {
			if (other.language != null) {
				return false;
			}
		} else if (!language.equals(other.language)) {
			return false;
		}
		if (otherIdentifiers == null) {
			if (other.otherIdentifiers != null) {
				return false;
			}
		} else if (!otherIdentifiers.equals(other.otherIdentifiers)) {
			return false;
		}
		if (pages == null) {
			if (other.pages != null) {
				return false;
			}
		} else if (!pages.equals(other.pages)) {
			return false;
		}
		if (publicsher == null) {
			if (other.publicsher != null) {
				return false;
			}
		} else if (!publicsher.equals(other.publicsher)) {
			return false;
		}
		if (publishedDate == null) {
			if (other.publishedDate != null) {
				return false;
			}
		} else if (!publishedDate.equals(other.publishedDate)) {
			return false;
		}
		if (subtile == null) {
			if (other.subtile != null) {
				return false;
			}
		} else if (!subtile.equals(other.subtile)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("Books [");
		if (title != null) {
			builder.append("title=");
			builder.append(title);
			builder.append(", ");
		}
		if (subtile != null) {
			builder.append("subtitile=");
			builder.append(subtile);
			builder.append(", ");
		}
		if (description != null) {
			builder.append("description=");
			builder.append(description);
			builder.append(", ");
		}
		if (publicsher != null) {
			builder.append("publicsher=");
			builder.append(publicsher);
			builder.append(", ");
		}
		if (author != null) {
			builder.append("author=");
			builder.append(author);
			builder.append(", ");
		}
		if (publishedDate != null) {
			builder.append("publishedDate=");
			builder.append(publishedDate);
			builder.append(", ");
		}
		if (imageLink != null) {
			builder.append("imageLink=");
			builder.append(imageLink);
			builder.append(", ");
		}
		if (language != null) {
			builder.append("language=");
			builder.append(language);
			builder.append(", ");
		}
		if (pages != null) {
			builder.append("pages=");
			builder.append(pages);
			builder.append(", ");
		}
		if (isbn10 != null) {
			builder.append("isbn10=");
			builder.append(isbn10);
			builder.append(", ");
		}
		if (isbn13 != null) {
			builder.append("isbn13=");
			builder.append(isbn13);
			builder.append(", ");
		}
		if (otherIdentifiers != null) {
			builder.append("otherIdentifiers=");
			builder.append(otherIdentifiers);
			builder.append(", ");
		}
		builder.append("]");
		return builder.toString();
	}
}
