package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;

import com.google.api.client.repackaged.com.google.common.base.Strings;

import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.util.date.DateUtils;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BooksExtractor implements Extractor<Books> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor#
	 * extract(java.sql.ResultSet)
	 */
	@Override
	public Books extract(ResultSet res) throws SQLException {
		Books books = new Books();
		books.setId(res.getLong(Fields.ID_BOOKS));
		books.setTitle(res.getString(Fields.TITLE));
		books.setSubtile(res.getString(Fields.SUBTITLE));
		books.setDescription(res.getString(Fields.DESCRIPTION));
		books.setPublicsher(res.getString(Fields.PUBLICSHER));
		books.setAuthor(res.getString(Fields.AUTHOR));

		String year = res.getString(Fields.PUBLISHED_DATE);
		Year yearPublished = null;
		if (!Strings.isNullOrEmpty(year)) {
			yearPublished = Year.parse(DateUtils.dateToYear(res.getString(Fields.PUBLISHED_DATE)));
		}
		books.setPublishedDate(yearPublished);

		books.setImageLink(res.getString(Fields.IMAGE_LINK));
		books.setLanguage(res.getString(Fields.LANGUAGE));
		books.setPages(res.getLong(Fields.PAGES));
		books.setIsbn10(res.getString(Fields.ISBN_10));
		books.setIsbn13(res.getString(Fields.ISBN_13));
		books.setOtherIdentifiers(res.getString(Fields.OTHER_IDENTIFIERS));
		return books;
	}

}
