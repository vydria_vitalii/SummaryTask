package ua.nure.vydrya.SummaryTask4.repository.entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class User extends Entity {
	private static final long serialVersionUID = -2698070661909051172L;
	private String email;
	private String password;
	private Long libraryCard;
	private String name;
	private String surname;
	private String patronomic;
	private String passportID;
	private String adress;
	private Role role;

	/**
	 * Default constructor.
	 */
	public User() {
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the libraryCard
	 */
	public Long getLibraryCard() {
		return libraryCard;
	}

	/**
	 * @param libraryCard
	 *            the libraryCard to set
	 */
	public void setLibraryCard(Long libraryCard) {
		this.libraryCard = libraryCard;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *            the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the patronomic
	 */
	public String getPatronomic() {
		return patronomic;
	}

	/**
	 * @param patronomic
	 *            the patronomic to set
	 */
	public void setPatronomic(String patronomic) {
		this.patronomic = patronomic;
	}

	/**
	 * @return the passportID
	 */
	public String getPassportID() {
		return passportID;
	}

	/**
	 * @param passportID
	 *            the passportID to set
	 */
	public void setPassportID(String passportID) {
		this.passportID = passportID;
	}

	/**
	 * @return the adress
	 */
	public String getAdress() {
		return adress;
	}

	/**
	 * @param adress
	 *            the adress to set
	 */
	public void setAdress(String adress) {
		this.adress = adress;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adress == null) ? 0 : adress.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((libraryCard == null) ? 0 : libraryCard.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((passportID == null) ? 0 : passportID.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((patronomic == null) ? 0 : patronomic.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (adress == null) {
			if (other.adress != null) {
				return false;
			}
		} else if (!adress.equals(other.adress)) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (libraryCard == null) {
			if (other.libraryCard != null) {
				return false;
			}
		} else if (!libraryCard.equals(other.libraryCard)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (passportID == null) {
			if (other.passportID != null) {
				return false;
			}
		} else if (!passportID.equals(other.passportID)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (patronomic == null) {
			if (other.patronomic != null) {
				return false;
			}
		} else if (!patronomic.equals(other.patronomic)) {
			return false;
		}
		if (role != other.role) {
			return false;
		}
		if (surname == null) {
			if (other.surname != null) {
				return false;
			}
		} else if (!surname.equals(other.surname)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("User [");
		if (email != null) {
			builder.append("email=");
			builder.append(email);
			builder.append(", ");
		}
		if (password != null) {
			builder.append("password=");
			builder.append(password);
			builder.append(", ");
		}
		if (libraryCard != null) {
			builder.append("libraryCard=");
			builder.append(libraryCard);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (surname != null) {
			builder.append("surname=");
			builder.append(surname);
			builder.append(", ");
		}
		if (patronomic != null) {
			builder.append("patronomic=");
			builder.append(patronomic);
			builder.append(", ");
		}
		if (passportID != null) {
			builder.append("passportID=");
			builder.append(passportID);
			builder.append(", ");
		}
		if (adress != null) {
			builder.append("adress=");
			builder.append(adress);
			builder.append(", ");
		}
		if (role != null) {
			builder.append("role=");
			builder.append(role);
		}
		builder.append("]");
		return builder.toString();
	}
}
