package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusOrder;
import ua.nure.vydrya.SummaryTask4.util.db.Fields;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class OrderExtractor implements Extractor<Order> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor.Extractor#
	 * extract(java.sql.ResultSet)
	 */
	@Override
	public Order extract(ResultSet res) throws SQLException {
		Order order = new Order();
		order.setId(res.getLong(Fields.ID_ORDER));
		order.setUserId(res.getLong(Fields.USER_ID));
		order.setExemplarId(res.getLong(Fields.EXEMPLAR_ID));
		order.setDateIn(res.getTimestamp(Fields.DATE_IN));
		order.setDateOut(res.getDate(Fields.DATE_OUT));
		order.setFine(res.getFloat(Fields.FINE));
		// order.setEmployeeInId(res.getLong(Fields.EMPLOYEE_IN_ID));
		// order.setEmployeeOutId(res.getLong(Fields.EMPLOYEE_OUT_ID));
		order.setStatusOrder(StatusOrder.valueOf(res.getString(Fields.STATUS_ORDER).toUpperCase()));
		order.setDeleted(res.getBoolean(Fields.DELETED_ORDER));
		return order;
	}

}
