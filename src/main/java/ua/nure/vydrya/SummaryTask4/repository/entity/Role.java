package ua.nure.vydrya.SummaryTask4.repository.entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public enum Role {
	ADMIN("admin"), LIBRARIAN("librarian"), READER("reader");

	private String value;

	/**
	 * Constructor with parameters.
	 * 
	 * @param value
	 *            - the values enum.
	 */
	private Role(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
