package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.vydrya.SummaryTask4.repository.entity.Entity;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface Extractor<T extends Entity> {
	T extract(ResultSet res) throws SQLException;
}
