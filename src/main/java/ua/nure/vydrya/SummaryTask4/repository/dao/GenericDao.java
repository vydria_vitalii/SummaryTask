package ua.nure.vydrya.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.dao.specification.Specification;
import ua.nure.vydrya.SummaryTask4.repository.entity.Entity;

/**
 * Implementation GenericDao. <br>
 * 
 * @param <T>
 *            - type extends {@link Entity}.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface GenericDao<T extends Entity, S> {

	/**
	 * Creates a new entry , the corresponding object object.
	 * 
	 * @param object
	 *            - object to insert.
	 * @return the insert object.
	 * @throws DaoException
	 *             {@link DaoException}.
	 */
	T insert(T object) throws DaoException;

	/**
	 * Find record by id.
	 * 
	 * @param id
	 *            - index.
	 * @return the find object.
	 * @throws DaoException
	 *             {@link DaoException}.
	 */
	T findById(Long id) throws DaoException;

	/**
	 * Saves the state of the object in the database.
	 * 
	 * @param object
	 *            - object update in the database.
	 * @return status update.
	 * @throws DaoException
	 *             {@link DaoException}.
	 */
	boolean update(T object) throws DaoException;

	List<T> query(Specification<? extends S> specification) throws DaoException;

	int count(Specification<? extends S> specification) throws DaoException;

	/**
	 * Deleted by id.
	 * 
	 * @param id
	 *            - id.
	 * @param isDeleted
	 *            - deleted.
	 * @return status deleted.
	 * @throws DaoException
	 *             {@link DaoException}.
	 */
	boolean deleted(Long id, Boolean isDeleted) throws DaoException;

}
