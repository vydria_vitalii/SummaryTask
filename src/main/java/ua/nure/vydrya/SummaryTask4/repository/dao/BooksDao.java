package ua.nure.vydrya.SummaryTask4.repository.dao;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface BooksDao extends GenericDao<Books, String> {
	Books findBooksByIdentifiers(String isbn13, String isbn10, String other) throws DaoException;
}
