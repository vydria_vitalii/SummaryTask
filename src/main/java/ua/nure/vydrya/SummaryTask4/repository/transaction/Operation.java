/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.transaction;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface Operation<T> {
	T execute() throws Exception;
}
