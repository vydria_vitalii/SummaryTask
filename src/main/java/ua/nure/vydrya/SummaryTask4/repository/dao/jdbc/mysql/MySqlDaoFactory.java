package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.mysql;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.repository.dao.BooksDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class MySqlDaoFactory extends DaoFactory {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(MySqlDaoFactory.class);
	private static final String FILE_QUERY = "sql.properties";

	// private InputStream is;
	private UserDao userDao;
	private BooksDao booksDao;
	private ExemplarDao exemplarDao;
	private OrderDao orderDao;

	// private InputStream initInputStream(String filePath) {
	// LOG.debug("initInputStream");
	// LOG.debug("initInputStream done");
	// return getClass().getClassLoader().getResourceAsStream(filePath);
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory#getUserDao()
	 */
	@Override
	public UserDao getUserDao() {
		LOG.debug("getUserDao start");
		if (Objects.isNull(userDao)) {
			userDao = new MySqlUserDao(FILE_QUERY);
		}
		LOG.debug("getUserDao done");
		return userDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory#getBooksDao()
	 */
	@Override
	public BooksDao getBooksDao() {
		LOG.debug("getBooksDao start");
		if (Objects.isNull(booksDao)) {
			booksDao = new MySqlDaoBooksDao(FILE_QUERY);
		}
		LOG.debug("getBooksDao done");
		return booksDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory#getExemplarDao()
	 */
	@Override
	public ExemplarDao getExemplarDao() {
		LOG.debug("getExemplarDao start");
		if (Objects.isNull(exemplarDao)) {
			exemplarDao = new MySqlExemplarDao(FILE_QUERY);
		}
		LOG.debug("getExemplarDao done");
		return exemplarDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory#getOrderDao()
	 */
	@Override
	public OrderDao getOrderDao() {
		LOG.debug("getOrderDao satrt");
		if (Objects.isNull(orderDao)) {
			orderDao = new MySqlOrderDao(FILE_QUERY);
		}
		LOG.debug("getOrderDao done");
		return orderDao;
	}
}
