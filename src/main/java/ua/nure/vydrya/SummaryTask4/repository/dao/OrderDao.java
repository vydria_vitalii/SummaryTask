/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.repository.dao;

import java.util.List;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface OrderDao extends GenericDao<Order, String> {
	List<Order> findFineOrders() throws DaoException;

	List<Order> findOrdersByUserId(Long userId) throws DaoException;

	int countOrdersByUser(Long idUser) throws DaoException;
}
