package ua.nure.vydrya.SummaryTask4.repository.dao.jdbc.connection;

import java.sql.Connection;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class StorageConnection {
	private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

	public static Connection getConnection() {
		return threadLocal.get();
	}

	public static void setConnection(final Connection con) {
		threadLocal.set(con);
	}

	public static void removeConnection() {
		threadLocal.remove();
	}
}