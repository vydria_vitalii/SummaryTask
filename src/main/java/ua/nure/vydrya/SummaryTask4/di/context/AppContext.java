package ua.nure.vydrya.SummaryTask4.di.context;

/**
 * Implementation AppContext.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface AppContext {
	/**
	 * Get Bean for name.
	 * 
	 * @param name
	 *            - name Bean.
	 * @return the Bean.
	 */
	Object getBean(String name);
}
