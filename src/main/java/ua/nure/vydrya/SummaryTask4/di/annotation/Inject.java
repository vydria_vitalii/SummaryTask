package ua.nure.vydrya.SummaryTask4.di.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Implementation annotation "Inject".
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
	/**
	 * Name inject.
	 * 
	 * @return the name inject;
	 */
	String value();
}
