package ua.nure.vydrya.SummaryTask4.di.context.servlet;

import javax.servlet.ServletContext;

import ua.nure.vydrya.SummaryTask4.di.context.AppContext;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AppServletContext implements AppContext {
	private ServletContext context;

	/**
	 * @param context
	 */
	public AppServletContext(final ServletContext context) {
		this.context = context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.web.di.context.AppContext#getBean(java.lang.
	 * String)
	 */
	@Override
	public Object getBean(final String name) {
		return context.getAttribute(name);
	}
}
