package ua.nure.vydrya.SummaryTask4.di;

import static java.util.Arrays.asList;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.di.annotation.Inject;
import ua.nure.vydrya.SummaryTask4.di.context.AppContext;
import ua.nure.vydrya.SummaryTask4.exception.DependencyInjectionException;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DependencyInjection {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(DependencyInjection.class);
	private AppContext context;

	/**
	 * @param context
	 */
	public DependencyInjection(AppContext context) {
		this.context = context;
	}

	public void inject(final Class<?> startClass, final Class<?> upperBound,
			final Object injectToClass) throws DependencyInjectionException {
		LOG.debug("inject start");

		List<Field> allFieds = collectUpTo(startClass, upperBound);
		List<Field> injectField = filterInject(allFieds);
		Inject annotatio;
		for (Field field : injectField) {
			field.setAccessible(true);
			annotatio = field.getAnnotation(Inject.class);

			LOG.info("Annotatio: " + annotatio);

			String beanName = annotatio.value();
			Object obj = context.getBean(beanName);
			if (Objects.isNull(obj)) {
				LOG.warn("There isn't bean name: " + beanName);
				throw new DependencyInjectionException("There isn't bean name: " + beanName);
			}
			try {
				field.set(injectToClass, obj);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				LOG.warn("Error inject bean: " + obj, e);
				throw new DependencyInjectionException("Error inject", e);
			}
		}

		LOG.debug("inject done");
	}

	private List<Field> collectUpTo(final Class<?> clazz, final Class<?> upperBound) {
		LOG.debug("collectUpTo start");
		List<Field> list = new ArrayList<>();
		Class<?> current = clazz;
		while (current != upperBound) {
			list.addAll(asList(current.getDeclaredFields()));
			current = current.getSuperclass();
		}
		LOG.debug("collectUpTo done");
		return list;
	}

	private List<Field> filterInject(List<Field> allFieds) {
		LOG.debug("filterInject start");
		List<Field> list = new ArrayList<>();
		for (Field field : allFieds) {
			Inject annotation = field.getAnnotation(Inject.class);
			if (Objects.nonNull(annotation)) {
				list.add(field);
			}
		}
		LOG.debug("filterInject done");
		return list;
	}
}
