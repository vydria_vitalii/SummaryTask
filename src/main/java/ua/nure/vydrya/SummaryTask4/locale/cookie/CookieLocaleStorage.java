package ua.nure.vydrya.SummaryTask4.locale.cookie;

import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.vydrya.SummaryTask4.locale.LocaleStorage;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class CookieLocaleStorage implements LocaleStorage {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.locale.LocaleStorage#getLocale(javax.servlet.
	 * http.HttpServletRequest)
	 */
	@Override
	public Locale getLocale(final HttpServletRequest req, final String name) {
		Cookie[] cookies = req.getCookies();
		if (Objects.nonNull(cookies)) {
			for (Cookie cookie : cookies) {
				if (Objects.equals(cookie.getName(), name)) {
					return new Locale(cookie.getValue());
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.locale.LocaleStorage#sendLocale(java.util.
	 * Locale, javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void sendLocale(final Locale locale, final HttpServletRequest req,
			final HttpServletResponse resp, final String name, final Integer maxAge) {
		Cookie cookie = new Cookie(name, locale.toString());
		cookie.setMaxAge(maxAge);
		resp.addCookie(cookie);
	}
}
