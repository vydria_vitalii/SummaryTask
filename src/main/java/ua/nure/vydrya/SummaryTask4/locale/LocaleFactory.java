package ua.nure.vydrya.SummaryTask4.locale;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.locale.cookie.CookieLocaleStorage;

/**
 * Locale factory implementation.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public abstract class LocaleFactory {
	private static final Logger LOG = Logger.getLogger(LocaleFactory.class);
	public static final String COOKIE_STORAGE = "cookie";

	/**
	 * Returns locale provider depending on the type of locale provider.
	 *
	 * @return locale provider.
	 */
	public static LocaleStorage getLocaleStorage(final String whichFactory) {
		LOG.debug("getLocaleStorage start");
		LOG.info("whichFactory: " + whichFactory);
		LOG.debug("getLocaleStorage done");
		if (Objects.equals(COOKIE_STORAGE, whichFactory)) {
			return new CookieLocaleStorage();
		} else {
			LOG.error("implementation is not found - " + whichFactory);
			throw new IllegalArgumentException("implementation is not found - " + whichFactory);
		}
	}
}
