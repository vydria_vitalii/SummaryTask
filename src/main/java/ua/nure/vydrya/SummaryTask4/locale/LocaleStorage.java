package ua.nure.vydrya.SummaryTask4.locale;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Locale Strategy.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface LocaleStorage {

	/**
	 * Returns locale.
	 *
	 * @param request
	 *            HTTP request
	 * @return locale
	 */
	Locale getLocale(HttpServletRequest req, String name);

	/**
	 * Sends locale.
	 *
	 * @param locale
	 *            the locale to send
	 * @param request
	 *            HTTP request
	 * @param response
	 *            HTTP response
	 */
	void sendLocale(Locale locale, HttpServletRequest req, HttpServletResponse resp, String name,
			Integer maxAge);
}
