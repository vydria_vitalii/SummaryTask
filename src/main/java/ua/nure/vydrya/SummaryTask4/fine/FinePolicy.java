package ua.nure.vydrya.SummaryTask4.fine;

import ua.nure.vydrya.SummaryTask4.exception.FinePolicyException;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface FinePolicy {
	Float getDayFine() throws FinePolicyException;

	Float getMaxFine() throws FinePolicyException;
}
