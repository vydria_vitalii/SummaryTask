package ua.nure.vydrya.SummaryTask4.fine.file.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.log4j.Logger;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import net.contentobjects.jnotify.JNotifyListener;
import ua.nure.vydrya.SummaryTask4.exception.JsonValidationException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.fine.FinePolicy;
import ua.nure.vydrya.SummaryTask4.util.json.ValidationUtils;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class FinePolicyImpl implements FinePolicy, JNotifyListener {
	private static final Logger LOG = Logger.getLogger(FinePolicyImpl.class);
	private static final String DAY_FINE = "day_fine";
	private static final String MAX_FINE = "max_fine";

	private float dayFine;
	private float maxFine;

	private final String jsonPath;
	private final String jsonSchema;

	public FinePolicyImpl(final String jsonPath, final String jsonSchema) {
		this.jsonPath = jsonPath;
		this.jsonSchema = jsonSchema;
		initFine(jsonPath, jsonSchema);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.fine.FinePolicy#getDayFine()
	 */
	@Override
	public Float getDayFine() {
		return this.dayFine;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.fine.FinePolicy#getMaxFine()
	 */
	@Override
	public Float getMaxFine() {
		return this.maxFine;
	}

	private void validationJson(final String jsonPath, final String jsonSchema) {
		final File jsonFile = new File(jsonPath);
		final File jsonSchemaFile = new File(jsonSchema);
		try {
			if (!ValidationUtils.isJsonValid(jsonSchemaFile, jsonFile)) {
				throw new JsonValidationException("Unvalid json");
			}
		} catch (ProcessingException | IOException e) {
			LOG.error("Error checkValidJson", e);
			throw new JsonValidationException("Error checkValidJson", e);
		}
	}

	private void initFine(final String jsonPath, final String jsonSchema) {
		LOG.debug("initFine start");
		validationJson(jsonPath, jsonSchema);
		try (JsonReader jsonReader = Json.createReader(new FileInputStream(this.jsonPath))) {
			JsonObject json = jsonReader.readObject();
			dayFine = Float
					.parseFloat(Objects.toString(json.getJsonNumber(DAY_FINE).doubleValue()));
			maxFine = Float
					.parseFloat(Objects.toString(json.getJsonNumber(MAX_FINE).doubleValue()));
			if (maxFine < dayFine) {
				throw new RuntimeException("Error logic Fine Policy: maxFine < dayFine");
			}
		} catch (FileNotFoundException e) {
			throw new ResourceException("FileNotFoundException: " + jsonPath, e);
		}
		LOG.debug("initFine done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.contentobjects.jnotify.JNotifyListener#fileCreated(int,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void fileCreated(int wd, String rootPath, String name) {
		LOG.error("non used this method");
		throw new UnsupportedOperationException("non used this method");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.contentobjects.jnotify.JNotifyListener#fileDeleted(int,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void fileDeleted(int wd, String rootPath, String name) {
		LOG.error("non used this method");
		throw new UnsupportedOperationException("non used this method");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.contentobjects.jnotify.JNotifyListener#fileModified(int,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void fileModified(int wd, String rootPath, String name) {
		LOG.debug("fileModified start");
		initFine(rootPath, this.jsonSchema);
		LOG.debug("fileModified done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.contentobjects.jnotify.JNotifyListener#fileRenamed(int,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
		LOG.error("non used this method");
		throw new UnsupportedOperationException("non used this method");
	}

}
