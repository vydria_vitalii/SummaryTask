package ua.nure.vydrya.SummaryTask4.hash.sha256;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ua.nure.vydrya.SummaryTask4.exception.HashException;
import ua.nure.vydrya.SummaryTask4.hash.Hash;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class Sha256 implements Hash {
	private static final String SHA_256 = "SHA-256";
	private static final String ENCODING = "UTF-8";

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.hash.Hash#hash(java.lang.String)
	 */
	@Override
	public String hash(final String input) throws HashException {
		try {
			MessageDigest digest = MessageDigest.getInstance(SHA_256);
			byte[] hash = digest.digest(input.getBytes(ENCODING));
			StringBuilder hexString = new StringBuilder();
			StringBuilder hex = new StringBuilder();
			for (byte var : hash) {
				hex.append(Integer.toHexString(0xFF & var));
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
				hex.setLength(0);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new HashException("Error hash", e);
		}
	}
}
