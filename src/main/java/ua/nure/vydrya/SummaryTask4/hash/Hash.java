package ua.nure.vydrya.SummaryTask4.hash;

import ua.nure.vydrya.SummaryTask4.exception.HashException;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface Hash {
	String hash(String input) throws HashException;
}
