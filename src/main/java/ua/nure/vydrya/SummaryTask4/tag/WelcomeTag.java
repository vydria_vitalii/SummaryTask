package ua.nure.vydrya.SummaryTask4.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class WelcomeTag extends SimpleTagSupport {
	private static final Logger LOG = Logger.getLogger(WelcomeTag.class);
	private String name;
	private String surname;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag()
	 */
	@Override
	public void doTag() throws JspException, IOException {
		LOG.debug("doTag start");
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		try {
			StringBuilder sb = new StringBuilder(" ");
			sb.append(surname).append(" ").append(name);
			out.print(sb);
		} catch (Exception ex) {
			LOG.warn("exeption doTag", ex);
		}
		LOG.debug("doTag done");
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *            the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
}
