package ua.nure.vydrya.SummaryTask4.service;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblockBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface AdminService {
	User addUser(User user) throws TransactionException;

	boolean blockUnblockUser(BlockUnblockBean bean) throws TransactionException;
}
