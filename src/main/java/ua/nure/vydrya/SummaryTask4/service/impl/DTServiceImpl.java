package ua.nure.vydrya.SummaryTask4.service.impl;

import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.dtable.AbstractDTFactory;
import ua.nure.vydrya.SummaryTask4.api.dtable.CreatorDTSpecification;
import ua.nure.vydrya.SummaryTask4.api.dtable.DTSpecification;
import ua.nure.vydrya.SummaryTask4.api.dtable.extratcor.DTExtractor;
import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.repository.dao.BooksDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.GenericDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.repository.entity.Entity;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.repository.transaction.Operation;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.service.DTService;
import ua.nure.vydrya.SummaryTask4.util.constants.AjaxConstants;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

//http://come2niks.com/datatables-server-side-processing-java/
/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTServiceImpl implements DTService {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(DTServiceImpl.class);
	private TransactionManager transaction;

	private UserDao userDao;
	private BooksDao booksDao;
	private ExemplarDao exemplarDao;
	private OrderDao orderDao;

	private CreatorDTSpecification<?> userSpecification;
	private CreatorDTSpecification<?> bookSpecification;
	private CreatorDTSpecification<?> exemplarSpecification;
	private CreatorDTSpecification<?> orderByUserSpecification;
	private CreatorDTSpecification<?> ordersActivation;
	private CreatorDTSpecification<?> allOrders;

	private DTExtractor<User> userExtratctor;
	private DTExtractor<Books> booksExtarctor;
	private DTExtractor<Exemplar> exemplarExtarctor;
	private DTExtractor<Order> orderExetractor;

	public DTServiceImpl(TransactionManager transactionManager, DaoFactory daoFactory,
			AbstractDTFactory<?> dtFactory, DTExtractor<User> usExtractor,
			DTExtractor<Books> booksExtarctor, DTExtractor<Exemplar> exemplarExtarctor,
			DTExtractor<Order> orderExtractor) {
		this.transaction = transactionManager;

		this.userDao = daoFactory.getUserDao();
		this.booksDao = daoFactory.getBooksDao();
		this.exemplarDao = daoFactory.getExemplarDao();
		this.orderDao = daoFactory.getOrderDao();

		userSpecification = dtFactory.getSpecificationUsers();
		bookSpecification = dtFactory.getSpecificationBooks();
		exemplarSpecification = dtFactory.getSpecificationExemplars();
		orderByUserSpecification = dtFactory.getSpecificationOrdersByUser();
		ordersActivation = dtFactory.getSpecificationOrdersActivation();
		allOrders = dtFactory.getSpecificationAllOrders();

		this.userExtratctor = usExtractor;
		this.booksExtarctor = booksExtarctor;
		this.exemplarExtarctor = exemplarExtarctor;
		this.orderExetractor = orderExtractor;
	}

	@SuppressWarnings("unchecked")
	private <T, E extends Entity, V extends CreatorDTSpecification<?>> String getDataTableResponse(
			final GenericDao<E, T> dao, V obj, final DTExtractor<E> extractor, final DTBean table)
			throws TransactionException {
		LOG.debug("getDataTableResponse start");

		final CreatorDTSpecification<T> creator = (CreatorDTSpecification<T>) obj;
		final DTSpecification<? extends T> sp = creator.setDBbean(table);

		Operation<String> operation = new Operation<String>() {
			@Override
			public String execute() throws Exception {
				LOG.debug("execute start");

				final int total = dao.count(sp.getTotal());
				List<E> list = dao.query(sp.getData());
				int totalAfter = dao.count(sp.getTotalAfterFilter());

				if (totalAfter == 0) {
					totalAfter = total;
				}

				JsonObjectBuilder resultBuilder = Json.createObjectBuilder();
				JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

				for (E e : list) {
					arrayBuilder.add(extractor.extract(e));
				}

				resultBuilder.add(AjaxConstants.TOTAL_RECORDS, total);
				resultBuilder.add(AjaxConstants.DATE, arrayBuilder);
				resultBuilder.add(AjaxConstants.ECHO, table.getEcho());
				resultBuilder.add(AjaxConstants.TOTAL_DISPLAY_RECORDS, totalAfter);

				LOG.debug("execute done");
				return resultBuilder.build().toString();
			}
		};
		LOG.debug("getDataTableResponse done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.DTService#getDataTableResponseUsers(
	 * ua.nure.vydrya.SummaryTask4.web.bean.DTBean)
	 */
	@Override
	public String getDataTableResponseUsers(DTBean table) throws TransactionException {
		LOG.debug("getDataTableResponseUsers start");
		LOG.debug("getDataTableResponseUsers done");
		return getDataTableResponse(userDao, userSpecification, userExtratctor, table);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.DTService#
	 * getDataTableResponseExemplarsBokks(ua.nure.vydrya.SummaryTask4.web.bean.
	 * DTBean)
	 */
	@Override
	public String getDataTableResponseBooks(DTBean table) throws TransactionException {
		LOG.debug("getDataTableResponseExemplarsBokks start");
		LOG.debug("getDataTableResponseExemplarsBokks done");
		return getDataTableResponse(booksDao, bookSpecification, booksExtarctor, table);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.DTService#
	 * getDataTableResponseExemplars(ua.nure.vydrya.SummaryTask4.web.bean.
	 * DTBean)
	 */
	@Override
	public String getDataTableResponseExemplars(DTBean table) throws TransactionException {
		LOG.debug("getDataTableResponseExemplars start");
		LOG.debug("getDataTableResponseExemplars done");
		return getDataTableResponse(exemplarDao, exemplarSpecification, exemplarExtarctor, table);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.DTService#
	 * getDataTableResponseOrdersByUser(ua.nure.vydrya.SummaryTask4.web.bean.
	 * DTBean)
	 */
	@Override
	public String getDataTableResponseOrdersByUser(DTBean table) throws TransactionException {
		LOG.debug("getDataTableResponseExemplars start");
		LOG.debug("getDataTableResponseExemplars done");
		return getDataTableResponse(orderDao, orderByUserSpecification, orderExetractor, table);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.DTService#
	 * getgetDataTableResponseOrdersActivation(ua.nure.vydrya.SummaryTask4.web.
	 * bean.DTBean)
	 */
	@Override
	public String getDataTableResponseOrdersActivation(DTBean table) throws TransactionException {
		LOG.debug("getgetDataTableResponseOrdersActivation start");
		LOG.debug("getgetDataTableResponseOrdersActivation done");
		return getDataTableResponse(orderDao, ordersActivation, orderExetractor, table);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.DTService#
	 * getDataTableResponseAllArders(ua.nure.vydrya.SummaryTask4.web.bean.
	 * DTBean)
	 */
	@Override
	public String getDataTableResponseAllArders(DTBean table) throws TransactionException {
		LOG.debug("getDataTableResponseAllArders start");
		LOG.debug("getDataTableResponseAllArders done");
		return getDataTableResponse(orderDao, allOrders, orderExetractor, table);
	}
}
