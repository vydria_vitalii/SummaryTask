package ua.nure.vydrya.SummaryTask4.service.impl;

import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.repository.dao.BooksDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusExemplar;
import ua.nure.vydrya.SummaryTask4.repository.transaction.Operation;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.service.BooksService;
import ua.nure.vydrya.SummaryTask4.web.bean.ExemplarBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BooksServiceImpl implements BooksService {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(BooksServiceImpl.class);

	private TransactionManager manager;
	private BooksDao booksDao;
	private ExemplarDao exemplarDao;

	/**
	 * @param manager
	 * @param booksDao
	 */
	public BooksServiceImpl(TransactionManager manager, DaoFactory daoFactory) {
		this.manager = manager;
		this.booksDao = daoFactory.getBooksDao();
		this.exemplarDao = daoFactory.getExemplarDao();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.BooksService#addBooks(com.google.api.
	 * services.books.Books)
	 */
	@Override
	public void addBooks(final Books books) throws TransactionException {
		LOG.debug("addBooks start");
		Operation<Object> operation = new Operation<Object>() {
			@Override
			public Books execute() throws DaoException {
				LOG.debug("execut strat");
				Books tmp = booksDao.findBooksByIdentifiers(books.getIsbn13(), books.getIsbn10(),
						books.getOtherIdentifiers());
				boolean booksExist = false;
				if (Objects.isNull(tmp)) {
					tmp = booksDao.insert(books);
					LOG.info("isert book: " + tmp);
					booksExist = true;
				}
				Exemplar exemplar = addExemplar(booksExist, tmp);
				LOG.info("add exemplar: " + exemplar);
				LOG.debug("execute done");
				return null;
			}
		};
		manager.execute(operation);
		LOG.debug("addBooks done");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.BooksService#editBooks(ua.nure.vydrya
	 * .SummaryTask4.repository.entity.Exemplar)
	 */
	@Override
	public boolean editBooksExemplar(final ExemplarBean exemplarBean) throws TransactionException {
		LOG.debug("editBooks start");
		final Books books = exemplarBean.getBooks();
		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws DaoException {
				LOG.debug("execute start");
				Exemplar exemplar = exemplarDao.findById(exemplarBean.getIdExemplar());
				boolean flag = false;

				if (Objects.nonNull(exemplar)
						&& Objects.equals(exemplar.getStatusExemplar().value(),
								StatusExemplar.PRESENT.value())
						&& !exemplar.isDeleted()) {
					Books tmpBooks = booksDao.findBooksByIdentifiers(books.getIsbn13(),
							books.getIsbn10(), books.getOtherIdentifiers());

					boolean booksExist = false;
					if (Objects.isNull(tmpBooks)) {
						tmpBooks = booksDao.insert(books);
						booksExist = true;
					}
					exemplarDao.deleted(exemplar.getId(), true);
					Exemplar resExemplar = addExemplar(booksExist, tmpBooks);

					LOG.info("resExemplar: " + resExemplar);
					flag = true;
				}
				LOG.debug("execute done");
				return flag;
			}
		};
		LOG.debug("editBooks done");
		return manager.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.BooksService#deleteBooksExemplr(java.
	 * lang.Long)
	 */
	@Override
	public boolean deletedBooksExemplar(final Long id) throws TransactionException {
		LOG.debug("deleteBooksExemplr satrt");

		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws DaoException {
				LOG.debug("execute satart");
				Exemplar exemplar = exemplarDao.findById(id);
				boolean flag = false;
				if (Objects.nonNull(exemplar)
						&& Objects.equals(exemplar.getStatusExemplar().value(),
								StatusExemplar.PRESENT.value())
						&& !exemplar.isDeleted()) {
					flag = exemplarDao.deleted(id, true);
				}
				LOG.debug("execute done");
				return flag;
			}
		};

		LOG.debug("deleteBooksExemplr done");
		return manager.execute(operation);
	}

	private Exemplar addExemplar(final boolean booksExist, final Books books) throws DaoException {
		LOG.debug("addExemplar addExemplar");
		Exemplar tmpExemplar;
		if (booksExist) {
			tmpExemplar = addExemplar(books);
		} else {
			tmpExemplar = exemplarDao.findExemplarIsDeletedByIdBooks(books.getId());
			if (Objects.nonNull(tmpExemplar)) {
				exemplarDao.deleted(tmpExemplar.getId(), false);
			} else {
				tmpExemplar = addExemplar(books);
			}
		}
		LOG.debug("addExemplar done");
		return tmpExemplar;
	}

	private Exemplar addExemplar(final Books books) throws DaoException {
		LOG.debug("addExemplar start");
		Exemplar exemplar = new Exemplar();
		exemplar.setBookId(books.getId());
		LOG.debug("addExemplar done");
		return exemplarDao.insert(exemplar);
	}
}
