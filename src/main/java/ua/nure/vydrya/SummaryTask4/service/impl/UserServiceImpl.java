package ua.nure.vydrya.SummaryTask4.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.mail.MailSender;
import ua.nure.vydrya.SummaryTask4.api.mail.MailSettings;
import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.exception.HashException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.hash.Hash;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;
import ua.nure.vydrya.SummaryTask4.repository.entity.Role;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.repository.transaction.Operation;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.service.UserService;
import ua.nure.vydrya.SummaryTask4.util.mail.MailMessage;
import ua.nure.vydrya.SummaryTask4.util.mail.MailSubject;
import ua.nure.vydrya.SummaryTask4.web.bean.TestBean;
import ua.nure.vydrya.SummaryTask4.web.bean.UserLoginBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class UserServiceImpl implements UserService {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);
	private TransactionManager transaction;
	private UserDao userDao;
	private OrderDao orderDao;
	private Hash hash;
	private MailSettings mailSettings;

	/**
	 * 
	 */
	public UserServiceImpl(TransactionManager transactionManager, DaoFactory daoFactory, Hash hash,
			MailSettings settings) {
		this.transaction = transactionManager;
		this.hash = hash;
		this.userDao = daoFactory.getUserDao();
		this.mailSettings = settings;
		this.orderDao = daoFactory.getOrderDao();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.UserService#login(ua.nure.vydrya.
	 * SummaryTask4.web.bean.UserLoginBean)
	 */
	@Override
	public User login(final UserLoginBean userLoginBean) throws TransactionException {
		LOG.debug("login start");
		Operation<User> operation = new Operation<User>() {

			@Override
			public User execute() throws DaoException {
				LOG.debug("start operation");
				User user = userDao.findUserByEmail(userLoginBean.getLogin());
				if (Objects.nonNull(user) && !user.isDeleted()) {
					try {
						String hashPassword = hash.hash(userLoginBean.getPassword());
						if (!Objects.equals(hashPassword, user.getPassword())) {
							user = null;
						}
					} catch (HashException e) {
						LOG.debug("Error hash", e);
						throw new ResourceException("Error hash", e);
					}
				}
				LOG.info("user: " + user);
				LOG.debug("operatin done");
				return user;
			}
		};
		LOG.debug("login done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.UserService#addUser(ua.nure.vydrya.
	 * SummaryTask4.repository.entity.User)
	 */
	@Override
	public User registrations(final User user) throws TransactionException {
		LOG.debug("addUser start");
		User res = null;
		if (Objects.equals(user.getRole().value(), Role.READER.value())) {
			Operation<User> operation = new Operation<User>() {
				@Override
				public User execute() throws DaoException {
					LOG.debug("execute start");
					User resEmail = userDao.findUserByEmail(user.getEmail());
					User resPassport = userDao.findUserByPasportID(user.getPassportID());
					User userTmp = user;

					final boolean flagEmail = Objects.isNull(resEmail);
					final boolean flagPassport = Objects.isNull(resPassport);

					if (flagEmail && flagPassport) {
						String hashPass;
						try {
							hashPass = hash.hash(user.getPassword());
						} catch (HashException e) {
							LOG.debug("Error hash", e);
							throw new ResourceException("Error hash", e);
						}
						String password = user.getPassword();
						user.setPassword(hashPass);

						Long libraryCard = userDao.findMaxLibraryCard();
						userTmp.setLibraryCard(++libraryCard);

						userTmp = userDao.insert(user);
						userTmp.setPassword(password);

						if (Objects.nonNull(userTmp.getId())) {
							MailSender sender = new MailSender(mailSettings.getSession(),
									user.getEmail(), MailMessage.ADD_NEW_USER_SYBJECT,
									MailSubject.addUserMessage(user));
							sender.execute();
						} else {
							userTmp = null;
						}
					} else {
						if (!flagEmail) {
							userTmp.setEmail(null);
						}
						if (!flagPassport) {
							userTmp.setPassportID(null);
						}
					}
					LOG.info("Result add user: " + userTmp);
					LOG.debug("execute done");
					return userTmp;
				}
			};
			res = transaction.execute(operation);
		}
		LOG.debug("addUser done");
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.UserService#findAllActiveUserByOrder(
	 * )
	 */
	@Override
	public List<TestBean> testService() throws TransactionException {
		LOG.debug("findAllActiveUserByOrder start");

		Operation<List<TestBean>> operation = new Operation<List<TestBean>>() {

			@Override
			public List<TestBean> execute() throws Exception {
				LOG.debug("testService start");
				List<TestBean> beans = new ArrayList<>();
				List<User> users = userDao.findAllActiveUserByOrder();
				for (User user : users) {
					TestBean testBean = new TestBean();
					testBean.setUser(user);
					testBean.setCount(orderDao.countOrdersByUser(user.getId()));
					beans.add(testBean);
				}
				LOG.debug("testService done");
				return beans;
			}
		};

		LOG.debug("findAllActiveUserByOrder done");
		return transaction.execute(operation);
	}

}
