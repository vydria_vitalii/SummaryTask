/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.service.impl;

import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.api.mail.MailSender;
import ua.nure.vydrya.SummaryTask4.api.mail.MailSettings;
import ua.nure.vydrya.SummaryTask4.exception.DaoException;
import ua.nure.vydrya.SummaryTask4.exception.HashException;
import ua.nure.vydrya.SummaryTask4.exception.ResourceException;
import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.hash.Hash;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusExemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.repository.transaction.Operation;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.service.AdminService;
import ua.nure.vydrya.SummaryTask4.util.mail.MailMessage;
import ua.nure.vydrya.SummaryTask4.util.mail.MailSubject;
import ua.nure.vydrya.SummaryTask4.web.bean.BlockUnblockBean;

/**
 * @author vydrya_vitaliy.
 *
 */
public class AdminServiceImpl implements AdminService {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(AdminServiceImpl.class);
	private TransactionManager transaction;

	private UserDao userDao;
	private Hash hash;
	private MailSettings mailSettings;
	private OrderDao orderDao;
	private ExemplarDao exemplarDao;

	public AdminServiceImpl(Hash hash, MailSettings settings, DaoFactory daoFactory,
			TransactionManager transactionManager) {
		this.hash = hash;
		this.transaction = transactionManager;
		this.mailSettings = settings;
		exemplarDao = daoFactory.getExemplarDao();
		orderDao = daoFactory.getOrderDao();
		userDao = daoFactory.getUserDao();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.AdminService#addUser(ua.nure.vydrya.
	 * SummaryTask4.repository.entity.User)
	 */
	@Override
	public User addUser(final User user) throws TransactionException {
		LOG.debug("addUser start");
		Operation<User> operation = new Operation<User>() {

			@Override
			public User execute() throws DaoException {
				LOG.debug("execute start");
				User resEmail = userDao.findUserByEmail(user.getEmail());
				User resPassport = userDao.findUserByPasportID(user.getPassportID());
				User userTmp = user;

				final boolean flagEmail = Objects.isNull(resEmail);
				final boolean flagPassport = Objects.isNull(resPassport);
				if (flagEmail && flagPassport) {
					String password = userTmp.getPassword();
					String hashPass;
					try {
						hashPass = hash.hash(user.getPassword());
					} catch (HashException e) {
						LOG.debug("Error hash", e);
						throw new ResourceException("Error hash", e);
					}
					user.setPassword(hashPass);
					userTmp = userDao.insert(user);
					userTmp.setPassword(password);
					if (Objects.isNull(userTmp.getId())) {
						userTmp = null;
					} else {
						MailSender sender = new MailSender(mailSettings.getSession(),
								user.getEmail(), MailMessage.ADD_NEW_USER_SYBJECT,
								MailSubject.addUserMessage(user));
						sender.execute();
					}
				} else {
					if (!flagEmail) {
						userTmp.setEmail(null);
					}
					if (!flagPassport) {
						userTmp.setPassportID(null);
					}
				}
				LOG.info("Result add user: " + userTmp);
				LOG.debug("execute done");
				return userTmp;
			}
		};
		LOG.debug("addUser done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.AdminService#blockUnblocUser(ua.nure.
	 * vydrya.SummaryTask4.web.bean.BlockUnblocBean)
	 */
	@Override
	public boolean blockUnblockUser(final BlockUnblockBean bean) throws TransactionException {
		LOG.debug("blockUnblocUser start");
		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws DaoException {
				LOG.debug("execute satrt");
				User user = userDao.findById(bean.getId());
				if (Objects.nonNull(user)) {
					if (bean.isDeleted()) {
						List<Order> orders = orderDao.findOrdersByUserId(user.getId());
						Exemplar exemplar;
						for (Order order : orders) {
							exemplar = exemplarDao.findById(order.getExemplarId());
							exemplar.setStatusExemplar(StatusExemplar.PRESENT);
							exemplarDao.update(exemplar);
							orderDao.deleted(order.getId(), true);
						}
					}
					LOG.debug("execute done");
					return userDao.deleted(bean.getId(), bean.isDeleted());
				}
				LOG.debug("execute done");
				return false;
			}
		};
		LOG.debug("blockUnblocUser done");
		return transaction.execute(operation);
	}
}
