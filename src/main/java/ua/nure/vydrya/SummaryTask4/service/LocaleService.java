package ua.nure.vydrya.SummaryTask4.service;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Set;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface LocaleService {
	Set<Locale> getAvailableLocales();

	Locale getDefaultLocale();

	boolean isSupported(String locale);

	Locale getMostSuitableLocale(Enumeration<Locale> locales);

	Integer getMaxAgeLocale();
}
