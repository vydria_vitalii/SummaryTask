package ua.nure.vydrya.SummaryTask4.service;

import java.util.List;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.web.bean.TestBean;
import ua.nure.vydrya.SummaryTask4.web.bean.UserLoginBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface UserService {
	User login(UserLoginBean userLoginBean) throws TransactionException;

	User registrations(User user) throws TransactionException;

	List<TestBean> testService() throws TransactionException;
}
