package ua.nure.vydrya.SummaryTask4.service.impl;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.service.LocaleService;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class LocaleServiceImpl implements LocaleService {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(LocaleServiceImpl.class);
	private Locale defaultLocale;
	private Set<Locale> availableLocales;
	private Integer maxAgeLocale;

	/**
	 * @param defaultLocale
	 * @param availableLocales
	 */
	public LocaleServiceImpl(Locale defaultLocale, Set<Locale> availableLocales,
			Integer maxAgeLocale) {
		this.defaultLocale = defaultLocale;
		this.availableLocales = availableLocales;
		this.maxAgeLocale = maxAgeLocale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.LocaleService#getAvailableLocales()
	 */
	@Override
	public Set<Locale> getAvailableLocales() {
		LOG.debug("getAvailableLocales start");
		LOG.debug("getAvailableLocales done");
		return availableLocales;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.LocaleService#getDefaultLocale()
	 */
	@Override
	public Locale getDefaultLocale() {
		LOG.debug("getDefaultLocale start");
		LOG.debug("getDefaultLocale done");
		return defaultLocale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.LocaleService#isSupported(java.lang.
	 * String)
	 */
	@Override
	public boolean isSupported(final String locale) {
		LOG.debug("isSupported start");
		boolean res = availableLocales.contains(new Locale(locale));
		LOG.info("isSupported locale - " + locale + " " + res);
		LOG.debug("isSupported done");
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.LocaleService#getMostSuitableLocale(
	 * java.util.Enumeration)
	 */
	@Override
	public Locale getMostSuitableLocale(Enumeration<Locale> locales) {
		LOG.debug("getMostSuitableLocale start");
		Locale locale;
		while (locales.hasMoreElements()) {
			locale = locales.nextElement();
			if (availableLocales.contains(locale)) {
				LOG.debug("getMostSuitableLocale done");
				return locale;
			}
		}
		LOG.debug("getMostSuitableLocale done");
		return defaultLocale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.LocaleService#getMaxAgeLocale()
	 */
	@Override
	public Integer getMaxAgeLocale() {
		return maxAgeLocale;
	}
}
