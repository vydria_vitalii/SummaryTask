/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.service;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderBean;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface OrderService {
	boolean cancellOrder(long id) throws TransactionException;

	boolean activationOrder(ActivationOrderBean bean) throws TransactionException;

	boolean openOrder(OpenOrderBean bean) throws TransactionException;

	boolean doneOrder(Long idOrder) throws TransactionException;

	void fineAccrual() throws TransactionException;
}
