package ua.nure.vydrya.SummaryTask4.service;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.web.bean.DTBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface DTService {
	// http://legacy.datatables.net/usage/server-side
	String getDataTableResponseUsers(DTBean table) throws TransactionException;

	String getDataTableResponseBooks(DTBean table) throws TransactionException;

	String getDataTableResponseExemplars(DTBean table) throws TransactionException;

	String getDataTableResponseOrdersByUser(DTBean table) throws TransactionException;

	String getDataTableResponseOrdersActivation(DTBean table) throws TransactionException;

	String getDataTableResponseAllArders(DTBean table) throws TransactionException;

}
