package ua.nure.vydrya.SummaryTask4.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.fine.FinePolicy;
import ua.nure.vydrya.SummaryTask4.repository.dao.DaoFactory;
import ua.nure.vydrya.SummaryTask4.repository.dao.ExemplarDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.OrderDao;
import ua.nure.vydrya.SummaryTask4.repository.dao.UserDao;
import ua.nure.vydrya.SummaryTask4.repository.entity.Exemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.Order;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusExemplar;
import ua.nure.vydrya.SummaryTask4.repository.entity.StatusOrder;
import ua.nure.vydrya.SummaryTask4.repository.entity.User;
import ua.nure.vydrya.SummaryTask4.repository.transaction.Operation;
import ua.nure.vydrya.SummaryTask4.repository.transaction.TransactionManager;
import ua.nure.vydrya.SummaryTask4.service.OrderService;
import ua.nure.vydrya.SummaryTask4.web.bean.ActivationOrderBean;
import ua.nure.vydrya.SummaryTask4.web.bean.OpenOrderBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class OrderServiceImpl implements OrderService {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(OrderServiceImpl.class);
	private TransactionManager transaction;

	private OrderDao orderDao;
	private UserDao userDao;
	private ExemplarDao exemplarDao;
	private FinePolicy finePolicy;

	public OrderServiceImpl(TransactionManager manager, DaoFactory factory, FinePolicy finePolicy) {
		transaction = manager;
		orderDao = factory.getOrderDao();
		exemplarDao = factory.getExemplarDao();
		userDao = factory.getUserDao();
		this.finePolicy = finePolicy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.OrderService#cancellOrder(long)
	 */
	@Override
	public boolean cancellOrder(final long id) throws TransactionException {
		LOG.debug("cancellOrder start");
		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws Exception {
				LOG.debug("execute satrt");
				boolean res = false;
				Order order = orderDao.findById(id);
				if (Objects.nonNull(order) && !order.isDeleted() && Objects
						.equals(order.getStatusOrder().value(), StatusOrder.OPEN.value())) {
					Exemplar exemplar = exemplarDao.findById(order.getExemplarId());
					if (!exemplar.isDeleted() && Objects.equals(
							exemplar.getStatusExemplar().value(), StatusExemplar.ORDER.value())) {
						exemplar.setStatusExemplar(StatusExemplar.PRESENT);
						res = exemplarDao.update(exemplar);
						if (res) {
							res = orderDao.deleted(id, true);
						}
					}
				}
				LOG.debug("execute done");
				return res;
			}
		};
		LOG.debug("cancellOrder done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.OrderService#activationOrder(ua.nure.
	 * vydrya.SummaryTask4.web.bean.ActivationOrderBean)
	 */
	@Override
	public boolean activationOrder(final ActivationOrderBean bean) throws TransactionException {
		LOG.debug("activationOrder start");
		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws Exception {
				LOG.debug("activationOrder start");
				Order order = orderDao.findById(bean.getIdOrder());
				if (Objects.nonNull(order) && !order.isDeleted() && Objects
						.equals(order.getStatusOrder().value(), StatusOrder.OPEN.value())) {
					Exemplar exemplar = exemplarDao.findById(order.getExemplarId());
					if (Objects.nonNull(exemplar) && !exemplar.isDeleted() && Objects.equals(
							exemplar.getStatusExemplar().value(), StatusExemplar.ORDER.value())) {
						order.setStatusOrder(StatusOrder.ACTIVE);
						order.setDateIn(bean.getDateIn());
						order.setDateOut(bean.getDateOut());
						orderDao.update(order);
						exemplar.setStatusExemplar(StatusExemplar.ISSUED);
						exemplarDao.update(exemplar);
						LOG.debug("activationOrder done");
						return true;
					}
				}
				LOG.debug("activationOrder done");
				return false;
			}
		};
		LOG.debug("activationOrder done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.OrderService#openOrder(ua.nure.vydrya
	 * .SummaryTask4.web.bean.OpenOrderBean)
	 */
	@Override
	public boolean openOrder(final OpenOrderBean bean) throws TransactionException {
		LOG.debug("openOrder start");
		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws Exception {
				LOG.debug("execute satrt");
				boolean flag = false;
				Exemplar exemplar = exemplarDao.findExemplarPresentByIdBooks(bean.getIdBook());
				if (Objects.nonNull(exemplar)) {
					User user = userDao.findById(bean.getIdUser());
					if (Objects.nonNull(user) && !user.isDeleted()) {
						exemplar.setStatusExemplar(StatusExemplar.ORDER);
						if (exemplarDao.update(exemplar)) {
							Order order = new Order();
							order.setExemplarId(exemplar.getId());
							order.setUserId(bean.getIdUser());
							orderDao.insert(order);
						}
						flag = true;
					}
				}
				LOG.debug("execute done");
				return flag;
			}
		};
		LOG.debug("openOrder done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ua.nure.vydrya.SummaryTask4.service.OrderService#doneOrder(java.lang.
	 * Long)
	 */
	@Override
	public boolean doneOrder(final Long idOrder) throws TransactionException {
		LOG.debug("doneOrder start");
		Operation<Boolean> operation = new Operation<Boolean>() {

			@Override
			public Boolean execute() throws Exception {
				LOG.debug("execute start");
				boolean res = false;
				Order order = orderDao.findById(idOrder);
				if (Objects.nonNull(order) && !order.isDeleted() && Objects
						.equals(order.getStatusOrder().value(), StatusOrder.ACTIVE.value())) {
					Exemplar exemplar = exemplarDao.findById(order.getExemplarId());
					if (Objects.nonNull(exemplar) && !exemplar.isDeleted() && Objects.equals(
							exemplar.getStatusExemplar().value(), StatusExemplar.ISSUED.value())) {
						exemplar.setStatusExemplar(StatusExemplar.PRESENT);
						if (exemplarDao.update(exemplar)) {
							order.setStatusOrder(StatusOrder.DONE);
							res = orderDao.update(order);
						}
					}
				}
				LOG.debug("execute done");
				return res;
			}
		};
		LOG.debug("doneOrder done");
		return transaction.execute(operation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.service.OrderService#fineAccrual()
	 */
	@Override
	public void fineAccrual() throws TransactionException {
		LOG.debug("fineAccrual start");
		Operation<Object> operation = new Operation<Object>() {

			@Override
			public Object execute() throws Exception {
				LOG.debug("execute start");
				Date currentDate = new Date();
				List<Order> orders = orderDao.findFineOrders();
				long diff;
				long diffDay;
				float fine;
				for (Order order : orders) {
					diff = Math.abs(currentDate.getTime() - order.getDateOut().getTime());
					diffDay = diff / (24 * 60 * 60 * 1000);
					fine = finePolicy.getDayFine() * diffDay;
					order.setFine(fine > finePolicy.getMaxFine() ? finePolicy.getMaxFine() : fine);
					orderDao.update(order);
					LOG.info("dif day: " + diffDay);
				}
				LOG.debug("execute done");
				return null;
			}
		};
		transaction.execute(operation);
		LOG.debug("fineAccrual done");
	}
}
