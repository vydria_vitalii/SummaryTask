/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.service;

import ua.nure.vydrya.SummaryTask4.exception.TransactionException;
import ua.nure.vydrya.SummaryTask4.repository.entity.Books;
import ua.nure.vydrya.SummaryTask4.web.bean.ExemplarBean;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface BooksService {
	void addBooks(Books books) throws TransactionException;

	boolean editBooksExemplar(final ExemplarBean exemplar) throws TransactionException;

	boolean deletedBooksExemplar(final Long id) throws TransactionException;
}