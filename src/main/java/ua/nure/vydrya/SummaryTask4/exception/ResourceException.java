package ua.nure.vydrya.SummaryTask4.exception;

/**
 * Implementation DaoException <br>
 * DaoException extends {@link Exception}.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class ResourceException extends RuntimeException {
	private static final long serialVersionUID = 1520214264969800268L;

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ResourceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ResourceException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ResourceException(Throwable cause) {
		super(cause);
	}

	/**
	 * Default constructor.
	 */
	public ResourceException() {
		super();
	}

}
