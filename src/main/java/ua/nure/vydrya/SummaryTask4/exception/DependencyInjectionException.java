package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DependencyInjectionException extends Exception {
	private static final long serialVersionUID = 3068792777728614769L;

	/**
	 * 
	 */
	public DependencyInjectionException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DependencyInjectionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DependencyInjectionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public DependencyInjectionException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DependencyInjectionException(Throwable cause) {
		super(cause);
	}

}
