/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class BooksAPIException extends Exception {
	private static final long serialVersionUID = 4041141010560927459L;

	/**
	 * 
	 */
	public BooksAPIException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public BooksAPIException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public BooksAPIException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public BooksAPIException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public BooksAPIException(Throwable cause) {
		super(cause);
	}

}
