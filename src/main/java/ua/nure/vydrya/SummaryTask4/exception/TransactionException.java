package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class TransactionException extends Exception {
	private static final long serialVersionUID = -3911258301527622160L;

	/**
	 * 
	 */
	public TransactionException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public TransactionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public TransactionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public TransactionException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public TransactionException(Throwable cause) {
		super(cause);
	}
}
