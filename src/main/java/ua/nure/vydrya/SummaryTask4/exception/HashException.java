package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class HashException extends Exception {
	private static final long serialVersionUID = 8006876960860461214L;

	/**
	 * 
	 */
	public HashException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public HashException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HashException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public HashException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public HashException(Throwable cause) {
		super(cause);
	}
}
