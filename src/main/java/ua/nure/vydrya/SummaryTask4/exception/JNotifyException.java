/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 *
 */
public class JNotifyException extends RuntimeException {
	private static final long serialVersionUID = 2117149466520527509L;

	/**
	 * 
	 */
	public JNotifyException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public JNotifyException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public JNotifyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public JNotifyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public JNotifyException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
