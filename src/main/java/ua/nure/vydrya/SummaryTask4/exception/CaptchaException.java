package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class CaptchaException extends Exception {
	private static final long serialVersionUID = -858535698955082850L;

	/**
	 * Default constructor.
	 */
	public CaptchaException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CaptchaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CaptchaException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public CaptchaException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CaptchaException(Throwable cause) {
		super(cause);
	}
}
