package ua.nure.vydrya.SummaryTask4.exception;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DTException extends Exception {
	private static final long serialVersionUID = 3538943279703439616L;

	public DTException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DTException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DTException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public DTException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DTException(Throwable cause) {
		super(cause);
	}
}
