package ua.nure.vydrya.SummaryTask4.exception;

/**
 * Exception DAO.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = -8701662673949824535L;

	/**
	 * Default constructor.
	 */
	public DaoException() {
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DaoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public DaoException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DaoException(Throwable cause) {
		super(cause);
	}

}
