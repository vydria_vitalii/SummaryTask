package ua.nure.vydrya.SummaryTask4.generator.password;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public interface PasswordGenerator {
	String generatePassword();
}
