package ua.nure.vydrya.SummaryTask4.generator.password.impl;

import java.security.SecureRandom;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import ua.nure.vydrya.SummaryTask4.generator.password.PasswordGenerator;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class PasswordGeneratorImpl implements PasswordGenerator {
	/** Logger. */
	private static final Logger LOG = Logger.getLogger(PasswordGenerator.class);
	private int passLen;

	public PasswordGeneratorImpl(int passLen) {
		this.passLen = passLen;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ua.nure.vydrya.SummaryTask4.generator.password.PasswordGenerator#
	 * generatePassword()
	 */
	@Override
	public String generatePassword() {
		LOG.debug("PasswordGenerator generatePassword starts");
		String generatedPassword = RandomStringUtils.random(passLen, 0, 0, true, true, null,
				new SecureRandom());
		LOG.info("Generated password:" + generatedPassword);
		LOG.debug("PasswordGenerator generatePassword finished");
		return generatedPassword;
	}
}
