package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class RegexConstants {

	/**
	 * Default constructor.
	 */
	private RegexConstants() {
		throw new UnsupportedOperationException("non instance RegexConstants");
	}

	// login
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static final String NUMBER_PATTERN = "^([1-9]([0-9]+))|([1-9])$";
	public static final String BOOLEAN = "true|false";

	// pattern date
	public static final String DATE_TIME_PATTERN = "^\\s*(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[0-2])-"
			+ "\\d\\d\\d\\d ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]\\s*$";
	public static final String DATE_PATTERN = "^\\s*(\\d\\d\\d\\d)-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])\\s*$";
	public static final String PASSPORT_PATTERN = "[А-Я]{2}\\s?[\\d]{6}";
}
