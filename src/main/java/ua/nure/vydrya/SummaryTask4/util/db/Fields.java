package ua.nure.vydrya.SummaryTask4.util.db;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class Fields {

	/**
	 * Default constructor.
	 */
	private Fields() {
		throw new UnsupportedOperationException("non instance Fields");
	}

	// user
	public static final String ID_USER = "id_user";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String LIBRARY_CARD = "library_card";
	public static final String NAME = "name";
	public static final String SURNAME = "surname";
	public static final String PATRONOMIC = "patronomic";
	public static final String PASSPORT_ID = "passport_ID";
	public static final String ADRESS = "adress";
	public static final String ROLE = "role";
	public static final String DELETED_USER = "deleted_user";

	// books
	public static final String ID_BOOKS = "id_book";
	public static final String TITLE = "title";
	public static final String SUBTITLE = "subtitle";
	public static final String DESCRIPTION = "description";
	public static final String PUBLICSHER = "publicsher";
	public static final String AUTHOR = "author";
	public static final String PUBLISHED_DATE = "published_date";
	public static final String IMAGE_LINK = "image_link";
	public static final String LANGUAGE = "language";
	public static final String PAGES = "pages";
	public static final String ISBN_10 = "isbn_10";
	public static final String ISBN_13 = "isbn_13";
	public static final String OTHER_IDENTIFIERS = "other_identifiers";
	public static final String DELETED_BOOK = "deleted_book";

	// exemplar
	public static final String ID_EXEMPLAR = "id_exemplar";
	public static final String BOOK_ID = "book_id";
	public static final String STATUS_EXEMPLAR = "status_exemplar";
	public static final String DELETED_EXEMPLAR = "deleted_exemplar";

	// order
	public static final String ID_ORDER = "id_order";
	public static final String USER_ID = "user_id";
	public static final String EXEMPLAR_ID = "exemplar_id";
	public static final String DATE_IN = "date_in";
	public static final String DATE_OUT = "date_out";
	public static final String FINE = "fine";
	public static final String EMPLOYEE_IN_ID = "employee_in_id";
	public static final String EMPLOYEE_OUT_ID = "employee_out_id";
	public static final String STATUS_ORDER = "status_order";
	public static final String DELETED_ORDER = "deleted_order";
}
