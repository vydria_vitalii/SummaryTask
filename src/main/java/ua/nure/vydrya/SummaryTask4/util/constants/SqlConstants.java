package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class SqlConstants {

	/**
	 * Default constructor.
	 */
	private SqlConstants() {
		throw new UnsupportedOperationException("non instance SqlConstants");
	}

	// user
	public static final String USER_SELECT = "user.select";
	public static final String USER_SELECT_BY_ID = "user.select.id";
	public static final String USER_SELECT_EMAIL = "user.select.email";
	public static final String USER_INSERT = "user.insert";
	public static final String USER_UPDATE = "user.update";
	public static final String USER_SELECT_PASSPORT_ID = "user.select.passport";
	public static final String USER_SELECT_MAX_LIBRARY_CARD = "user.select.max_library_card";
	public static final String USER_DELETED = "user.deleted";

	// books
	public static final String BOOKS_SELECK_BY_IDENTIFIERS = "books.select.identifiers";
	public static final String BOOKS_INSERT = "books.insert";
	public static final String BOOKS_SELECT_BY_ID = "books.select.id";

	// exemplar
	public static final String EXEMPLAR_SELECT_BY_ID = "exemplar.select.id";
	public static final String EXEMPLAR_INSERT = "exemplar.insert";
	public static final String EXEMPLAR_UPDATE = "exemplar.update";
	public static final String EXEMPLAR_SELECT_IS_DELETEB_BY_BOOKS_ID = "exemplar.select.is_deleted_by_books_id";
	public static final String EXEMPLAR_DELETED = "exemplar.deleted";
	public static final String EXEMPLAR_SELECT_PRESENR_BY_ID_BOOKS = "exemplar.select.id.present_books";

	// order
	public static final String ORDER_SELECT_BY_ID = "order.select.id";
	public static final String ORDER_INSERT = "order.insert";
	public static final String ORDER_UPDATE = "order.update";
	public static final String ORDER_DELETED = "order.deleted";
	public static final String ORDER_SELECT_FINE = "order.select.fine";
	public static final String ORDER_SELECT_FIND_BU_USER_ID = "order.select.user_id";

	public static final String USER_TEST = "user_test";
	public static final String ORDER_TEST = "order_test";
}