package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class JobConstants {

	private JobConstants() {
		throw new UnsupportedOperationException("non instance JobConstants");
	}

	public static final String NAME_JOB = "FineJob";
	public static final String NAME_TRIGGER = "FineTigger";
	public static final String GROUP = "Group";

	public static final String ORDER_SERVICE = "orderService";
	public static final String REPEAT_COUNT = "repeatCount";
	public static final String TIME_WAIT = "timeWait";
}
