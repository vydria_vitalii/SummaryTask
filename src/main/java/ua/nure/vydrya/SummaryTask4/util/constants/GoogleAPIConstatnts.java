package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class GoogleAPIConstatnts {

	/**
	 * Default constructor.
	 */
	private GoogleAPIConstatnts() {
		throw new UnsupportedOperationException("non instance GoogleAPIConstatnts");
	}

	public static final String VOLUME_INFO = "volumeInfo";
	public static final String ITEMS = "items";
	public static final String TITLE = "title";
	public static final String SUBTITLE = "subtitle";
	public static final String AUTHORS = "authors";
	public static final String PUBLISHER = "publisher";
	public static final String PUBLISHED_DATE = "publishedDate";
	public static final String DESCRIPTION = "description";

	public static final String INDUSTRY_IDENTIFIERS = "industryIdentifiers";
	public static final String TYPE = "type";
	public static final String ISBN_13 = "ISBN_13";
	public static final String ISBN_10 = "ISBN_10";
	public static final String OTHER = "OTHER";
	public static final String INDENTIFIER = "identifier";
	public static final String PAGE_COUNT = "pageCount";
	public static final String LANGUAGE = "language";
	public static final String IMAGE_LINKS = "imageLinks";
	public static final String ID = "id";
	public static final String SMALL_THUMBNAIL = "smallThumbnail";
	public static final String TOTAL_ITEMS = "totalItems";
	public static final String ERROR = "error";
}
