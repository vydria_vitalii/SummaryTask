/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class FileConstants {

	/**
	 * 
	 */
	private FileConstants() {
		throw new UnsupportedOperationException();
	}

	public static final String FILE_SCHEMA = "/resources/json/schema.json";

}
