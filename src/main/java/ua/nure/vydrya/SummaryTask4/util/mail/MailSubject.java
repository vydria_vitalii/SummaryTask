/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.util.mail;

import ua.nure.vydrya.SummaryTask4.repository.entity.User;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class MailSubject {

	/**
	 * Default constructor.
	 */
	private MailSubject() {
		throw new UnsupportedOperationException("non innstance MailSubject");
	}

	public static String addUserMessage(User user) {
		StringBuilder message = new StringBuilder();
		message.append(MailMessage.MESSAGE_TREATMENT);
		message.append(user.getName());
		message.append(MailMessage.MESSAGE_ANNOUNCEMENT);
		message.append(System.lineSeparator());
		message.append(MailMessage.MESSAGE_CREDENTIAL_TEXT);
		message.append(System.lineSeparator());
		message.append(MailMessage.MESSAGE_LOGIN).append(user.getEmail());
		message.append(System.lineSeparator());
		message.append(MailMessage.MESSAGE_PASSWORD).append(user.getPassword());
		message.append(System.lineSeparator());
		message.append(MailMessage.MESSAGE_SIGN);
		return message.toString();
	}
}
