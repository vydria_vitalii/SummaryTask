package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class DependencyInjectionConstants {

	/**
	 * Default constructor.
	 */
	private DependencyInjectionConstants() {
		throw new UnsupportedOperationException("non instance DependencyInjectionConstants");
	}

	public static final String DEPENDENCY_INJECTION = "DependencyInjection";
	public static final String LOCALE_STORAGE = "LocaleStorage";
	public static final String LOCALE_SERVICE = "LocaleService";
	public static final String LOGIN_USER_BEAN_VALIDATOR = "LoginUserBeanValidator";
	public static final String USER_LOGIN_BEAN_EXTRACTOR = "UserLoginBeanExtractor";
	public static final String USER_SERVICE = "UserService";
	public static final String CAPTCHA = "Captcha";

	//
	public static final String REGISTARATIONS_USER_EXTRACTOR = "RegistrationsUserExtractor";
	public static final String ADD_USER_VALIDATOR = "AddUserBeanValidator";
	public static final String ADD_USER_BEAN_TO_USER_CONVERTER = "AddUserBeanToUserConverter";

	//
	public static final String DT_SERVICE = "DTService";
	public static final String DT_EXTRACTOR = "DTExtractor";

	//
	public static final String ADMIN_SERVICE = "AdminService";
	public static final String ADD_EMPLOYEE_EXTRACTOR = "AddEmployeeExtractor";
	public static final String BLOCK_UNBLOCK_PARAM_EXTRATOR = "BlockUnblocParamExtractor";
	public static final String BLOCK_UNBLOCK_USER_VALIDATOR = "BlockUnblockUserValidator";
	public static final String BLOCK_UNBLOCK_PARAM_BEAN_TOBLOCK_UNBLOCK_BEAN_CONVERTER = "BlockUnblocParamBeanToBlockUnblocBeanConverter";

	//
	public static final String ADD_BOOKS_EXTRACTOR = "AddBooksExtractor";
	public static final String BOOKS_SERVICE = "BooksService";
	public static final String ADD_BOOKS_VALIDATOR = "AddBooksValidator";
	public static final String EDIT_BOOKS_EXTRACTOR = "EditBooksExtractor";
	public static final String PARAM_ID_EXTRACTOR = "ParamIdExtractor";
	public static final String PARAM_ID_VALIDATOR = "ParamIdValidator";

	//
	public static final String ORDER_SERVICE = "OrderService";
	public static final String OPEN_ORDER_PARAM_BEAN_EXTARCTOR = "OpenOrderParamBeanExtractor";
	public static final String OPEN_ORDER_PARAM_BEAN_VALIDATOR = "OpenOrderParamBeanValidator";
	public static final String OPEN_ORDER_PARAM_BEAN_TO_OPEN_ORDER_BEAN_CONVERTOR = "OpenOrderParamBeanToOpenOrderBeanConverter";
	public static final String CANCEL_ORDER_EXTRACTOR = "CancelOrderExtractor";
	public static final String ACTIVATION_ORDER_PARAM_BEAN_TO_ACTIVATION_ORDER_BEAN_CONVERTER = "ActivationOrderParamBeanToActivationOrderBeanConverter";
	public static final String ACTIVATION_ORDER_PARAM_BEAN_VALIDATOR = "ActivationOrderParamBeanValidator";
	public static final String ACTIVATION_ORDER_PARAM_BEAN_EXTRACTOR = "ActivationOrderParamBeanExtractor";
	public static final String DONE_ORDER_EXTARCTOR = "DoneOrderExtractor";
	public static final String REMOVE_BOOKS_EXTRACTOR = "RemoveBooksExtractor";

	//
	public static final String FINE_POLICY = "FinePolicy";
	public static final String LIST_BLOCK_USERS = "ListBlockUsers";
}
