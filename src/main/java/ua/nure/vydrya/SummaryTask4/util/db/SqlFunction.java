package ua.nure.vydrya.SummaryTask4.util.db;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class SqlFunction {
	private SqlFunction() {
		throw new UnsupportedOperationException("non instarnce SqlFunction");
	}

	public static String count(String param) {
		return "COUNT(" + param + ")";
	}

	public static String min(String param) {
		return "MIN(" + param + ")";
	}

	public static String max(String param) {
		return "MAX(" + param + ")";
	}

	public String avg(String param) {
		return "AVG(" + param + ")";
	}

	public static String like(String param) {
		return "LIKE " + param;
	}
}
