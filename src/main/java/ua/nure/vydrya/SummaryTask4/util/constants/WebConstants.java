package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class WebConstants {

	/**
	 * Default constants.
	 */
	private WebConstants() {
		throw new UnsupportedOperationException("non instance WebConstants");
	}

	public static final String ID = "id";
	public static final String DELETED = "deleted";
	// locale
	public static final String LOCALE = "Locale";
	public static final String INTERVAL_INVALID_LOCALE = "IntervalInvalidLocale";
	public static final String LANG = "lang";

	// login
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String PARAM_RECAPTCHA = "g-recaptcha-response";

	// user
	public static final String USER_ID = "UserId";
	public static final String USER = "user";
	public static final String ROLE = "role";

	// form bean
	public static final String FORM_BEAN = "form_bean";

	// reg
	public static final String PASSWORD_1 = "password1";
	public static final String PASSWORD_2 = "password2";
	public static final String NAME = "nameUser";
	public static final String SURNAME = "surname";
	public static final String PATRONOMIC = "patronomic";
	public static final String PASSPOTR_ID = "passportID";
	public static final String ADRESS = "adress";

	// book
	public static final String BOOK_ID = "bookId";

	// date format
	public static final String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm";
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	public static final String DATE_OUT = "dateOut";
}
