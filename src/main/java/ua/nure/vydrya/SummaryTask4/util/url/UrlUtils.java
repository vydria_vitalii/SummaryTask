/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.util.url;

/**
 * Implementation UrlUtils.
 * 
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class UrlUtils {
	private static final String PATTERN_COMMAND_HEAD = "(.*)[//]";
	private static final String PATTERN_COMMAND_TAIL = "[?](.*)";

	/**
	 * Default constructor.
	 */
	private UrlUtils() {
		throw new UnsupportedOperationException("non instance UrlUtils");
	}

	/**
	 * Convert url to command.
	 * 
	 * @param url
	 *            web url.
	 * @return the string command.
	 */
	public static String convertUrlToCommand(final String url) {
		return url.replaceAll(PATTERN_COMMAND_HEAD, "").replace(PATTERN_COMMAND_TAIL, "");
	}
}
