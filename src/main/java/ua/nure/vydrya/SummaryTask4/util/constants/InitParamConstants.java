package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class InitParamConstants {

	/**
	 * Default constructor.
	 */
	private InitParamConstants() {
		throw new UnsupportedOperationException();
	}

	// encoding
	public static final String CHARACTER_ENCODING = "CharacterEncoding";
	public static final String CONTENT_TYPE = "ContentType";

	// context init
	public static final String DB = "DB";
	public static final String DATA_SOURCE_NAME = "DataSourceName";
	public static final String LOCALE_STORAGE = "LocaleStorage";
	public static final String INTERVAL_INVALID_LOCALE_STORAGE = "IntervalInvalidLocale";
	public static final String AVAILABLE_LOCALES = "AvailableLocales";
	public static final String DEFAULT_LOCALE = "DefaultLocale";
	public static final String CAPTCHA = "Captcha";
	public static final String SECRET_KEY = "SecretKey";
	public static final String MAIL = "mail";
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String MIN_DT = "Min DT";
	public static final String MAX_DT = "Max DT";
	public static final String DT = "DT";
	public static final String LENGTH_PASSWORD = "Length password";

	// ResourceBundle
	public static final String RESOURCE_BUNDLE = "resource";

	// role
	public static final String COMMONS = "commons";

	// google books api
	public static final String GOOGLE_BOOKS_API_KEY = "Google_Books_API_Key";

	public static final String PATH_FINE_FILE = "PathFineFile";
	public static final String REPEAT_COUNT = "RepeatCount";
	public static final String TIME_WAIT = "TimeWait";
	public static final String CRON_SCHEDULE = "CronSchedule";
}
