/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.util.date;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;

/**
 * @author vydrya_vitaliy.
 *
 */
public final class DateUtils {
	private static final int YEAR_LEN = 4;

	/**
	 * Default constructor.
	 */
	private DateUtils() {
		throw new UnsupportedOperationException("non instance DateUtils");
	}

	public static Date yearToDate(final Year year) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		java.util.Date parsed = null;
		try {
			parsed = format.parse(year.toString());
			return new Date(parsed.getTime());
		} catch (ParseException e) {
			throw new RuntimeException("Error convert yearToDate");
		}
	}

	public static String dateToYear(final String date) {
		String year = date;
		if (year.length() > YEAR_LEN) {
			year = year.substring(0, YEAR_LEN);
		}
		return year;
	}

}