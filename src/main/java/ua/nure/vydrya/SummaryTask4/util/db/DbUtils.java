package ua.nure.vydrya.SummaryTask4.util.db;

import java.util.Objects;

import org.apache.log4j.Logger;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class DbUtils {

	private static final Logger LOG = Logger.getLogger(DbUtils.class);

	/**
	 * Default constructor.
	 */
	private DbUtils() {
		throw new UnsupportedOperationException("non instance DbUtils");
	}

	@SafeVarargs
	public static <E extends AutoCloseable> void closingResources(final E... resources) {
		LOG.debug("DbUtils closingResources starts");
		for (E res : resources) {
			LOG.info("close resurss: " + res);
			closeQuietly(res);
		}
		LOG.debug("DbUtils closingResources finishedts");
	}

	public static <E extends AutoCloseable> void closeQuietly(final E quietly) {
		LOG.debug("DbUtils closeQuietly starts");
		try {
			if (Objects.nonNull(quietly)) {
				quietly.close();
			}
		} catch (Exception e) {
			LOG.warn("Error closeQuietly", e);
		}
		LOG.debug("DbUtils closeQuietly finishedts");
	}
}