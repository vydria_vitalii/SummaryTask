package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class Pages {

	/**
	 * Default constants.
	 */
	private Pages() {
		throw new UnsupportedOperationException("non instance Pages");
	}

	// pages
	public static final String INDEX = "index.jsp";

	// url
	public static final String LOGOUT = "logout";
	public static final String LOGIN = "login";
	public static final String REGISTRATIONS = "registrations";
	public static final String LIST_USERS = "listUsers";
	public static final String ADD_EMPLOYEE = "addEmployee";
	public static final String ADD_BOOKS = "addBooks";
	public static final String EDIT_BOOKS = "editBooks";
	public static final String LIST_EXEMPLARS = "listExemplars";
	public static final String LIST_ORDERS = "listOrders";
	public static final String ACTIVATION_ORDER = "activationOrder";
	public static final String LIST_ORDERS_ACTIVATION = "listOrdersActivation";
	public static final String LIST_ALL_ORDERS = "listAllOrders";
	public static final String ERROR_404 = "/WEB-INF/error/error_404.jsp";
	public static final String LOG_OUT = "logout";
}