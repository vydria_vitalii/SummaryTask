package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class ErrorConstants {

	/**
	 * Default constructor.
	 */
	private ErrorConstants() {
		throw new UnsupportedOperationException("non instance ErrorConstants");
	}

	public static final int ERROR_500 = 500;
	public static final int ERROR_404 = 404;

	public static final String SUCCESS = "succes";
	public static final String ERROR = "error";

	// login error
	public static final String ERROR_EMAIL = "error_email";
	public static final String ERROR_PASSWORD = "error_password";
	public static final String ERROR_NOT_USER = "error_not_user";
	public static final String ERROR_CAPCHA = "error_capcha";
	// error_key login
	public static final String ERROR_EMAIL_KEY = "error.login.email";
	public static final String ERROR_PASSWORD_KEY = "error.login.password";
	public static final String ERROR_NOT_USER_KEY = "error.login.not_user";
	public static final String ERROR_CAPCHA_KEY = "captcha.invalid";

	// registrations error
	public static final String ERROR_REGISTARTIONS = "error_registaration";
	public static final String ERROR_REGISTARTIONS_EMAIL = "errror_reg_email";
	public static final String ERROR_REGISTARTIONS_PASSPORT = "error_reg_passpotr";
	public static final String ERROR_PASSPORT_INCORRECT = "error_passport_incorrect";
	public static final String ERROR_REG_FORM = "error_registaration";
	// error_key registrations
	public static final String ERROR_REGISTARTIONS_KEY = "error.registaration";
	public static final String ERROR_REGISTARTIONS_EMAIL_KEY = "error.registaration.email";
	public static final String ERROR_REGISTARTIONS_PASSPORT_KEY = "error.registaration.passport";
	public static final String ERROR_PASSPORT_INCORRECT_KEY = "error.registaration.passport.incorrect";
	public static final String ERROR_REG_FORM_KEY = "error.registaration";

	// add employee
	public static final String ERROR_ADD_EMPLOYEE = "error_add_employee";
	// error_key registrations
	public static final String ERROR_ADD_EMPLOYEE_KEY = "error.add.employee";

	// delete user
	public static final String ERROR_USER_DELETED = "error_deleted_user";
	// key
	public static final String ERROR_USER_DELETED_KEY = "error.deleted.user";

	// books
	public static final String ERROR_BOOKS = "error_books";
	// key
	public static final String ERROR_ADD_BOOKS_KEY = "error.add.books";

	// order
	public static final String ERROR_OPEN_ORDER_BOOK_ID = "error_open_order_book_id";
	public static final String ERROR_OPEN_ORDER_USER_ID = "error_open_order_user_id";
	public static final String ERROR_ORDER_DATE_OUT = "error_order_date_out";
	// key
	public static final String EROR_OPEN_ORDER_BOOK_ID_KEY = "error.open.order.book_id";
	public static final String ERROR_OPEN_ORDER_USER_ID_KEY = "error.open.order.user_id";
	public static final String ERROR_ORDER_DATE_OUT_KEY = "error.order.date_out";
}
