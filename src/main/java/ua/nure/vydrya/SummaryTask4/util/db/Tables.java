/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.util.db;

/**
 * @author vydrya_vitaliy.
 *
 */
public final class Tables {

	/**
	 * 
	 */
	private Tables() {
		throw new UnsupportedOperationException("non instance Tables");
	}

	public static final String USER = "user";
	public static final String BOOKS = "books";
	public static final String EXEMPLAR = "exemplar";
	public static final String ORDER = "`order`";
}
