package ua.nure.vydrya.SummaryTask4.util.json;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class JsonUtils {
	private JsonUtils() {
		throw new UnsupportedOperationException("non instnce JsonUtils");
	}

	public static JsonObject jsonFromString(String jsonObjectStr) {
		try (JsonReader jsonReader = Json.createReader(new StringReader(jsonObjectStr))) {
			return jsonReader.readObject();
		}
	}

	public static String getStringJsonByUrl(String url, int timeout, String method)
			throws JsonException {
		HttpURLConnection c = null;
		try {
			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod(method);
			c.setRequestProperty("User-Agent", "Mozilla/5.0");
			c.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			c.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.setConnectTimeout(timeout);
			c.setReadTimeout(timeout);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:
				StringBuilder sb = new StringBuilder();
				try (BufferedReader br = new BufferedReader(
						new InputStreamReader(c.getInputStream(), "UTF-8"))) {
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line + System.lineSeparator());
					}
				}
				return sb.toString();
			default:
				return null;
			}

		} catch (Exception ex) {
			Logger.getLogger(JsonUtils.class.getName()).log(Level.SEVERE,
					"Error parse json from in url", ex);
			throw new JsonException("Error parse json from in url", ex);
		} finally {
			if (Objects.nonNull(c)) {
				try {
					c.disconnect();
				} catch (Exception ex) {
					Logger.getLogger(JsonUtils.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}
}
