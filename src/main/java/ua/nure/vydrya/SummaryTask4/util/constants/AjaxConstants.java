package ua.nure.vydrya.SummaryTask4.util.constants;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public class AjaxConstants {

	/**
	 * Default constructor.
	 */
	private AjaxConstants() {
		throw new UnsupportedOperationException();
	}

	public static final String CONTENT_TYPE = "application/json";
	public static final String CHARACTER_ENCODING = "utf-8";

	// DT
	public static final String COLUMNS = "iColumns"; // количество столобцов
	public static final String DISPLAY_START = "iDisplayStart"; // First record
																// that should
																// be shown
																// (used for
																// pagination).
	public static final String SORTING_COLS = "iSortingCols"; // количество
																// заказов
																// сортировок.
	public static final String DISPLAY_LENGTH = "iDisplayLength";
	public static final String ECHO = "sEcho";
	public static final String GLOBAL_SEARCH = "sSearch";
	public static final String SORTING_COL = "iSortCol_";
	public static final String SORD_DIR = "sSortDir_";
	public static final String SEARCH_COL = "sSearch_";

	public static final String TOTAL_RECORDS = "iTotalRecords";
	public static final String DATE = "aaData";
	public static final String TOTAL_DISPLAY_RECORDS = "iTotalDisplayRecords";
}
