/**
 * 
 */
package ua.nure.vydrya.SummaryTask4.util.mail;

/**
 * @author vydrya_vitaliy.
 * @version 1.0.
 */
public final class MailMessage {

	/**
	 * Default constructor.
	 */
	private MailMessage() {
		throw new UnsupportedOperationException("non instance MailConstants");
	}

	public static final String ADD_NEW_USER_SYBJECT = "New user registration.";

	public static final String MESSAGE_TREATMENT = "Dear, ";

	public static final String MESSAGE_ANNOUNCEMENT = ", You have been registered to the Library system.";

	public static final String MESSAGE_CREDENTIAL_TEXT = "Your authentication credentials are: ";

	public static final String MESSAGE_LOGIN = "Login: ";

	public static final String MESSAGE_PASSWORD = "Password: ";

	public static final String MESSAGE_SIGN = "Sincerely yours, admin. ";

}
