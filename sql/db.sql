CREATE DATABASE  IF NOT EXISTS `db_library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_library`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: db_library
-- ------------------------------------------------------
-- Server version	5.5.48-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id_book` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `subtitle` text,
  `description` text,
  `publicsher` text,
  `author` text,
  `published_date` varchar(11) DEFAULT NULL,
  `image_link` text,
  `language` varchar(30) NOT NULL,
  `pages` int(11) DEFAULT NULL,
  `isbn_10` varchar(10) DEFAULT NULL,
  `isbn_13` varchar(13) DEFAULT NULL,
  `other_identifiers` varchar(255) DEFAULT NULL,
  `deleted_book` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_book`),
  UNIQUE KEY `id_book_UNIQUE` (`id_book`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (5,'SDFS-A Design and Architecture of Highly Scalable Distributed File System for Linux Cluster',NULL,NULL,'','O-larn Suriyabhumi','2001','http://books.google.com/books/content?id=1M0aAAAAYAAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api','en',90,'9744618922','9789744618924',NULL,0),(6,'Освой самостоятельно С++за 21 день','[пер. с англ.]',NULL,'Издательский дом Вильямс','\"Джесс Либерти\"','2008','http://books.google.com/books/content?id=42F8RHC4ZqIC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73iiWuVbD3gCWfSjajxe8PBYSwALgq286HGBA5B6nL8u-ZQR2li8UO3cCelx58LtzipWe5X8NtyQB9jvpSDI6JLWU5cMpZAwRdXkBgrWUMRfOgTgbcBk-sL6P0yfiuzwHuglYSU&source=gbs_api','ru',767,'5845909260','9785845909268',NULL,0),(10,'Освой самостоятельно С++за 21 день','[пер. с англ.]',NULL,'Издательский дом Вильямс','\"Джесс Либерти\"','2008','http://books.google.com/books/content?id=42F8RHC4ZqIC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE72FXGHhJKr0q1yzW4KJUe_h5pyWcYuDhfGnC4mAtiGBoBWphzXtpikGS5VleRCKNrz_QUQI4NIkKetckKefEOpBfzOJCwmLGZqXApbbJ0sSP2EAYRfd5qMUGPfXSehkhDO5qRAS&source=gbs_api','ru',767,'5845909260','9785845909268',NULL,0),(11,'Язык программирования C, 2-е издание',NULL,'Книга является как практически исчерпывающим справочником, так и учебным пособием по самому распространенному языку программирования. Адресована для чтения новичкам: для своего изучения она требует знания основ программирования и вычислительной техники','Издательский дом Вильямс','\"Брайан В. Керниган\"','2012','http://books.google.com/books/content?id=cqxkc9IaozYC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70Sc3d5sHIw_4cBC3d56RPoPGovaCFky19wpyfDfS4PkBaXBXzi42JKz_eZwtXVoORAvVtqpMporY9YtISoawjITuCr03CpX66GDg1d701gePrmk_zhgqcGTY3404VgAucuLD6Q&source=gbs_api','ru',289,'5845908914','9785845908919',NULL,0),(12,'Язык программирования C. Лекции и упражнения, 5-е издание',NULL,NULL,'Издательский дом Вильямс',NULL,NULL,'http://books.google.com/books/content?id=SOLy5EmvkzsC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73McJS_0PozmqbX021HfcRfUgLPOn044KBf8DPBQYOLgJNABUH-x_c5-FxTWXEI3_jGOVlh-qdHgmNV3uECCIPkB-VjdtvnrMPrlzK8s5nhfMQYrxOMvSong6FDmIQsiXbWBH5D&source=gbs_api','ru',962,'5845909864','9785845909862',NULL,0),(13,'Язык программирования C. Лекции и упражнения, 5-е издание',NULL,NULL,'Издательский дом Вильямс',NULL,NULL,'http://books.google.com/books/content?id=SOLy5EmvkzsC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71Lq8JYDP9-L_l9C2W_uPlgUGvJmJ1itOZEsguFPcebObiHhlzFaCTPI2vRB5n3TOaes2N6jWLx06hPAex0PwzsVPEOeOmKfpEfv7t31YGllOcXiiZFrtNZiSJdito8zsCv4aer&source=gbs_api','ru',962,'5845909864','9785845909862',NULL,0),(14,'Справочник программиста по C/C++, 3-е издание',NULL,NULL,'Издательский дом Вильямс',NULL,NULL,'http://books.google.com/books/content?id=3t75kJd3UtsC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE72zsgYWI2f_cfBa9GUxG_dKUM7MI4aOw6VU5EfZXNfwLl3B0uPI2JiPm-yfbuwvV3WtKar7Rc2ipTcopqZNoSXZsx4ddKoP25WvzqwF4jhDT7B4u8SwZieG3T0MvsWrT-M1pw8a&source=gbs_api','ru',430,'5845910773','9785845910776',NULL,0),(15,'Пушкин в зеркале мифов',NULL,NULL,'Языки славянской культуры,','\"Виктор Есипов\"','2006','http://books.google.com/books/content?id=01kYAQAAIAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70ZCchFUu4RnlWb6xz7Ko96geO1BqVvO3-0OHt2sz2Judc-P6us-2BHFLNqPeXAtAAg7UU1vymx6afvWtW79Tuqi8DQXAPCdQGJCOhatbTRrR_Li7H_nPgEzdN3iPF9_RXWo4ry&source=gbs_api','ru',558,'595510125X','9785955101255',NULL,0),(16,'Medieval European Pilgrimage, C.700-c.1500',NULL,'This book introduces the reader to the history of European Christian pilgrimage in the twelve hundred years between the conversion of the Emperor Constantine and the beginnings of the Protestant Reformation. It sheds light on the varied reasons for which men and women of all classes undertook journeys, which might be long (to Rome, Jerusalem and Compostela) or short (to innumerable local shrines). It also considers the geography of pilgrimage and its cultural legacy. <br>','Palgrave','\"Diana Webb\"','2002','http://books.google.com/books/content?id=lOsGmQEACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE72Ggv7vE8bf7nVfqLhE-vq0limAs_FcUZ43MdUmbCnNG7x3I9f5SSvatlvDsPavGXzVUawPdmwv0u5CC2Mqpiz-LSEuKehCS0_JMPGCRb6kBTZOCYgTwkgplsco46gxP_D2iD2m&source=gbs_api','en',201,'0333762592','9780333762592',NULL,0),(17,'The Hidden Treasures Of C',NULL,'Written In An Extremely Clear And Interesting Manner, This Book Reveals The Sec5Tet Features, Tricks And Techniques Of C Designed To Make You An Expert C Programmer. The Book Is Divided Into Two Parts. Part I Focuses On The Basic Syntax Of C And Explains Them In Depth. Part Ii Is Devoted To Programming In C. This Part Reveals The Inner Dynamics Of C Programming Through 200 Carefully Designed Solved Examples. These Examples Illustrate The Various Ways Of Using C Language Features To Solve A Given Problem And Also Provide The Secrets, Tricks And Techniques For An Efficient And Elegant Programming Style. Possible Errors And Solutions For Them Have Also Been Highlighted In These Examples. All Example Programs Are Accompanied By Their Outputs. With All These Features This Book Is An Invaluable Asset For Students Wishing To Master C As Well As For Experienced Programmers Wishing To Develop Powerful Programs.','BPB Publications','\"Dharaskar\"','2004','http://books.google.com/books/content?id=Ly9CPQAACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE73myO1wdr52kAUR6mb5SvIIN8hE0380zcGYRWopDGaCXHzmamj9RqZvKGc8LLuu-MBueg3JaPVr0Gta-9e_a9nNCgnkOX-MV-8RTx_AXGl7VVime5xjOvAyHqhKurjG0ywgbHDm&source=gbs_api','en',327,'8170295505','9788170295501',NULL,0),(18,'Italian Reform and English Reformations, C.1535-c.1585',NULL,'This book is the first full-scale study of interactions between Italy\'s religious reform and English reformations, which were notoriously liable to pick up other people\'s ideas and run. Anne Overell adopts an inclusive approach, retaining within the group of Italian reformers those spirituali who left the church and those who remained within it, and exploring commitment to reform, whether \'humanist\', \'protestant\' or \'catholic\'. The book is of fundamental importance for those whose work includes revisionist themes of ambiguity, opportunism and interdependence in sixteenth century religious change.','Ashgate Publishing, Ltd.','\"Anne Overell\"','2008','http://books.google.com/books/content?id=4YylaXlHodAC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71J6U7Kri7V8aafcT1Rw9M8yNuih9vEs8HRNP6oQxgnJk6m_OadXeD8Zb_8N8C3wkystiFv0u9eSCtVy7BZstrsuWYHvVj1oROykeG-wByEKEx3T6CTF-6H4v4uje6JGxeN5M2o&source=gbs_api','en',264,'0754692205','9780754692201',NULL,0),(19,'Data Structures With C (Sie) (Sos)',NULL,NULL,'McGraw-Hill Education (India) Pvt Limited','\"Lipschutz\"','2011','http://books.google.com/books/content?id=YJQIOLgFnnYC&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE73NpeF2eDZfdpzcQ1JPHzwUsiS3LVN5KigOb1MEhgpT3hhA-c5nhhB5Brg8OO9uD2-IVmCMcG2C45EEJFQjqeMClWQnEKsafD2pc4nJoyH6a_HjfAISEuamYCnkt06F3eFBn4vk&source=gbs_api','en',13,'0070701989','9780070701984',NULL,0),(20,'Italian Reform and English Reformations, c.1535–c.1585',NULL,'This is the first full-scale study of interactions between Italy\'s religious reform and English reformations, which were notoriously liable to pick up other people\'s ideas. The book is of fundamental importance for those whose work includes revisionist themes of ambiguity, opportunism and interdependence in sixteenth century religious change. Anne Overell adopts an inclusive approach, retaining within the group of Italian reformers those spirituali who left the church and those who remained within it, and exploring commitment to reform, whether \'humanist\', \'protestant\' or \'catholic\'. In 1547, when the internationalist Archbishop Thomas Cranmer invited foreigners to foster a bolder reformation, the Italians Peter Martyr Vermigli and Bernardino Ochino were the first to arrive in England. The generosity with which they were received caused comment all over Europe: handsome travel expenses, prestigious jobs, congregations which included the great and the good. This was an entry con brio, but the book also casts new light on our understanding of Marian reformation, led by Cardinal Reginald Pole, English by birth but once prominent among Italy\'s spirituali. When Pole arrived to take his native country back to papal allegiance, he brought with him like-minded men and Italian reform continued to be woven into English history. As the tables turned again at the accession of Elizabeth I, there was further clamour to \'bring back Italians\'. Yet Elizabethans had grown cautious and the book\'s later chapters analyse the reasons why, offering scholars a new perspective on tensions between national and international reformations. Exploring a nexus of contacts in England and in Italy, Anne Overell presents an intriguing connection, sealed by the sufferings of exile and always tempered by political constraints. Here, for the first time, Italian reform is shown as an enduring part of the Elect Nation\'s literature and myth.','Routledge','\"M. Anne Overell\"','2016','http://books.google.com/books/content?id=_UUfDAAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70njoQD02ID_cXsHKRqblBGmzXmq_3dvSxqsBPB2aoaohaYz5z5ed4Py0G4ftlPmdQSyoDCybb9atj3eqgmYZVbtV6eFdAiYMEhbYnkixklmG18xlPVSbNQlTPBuTsYm29TNvHI&source=gbs_api','en',264,'1317111699','9781317111696',NULL,0),(21,'Сказки','для детей от 7 до 10 лет','\"Самоделки от Мурзилки\" — это коллекция лучших моделей для сборки. Каждая книжка —это несколько часов увлекательного времяпрепровождения для Ваших детей и игрушка, сделанная своими руками! Для занятия родителей с детьми младшего школьного возраста.','Рипол Классик','\"Коваленко М.\"','2011','http://books.google.com/books/content?id=ETUo9LjPvHwC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70ZDh0NhP_qnZnCT4CaMQJZuoHuc3xS6sdEOtXIVQFfzcvJV4mOgIFUeJU5u5hZo_H5uuR7plJ1zNLuRYhXaf4NlFtb5ROwo5HRD569NZHNSZandf5xEBvpCzdMVd0VBZY_Aa3i&source=gbs_api','ru',17,'5386039539','9785386039530',NULL,0),(24,'С.С.С. Скрытые сексуальные сигналы',NULL,'Свершилось! Лейл Лаундес, автор бестселлеров «Как влюбить в себя любого» и «Как говорить с кем угодно и о чем угодно», наконец-то написала книгу специально для мужчин, книгу, в которой она раскрывает все женские хитрости и уловки. Эта книга – только для мужчин. Строго конфиденциально.Если женщина хочет вас, она сама расскажет вам об этом, не проронив ни единого слова – просто подавая вам скрытые сексуальные сигналы (С.С.С.), и, если вы правильно отреагируете на эти сигналы, вы никогда больше не получите отказа. Перестаньте спрашивать себя: «Кто нравится мне?», спросите: «Кому нравлюсь я?». Научитесь играть по правилам, принятым в мире женщин. Все, что вам нужно делать, – определить, кто из окружающих вас женщин симпатизирует вам и не прочь познакомиться, а затем правильно «разыграть партию» между вами и вашей новой подругой. Лейл Лаундес расскажет вам о безотказных приемах, которые помогут вам стать виртуозом в общении с противоположным полом. Вы узнаете:• Как женщины выслеживают и приманивают мужчин (о чем мужчины даже не догадываются)• Как научиться безошибочно выявлять (и возбуждать) интерес к своей персоне у представительниц противоположного пола• Как научиться легко и непринужденно знакомиться: как распознать приглашение к знакомству; как сделать первый шаг; что и как говорить, чтобы успешно перейти от знакомства к близости• Как разработать стратегию и тактику развития отношений: как провести первые свидания, чтобы ваши отношения успешно развивались.• Как знакомиться и крутить романы в виртуальном мире, и стоит ли вообще это делать: особенности знакомства в сети Интернет и альтернативные технологии поиска• Как подыграть женщине в предварительной любовной игре, сидя в противоположном конце комнаты: как почувствовать, что в воздухе запахло сексом, и безошибочно определить, готова ли женщина лечь с вами в постель.Эта книга поможет любому мужчине добиться успеха в ухаживании за женщинами успеха, которого все женщины так же сильно желают.','Litres','\"Лейл Лаундес\"','2017','http://books.google.com/books/content?id=kEHWBQAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70KmfM-mQ6maRnmXXLIntjxk5IebYgudn8PWwzGzOl7nGGZST3snWFbMR_P-Ilbkk2NzslxSNphsPEAu0aRguIjqkYFNlhwXugiw6z0C8jXU_VI5GL6E77Kfc8RRkM0fXGjWCbM&source=gbs_api','ru',339,'5457675653','9785457675650',NULL,0),(25,'Opyt o blizhaishem srodstve iazyka slavianorossiiskago s grecheskim',NULL,NULL,'V tip. Akad. Nauk','\"Kōnstantinos Oikonomos\"','1828','http://books.google.com/books/content?id=1M0aAAAAYAAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73yOgrtdHaNlyBpnaRqZ-m4V5d5huOTCm0G_qlO8abOkeNkYg6NqN-YbahSstoIx_OkN0IDGo68XInWFoP0R-CyyzvXgyhBNdCTdsOKfy2yJpWEeBoEtvZtaKbRqqdjqp2J8jNn&source=gbs_api','ru',644,NULL,NULL,'1M0aAAAAYAAJ',0),(29,'Baptism and Change in the Early Middle Ages, C.200-c.1150',NULL,'The liturgy of the medieval church has been little studied in its relation to medieval thought and society. It has often been taken for granted that the Latin liturgy was understood by the priest, but to his congregation was only a spectacle of authority. This book begins with the hypothesis that the liturgy was, in some senses, understood by its congregations, and it attempts to discover what this understanding might have been. Through studies of the sermons and writings of Tertullian, Ambrose, Augustine, Bede, Abelard and others, of the practice of infant baptism, and of the art and architecture of the baptistery, the book attempts to rediscover the underlying philosophy of symbol which is the grounds of ritual understanding in both priest and congregation.','Cambridge University Press','\"Peter Cramer\"','2003','http://books.google.com/books/content?id=AueTHQdX9ngC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE714QU_L9gAA55afL8B-60gPuWxtUY4m8YgFU68PeMIcNDGlaxXU74y50zRUX4S1xz-mPODcHn8bdeox9HjW9LcqKseMt1eiaYDcQ9T9n-WY_2zxRUF_RmNPPZsFQ2kaHMVjLCxu&source=gbs_api','en',380,'0521526426','9780521526425',NULL,0),(30,'C Street',NULL,'Book 2 - Solomon Rose Novels - Trilogy - Stories just get deeper and are designed to be read in any order... Searing sensual nature of Jacqueline is the yin/yang of a life...in a foreign world where power, wealth and corruption reign supreme. The secret of C Street is secrecy. When Solomon Rosenberg, a twenty year veteran of C Street, government lawyer and bi-polar CIA assassin breaks the silence by recording his missions, Jacqueline is left holding the evidence of the treasonous acts. The CIA, the men of C Street, are one step behind, on a mountain top, in London and all the way to Costa Rica. Palestine is willing to pay, and Israel\'s is offering to help. With the evidence of espionage, murder, and the computer hijacking of America, the game is on...','Abacus Books, Incorporated','\"Claudette Walker\"','2010','http://books.google.com/books/content?id=QKnGYgEACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70tbZvNpxeDlw0ESqGvA8Ph6Vu5IGiAZWGQJ7FLH8f9SPbULGkYulfj30kgJITpjDdY4T2NRLWcfRokg97Isb68EVoOdspAWxQKDau_SVnBRoDdfUNc4bbVlsUQsZSNBWY8AG-O&source=gbs_api','en',477,'0971629250','9780971629257',NULL,0),(31,'Географическое и статистическое описаніе Грузіи и Кавказа изъ путешествія ... И. А. Гильденштедта грезъ Россію и по Кавказскимъ горамъ въ 1770, 71, 72, и 73 годахъ. [With a preface by C. Herrmann.]',NULL,NULL,'','\"Johann Anton GUELDENSTAEDT\",\"Carl HERRMANN (of St. Petersburg.)\"','1809','http://books.google.com/books/content?id=LRNbAAAAcAAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70cD-DjctIP7KkMVtYnXUiKc3vvo3LQQH4sH4l0_6s8WHNJBL99k3Sw0OiKgaz_EQQkVMO6IxCL--DyxOGLgJ318fcbdb83MfJ32-oFGT4yQMQuSoJzdaMJNkf0UOFQHbvMWt9m&source=gbs_api','ru',404,NULL,NULL,'LRNbAAAAcAAJ',0),(32,'Безлимитный холдем с небольшими ставками',NULL,'Игра в покер может превратиться в прибыльный источник дополнительного дохода','Poker Books Russia','\"Эд Миллер\"','2011','http://books.google.com/books/content?id=deXe3H1HiVUC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71bO4Jac64yJRIgZ21SgOlHYbLuYI7lT3plN90bRV7kcVm8WWgyTIZ5zMGE12OjYJto673P7Y5SjKQ22YDh4mKsjq-QgMTKqT0IpIaMHUaQJdncv1S6sJJqf42wcbQVgac5TwZw&source=gbs_api','ru',410,'5905432015','9785905432019',NULL,0),(33,'Художник и Adobe Photoshop: от концепции к шедевру',NULL,NULL,'Издательский дом Вильямс',NULL,NULL,'http://books.google.com/books/content?id=easEmDKUQ48C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73KQ5WoVRRtmelp7xteZgi5fmnoheCbKxXui4GH3DG6RKmMmifuTi4Kk_wlI-n7X-8YWeO-ed44xYivpcCNQAmaHj9MEzZtoenT1agGJ4vXTNTlnx-lXi8fR8YQCnJd1DApbr2D&source=gbs_api','ru',241,'5845906547','9785845906540',NULL,0);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exemplar`
--

DROP TABLE IF EXISTS `exemplar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exemplar` (
  `id_exemplar` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `status_exemplar` enum('present','order','issued') NOT NULL DEFAULT 'present',
  `deleted_exemplar` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_exemplar`),
  KEY `book_id_idx` (`book_id`),
  CONSTRAINT `book_id` FOREIGN KEY (`book_id`) REFERENCES `books` (`id_book`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exemplar`
--

LOCK TABLES `exemplar` WRITE;
/*!40000 ALTER TABLE `exemplar` DISABLE KEYS */;
INSERT INTO `exemplar` VALUES (3,5,'issued',0),(4,5,'present',0),(5,6,'present',0),(6,10,'present',0),(7,11,'present',0),(8,12,'present',0),(9,13,'present',0),(10,14,'present',0),(11,15,'present',0),(12,16,'present',0),(13,17,'present',0),(14,18,'present',1),(15,19,'issued',0),(16,20,'order',0),(17,21,'present',0),(20,24,'present',1),(21,25,'present',0),(25,20,'present',1),(26,29,'issued',0),(27,30,'present',1),(28,31,'present',0),(29,32,'present',1),(30,33,'present',0);
/*!40000 ALTER TABLE `exemplar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exemplar_id` int(11) NOT NULL,
  `date_in` timestamp NULL DEFAULT NULL,
  `date_out` date DEFAULT NULL,
  `fine` float NOT NULL DEFAULT '0',
  `status_order` enum('open','active','done') NOT NULL DEFAULT 'open',
  `deleted_order` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order`),
  KEY `user_id_idx` (`user_id`),
  KEY `exemplar_id_idx` (`exemplar_id`),
  CONSTRAINT `exemplar_id` FOREIGN KEY (`exemplar_id`) REFERENCES `exemplar` (`id_exemplar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (3,10,12,NULL,NULL,0,'open',1),(4,10,12,NULL,NULL,0,'open',1),(5,10,12,NULL,NULL,0,'open',1),(6,10,12,'2017-01-31 23:09:53','2017-02-17',0,'done',0),(7,11,3,'2017-02-02 13:16:34','2017-02-15',0,'active',0),(8,10,15,NULL,NULL,0,'open',1),(9,10,15,'2017-02-02 13:18:45','2017-01-31',3,'active',0),(10,10,21,NULL,NULL,0,'open',1),(11,11,26,NULL,NULL,0,'open',1),(12,10,26,NULL,NULL,0,'open',1),(13,11,26,'2017-02-03 08:46:16','2017-02-24',0,'active',0),(14,11,16,NULL,NULL,0,'open',0);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  `password` varchar(100) NOT NULL,
  `library_card` int(16) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `patronomic` varchar(100) NOT NULL,
  `passport_ID` varchar(10) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `role` enum('admin','librarian','reader') NOT NULL,
  `deleted_user` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `library_card_UNIQUE` (`library_card`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@gmail.ru','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',NULL,'f','f','f','EH 000000','f','admin',0),(2,'kuwafa@rootfest.net','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',NULL,'1','11','1','АМ 123456','іаіа','librarian',1),(3,'zuwocerat@binka.me','6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',NULL,'1','1','1','АМ 223456','аывыва','librarian',0),(5,'yofilojepa@binka.me','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',NULL,'1','1','1','АМ 243456','аывыва','librarian',0),(6,'rk0hv@vmani.com','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',0,'12','23','123','ВА 344354','ыаыа','librarian',0),(7,'wuyikug@binka.me','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',1,'12','23','123','ВА 346354','ыаыа','librarian',0),(8,'hgfq5@vmani.com','e05212ed9874e606ca9d0dc9304f2f73b7108810f205e14ce7d4c23ded65f727',3,'afaf','sdf','fdfds','АМ 123457','віавіа','librarian',0),(9,'rejamibu@binka.me','322bd64b69b65d00e6eb0f2980ad344b5ead555cb097e3f53b42d710d492a507',NULL,'fdsf','sdfsdf','sdfdsf','АИ 214345','324234','librarian',0),(10,'user@gmail.ru','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',NULL,'f','f','f','EH 000001','f','reader',0),(11,'user1@gmail.ru','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',NULL,'f','f','f','EH 000002','f','reader',0),(12,'rejamibu2@binka.me','322bd64b69b65d00e6eb0f2980ad344b5ead555cb097e3f53b42d710d492a507',NULL,'fdsf','sdfsdf','sdfdsf','АИ 214345','324234','librarian',0),(13,'rejamib2u@binka.me','322bd64b69b65d00e6eb0f2980ad344b5ead555cb097e3f53b42d710d492a507',NULL,'fdsf','sdfsdf','sdfdsf','АИ 214345','324234','librarian',0),(14,'librarian@gmail.ru','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',NULL,'fdsf','sdfsdf','sdfdsf','АИ 214345','324234','librarian',0),(15,'coxoyum@binka.me','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',4,'авыа','ыва','ыва','АМ 123466','Кирова','reader',0),(16,'wagalem@binka.me','03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4',5,'fdf','dfdsf','dsffdsf','АИ 314345','Харьков','reader',0),(17,'yesefaz@binka.me','f6e0a1e2ac41945a9aa7ff8a8aaa0cebc12a3bcc981a929ad5cf810a090e11ae',6,'Юра','Юрий ','Юрьевич','ЮР 131342','Ростов','reader',0),(18,'xeyekece@binka.me','37ba7a51e2e96529d302a0d3c4b210b56510391ac3d41390c1c7fc21deecf1a8',NULL,'Тест1','Тест1','Тест1','АМ 123422','ываыва','admin',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-06 20:00:44
